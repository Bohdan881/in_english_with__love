import { createStore, compose, applyMiddleware } from "redux";
import rootReducer from "./reducers/root";
// import logger from "redux-logger";
import { persistStore } from "redux-persist";
import { withFirebase } from "../firebase";
import firebaseLessonsMiddleware from "./middleware";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(firebaseLessonsMiddleware))
);

export const persistor = persistStore(store);

window.store = withFirebase(store); // makes the store available globally
