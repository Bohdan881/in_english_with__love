const asyncActionType = (type) => ({
  PENDING: `${type}_PENDING`,
  SUCCESS: `${type}_SUCCESS`,
  ERROR: `${type}_ERROR`,
});

/* API */
export const API_REQUEST = "API_REQUEST";

/* API requests  */
export const GET_ALL_LESSONS = asyncActionType("GET_ALL_LESSONS");

/* */
export const AUTH_USER_SET = "AUTH_USER_SET";
export const USERS_SET = "USERS_SET";
export const USER_SET = "USER_SET";

/* post values */
export const SET_LESSON_VALUES = "SET_LESSON_VALUES";
export const SET_LESSON_TO_INIT = "SET_LESSON_TO_INIT";
export const SET_LESSON_TO_INIT_BY_CATEGORY = "SET_LESSON_TO_INIT_BY_CATEGORY";

/* user activity */
export const SET_USER_ACTIVITY_VALUES = "SET_USER_ACTIVITY_VALUES";
