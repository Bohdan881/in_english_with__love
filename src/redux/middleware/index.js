import { API_REQUEST } from "../constants/actionTypes";

const firebaseLessonsMiddleware = (store) => (next) => (action) => {
  const { type, firebase } = action;
  if (type === API_REQUEST) {
    next({
      type: action.next.PENDING,
    });

    firebase.db.ref("posts").on(
      "value",
      (snapshot) => {
        const lessonsObject = snapshot && snapshot.val();
        if (lessonsObject) {
          let response = {};
          Object.entries(lessonsObject || {}).forEach(
            ([lessonId, lessonContent]) => {
              const { category, subCategory } = lessonContent;

              const assignedLesson = { ...lessonContent, uid: lessonId };
              if (!response[category]) {
                response[category] = {
                  [subCategory]: [assignedLesson],
                };
              } else {
                if (!response[category][subCategory]) {
                  response[category][subCategory] = [assignedLesson];
                } else {
                  response[category][subCategory] = [
                    ...response[category][subCategory],
                    assignedLesson,
                  ];
                }
              }
            }
          );

          next({
            type: action.next.SUCCESS,
            payload: response,
          });
        }
      },
      (error) => {
        next({
          type: action.next.ERROR,
          error: true,
          payload: error.message,
        });
      }
    );
  } else {
    next(action);
  }
};

export default firebaseLessonsMiddleware;
