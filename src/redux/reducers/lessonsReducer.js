import { GET_ALL_LESSONS } from "../constants/actionTypes";

const initState = {
  loading: false,
  error: false,
  data: null,
};

const lessonsReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_ALL_LESSONS.PENDING:
      return {
        ...state,
        loading: true,
      };
    case GET_ALL_LESSONS.ERROR:
      return {
        ...state,
        error: action.payload,
        data: [],
        loading: false,
      };
    case GET_ALL_LESSONS.SUCCESS:
      return {
        ...state,
        data: action.payload,
        error: false,
        loading: false,
      };
    default:
      return state;
  }
};

export default lessonsReducer;
