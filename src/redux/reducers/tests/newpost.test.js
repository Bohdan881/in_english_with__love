// import reducer from '../../structuring-reducers/todos'
import * as types from "../../constants/ActionTypes";
import { newPostReducer, initState } from "../newpost";
import * as CONSTANTS from "../../../constants";

describe("root reducer", () => {
  it("should return the initial state", () => {
    expect(newPostReducer(initState, { type: types.SET_LESSON_TO_INIT })).toEqual(
      {
        ...CONSTANTS.INIT_NEW_POST_VALUES,
        reset: true,
      }
    );
  });
});
