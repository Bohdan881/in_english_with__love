import { combineReducers } from "redux";
import lessonsReducer from "./lessonsReducer";
import newLessonReducer from "./newLessonReducer";
import sessionReducer from "./session";
import userReducer from "./users";

const rootReducer = combineReducers({
  lessons: lessonsReducer,
  sessionState: sessionReducer,
  userState: userReducer,
  newLessonState: newLessonReducer,
});

export default rootReducer;
