/* deals with the list of users from the Firebase realtime database. */

import { USERS_SET, USER_SET } from "../constants/actionTypes";

const INITIAL_STATE = {
  users: [],
  usersDataChart: [],
};

const applySetUser = (state, action) => ({
  ...state,
  users: {
    ...state.users,
    [action.uid]: action.user,
  },
});

function userReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case USERS_SET: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case USER_SET: {
      return applySetUser(state, action);
    }
    default:
      return state;
  }
}

export default userReducer;
