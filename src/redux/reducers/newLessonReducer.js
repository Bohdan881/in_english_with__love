import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import {
  SET_LESSON_VALUES,
  SET_LESSON_TO_INIT,
  SET_LESSON_TO_INIT_BY_CATEGORY,
} from "../constants/actionTypes";
import {
  CATEGORIES,
  INIT_LESSON_SHARED_KEY_VALUES,
  INIT_LESSON_KEY_VALUES_BY_CATEGORY,
} from "../../constants";

export const initState = {
  uid: "",
  ...INIT_LESSON_SHARED_KEY_VALUES,
  ...INIT_LESSON_KEY_VALUES_BY_CATEGORY[CATEGORIES[0].key],
};

const authUser = JSON.parse(localStorage.getItem("authUser"));

const isAdmin =
  !!authUser && !!authUser.roles && authUser.roles["ADMIN"] === "ADMIN";

const newLessonStateConfig = {
  key: "root",
  storage: storage,
  blacklist: !isAdmin ? [...Object.keys(initState)] : [""],
};

const newLessonReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case SET_LESSON_VALUES:
      return {
        ...state,
        ...payload,
      };
    case SET_LESSON_TO_INIT_BY_CATEGORY:
      return {
        ...payload,
      };
    case SET_LESSON_TO_INIT:
      return {
        ...INIT_LESSON_SHARED_KEY_VALUES,
        ...INIT_LESSON_KEY_VALUES_BY_CATEGORY[CATEGORIES[0].key],
      };
    default:
      return state;
  }
};

export default persistReducer(newLessonStateConfig, newLessonReducer);
