import storage from "redux-persist/lib/storage";
import persistReducer from "redux-persist/es/persistReducer";
import {
  AUTH_USER_SET,
  SET_USER_ACTIVITY_VALUES,
} from "../constants/actionTypes";

const initState = {
  authUser: null,
  progressAreaChart: [],
  categoriesDataChart: [],
  allLessons: 0,
  lessonProgress: {},
  lessonInfo: {},
};

const persistConfig = {
  key: "session",
  storage: storage,
  whiteList: ["lessonInfo", "lessonProgress", "authUser"],
};

const sessionReducer = (state = initState, action) => {
  const { type, authUser, payload } = action;

  switch (type) {
    case AUTH_USER_SET: {
      return {
        ...state,
        authUser: authUser,
      };
    }
    case SET_USER_ACTIVITY_VALUES:
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
};

export default persistReducer(persistConfig, sessionReducer);
// export default sessionReducer;
