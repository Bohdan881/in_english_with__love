import {
  API_REQUEST,
  SET_USER_ACTIVITY_VALUES,
  SET_LESSON_VALUES,
  USERS_SET,
  SET_LESSON_TO_INIT,
  SET_LESSON_TO_INIT_BY_CATEGORY,
  GET_ALL_LESSONS,
} from "../constants/actionTypes";

// Get All Lessons from DB -------------------------
export const getAllLessons = (firebase) => {
  return {
    type: API_REQUEST,
    firebase,
    next: GET_ALL_LESSONS,
  };
};

// --------------------------------------------------

// User Activity ------------------------------------

export const setSessionValues = (values) => {
  return {
    type: SET_USER_ACTIVITY_VALUES,
    payload: {
      ...values,
    },
  };
};

// ------------------------------------------------

// Lesson changes ---------------------------------

export const setLessonValues = (values) => {
  return {
    type: SET_LESSON_VALUES,
    payload: {
      ...values,
    },
  };
};

export const setLessonToInitByCategory = (values) => {
  return {
    type: SET_LESSON_TO_INIT_BY_CATEGORY,
    payload: {
      ...values,
    },
  };
};

export const setLessonToInit = () => ({ type: SET_LESSON_TO_INIT });

// ------------------------------------------------

// Users changes ----------------------------------

export const setUsers = (values) => {
  return {
    type: USERS_SET,
    payload: {
      ...values,
    },
  };
};

// ------------------------------------------------
