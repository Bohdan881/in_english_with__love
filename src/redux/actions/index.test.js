import * as actions from "./index";
import * as types from "../constants/actionTypes";
import * as CONSTANTS from "../../constants";

describe("actions", () => {
  it("should create an action to refresh all values in newpost store", () => {
    // const text = "Finish docs";
    const expectedAction = {
      type: types.SET_LESSON_TO_INIT,
      ...CONSTANTS.INIT_NEW_POST,
    };
    expect(actions.setLessonToInit()).toEqual(expectedAction);
  });
});
