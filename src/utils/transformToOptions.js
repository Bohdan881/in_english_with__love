/**   Converts values to dropdown options
 * @param {Array} arr an array of values that should be transformed into options object
 * @param {Object} parameters and object extra parameters that can be passed into the array
 * An example => "a" => {text: "a", value: "a", key: "a", disabled: "false"}
 **/

const transformToOptions = (arr, parameters = {}) => {
  const { disabled } = parameters;
  return arr && arr.length
    ? arr.map((el) => ({
        key: el,
        text: el,
        value: el,
        ...(disabled && { disabled }),
      }))
    : [];
};

export default transformToOptions;
