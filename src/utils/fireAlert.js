/**
 * Custom modal windows
 */

import Swal from "sweetalert2";

export const confirmationAlert = ({
  icon,
  title,
  text,
  cancelButtonText,
  confirmButtonText,
  showDenyButton,
  denyButtonText,
}) =>
  Swal.fire({
    icon,
    title,
    text,
    cancelButtonText,
    confirmButtonText,
    showCancelButton: true,
    showDenyButton,
    denyButtonText,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: !showDenyButton && "#d33",
    position: "top-end",
  });

export const infoAutoCloseAlert = ({ icon, title, text }) => {
  setTimeout(() => Swal.close(), 4000);
  return Swal.fire({
    icon,
    heightAuto: false,
    title,
    text,
    customClass: {
      container: "alert-container-class",
    },
    position: "top-end",
    popup: "swal2-show",
    className: "admit-sweet-alert",
    reverseButtons: true,
  });
};
