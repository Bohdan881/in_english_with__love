/**
 * Shuffles array in place
 * @param {Array} a items of an array of the items.
 */

const shuffleArray = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
};

export default shuffleArray;
