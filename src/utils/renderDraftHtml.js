/**  
  Converts draft format from editor || json || string formats to html
  * @param {Object} draftData the payload with the data written via DraftJS
**/

import React from "react";
import { convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";

const renderDraftHtml = (draftData) => {
  if (!!draftData) {
    // parse json if it has typeof string
    if (
      typeof draftData === "string" &&
      typeof JSON.parse(draftData) === "string"
    ) {
      return (
        <div
          dangerouslySetInnerHTML={{
            __html: JSON.parse(draftData),
          }}
        />
      );
    }
    // convert to html and then parse it
    if (
      typeof draftData === "string" &&
      typeof JSON.parse(draftData) === "object"
    ) {
      return (
        <div
          dangerouslySetInnerHTML={{
            __html: draftToHtml(JSON.parse(draftData)),
          }}
        />
      );
    }
    // use libraries tools to prase it if it has an immutable key
    else if (Object.keys(draftData)[0] === "_immutable") {
      return (
        <div
          dangerouslySetInnerHTML={{
            __html: draftToHtml(convertToRaw(draftData.getCurrentContent())),
          }}
        />
      );
    }
  }
  return null;
};

export default renderDraftHtml;
