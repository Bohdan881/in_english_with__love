/**
 * Optimizes image
 * @param  {Blob Files} files the initial paratemers of the picture
 * @param  {Object} options the options to which the picture should be converted
 */

const optimizeImage = async (files, options) => {
  const { maxWidth, maxHeight, quality, outputAddress } = options;

  const photo = files[0];

  let canvas = await readPhoto(photo);

  while (canvas.width >= 2 * maxWidth) {
    canvas = scaleCanvas(canvas, 0.5);
  }

  if (canvas.width > maxWidth) {
    canvas = scaleCanvas(canvas, maxWidth / canvas.width);
  }

  while (canvas.height >= 2 * maxWidth) {
    canvas = scaleCanvas(canvas, 0.5);
  }

  if (canvas.height > maxHeight) {
    canvas = scaleCanvas(canvas, maxHeight / canvas.height);
  }

  return new Promise((resolve) => {
    canvas.toBlob(resolve, "image/jpeg", quality);
  }).then((blob) => {
    let output = document.querySelector(outputAddress);

    if (output) {
      output.src = canvas.toDataURL("image/png", quality);
    }

    return blob;
  });
};

const readPhoto = async (photo) => {
  const canvas = document.createElement("canvas");
  const img = document.createElement("img");

  // create img element from File object
  img.src = await new Promise((resolve) => {
    const reader = new FileReader();
    reader.onload = (e) => resolve(e.target.result);
    reader.readAsDataURL(photo);
  });
  await new Promise((resolve) => {
    img.onload = resolve;
  });

  // draw image in canvas element
  canvas.width = img.width;
  canvas.height = img.height;
  canvas.getContext("2d").drawImage(img, 0, 0, canvas.width, canvas.height);

  return canvas;
};

const scaleCanvas = (canvas, scale) => {
  const scaledCanvas = document.createElement("canvas");
  scaledCanvas.width = canvas.width * scale;
  scaledCanvas.height = canvas.height * scale;

  scaledCanvas
    .getContext("2d")
    .drawImage(canvas, 0, 0, scaledCanvas.width, scaledCanvas.height);

  return scaledCanvas;
};

export default optimizeImage;
