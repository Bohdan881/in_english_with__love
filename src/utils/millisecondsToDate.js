/**
 * Converts milliseconds into the date
 * @param  {Number} milliseconds an amount of milliseconds that should be converted into the date
 */

const convertMillisecondsToDate = (milliseconds) => {
  const date = new Date(milliseconds);
  const day = date.getDate();
  const month = date.toString().slice(4, 7);
  const year = date.getFullYear();

  return `${month} ${day}, ${year} `;
};

export default convertMillisecondsToDate;
