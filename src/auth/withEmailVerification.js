import React from "react";
import { Button } from "semantic-ui-react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withFirebase } from "../firebase";
import { InfoMessage, ErrorMessage } from "../views/Shared/Message/Message";

// style
import "./style.scss";

const needsEmailVerification = (authUser) =>
  authUser &&
  !authUser.emailVerified &&
  authUser.providerData
    .map((provider) => provider.providerId)
    .includes("password");

const withEmailVerification = (Component) => {
  class WithEmailVerification extends React.Component {
    state = {
      isSent: false,
      error: false,
    };

    onSendEmailVerification = () => {
      this.props.firebase
        .doSendEmailVerification()
        .then(() => this.setState({ isSent: true }))
        .catch((error) => this.setState({ error: error.message }));
    };

    render() {
      const { error, isSent } = this.state;
      return needsEmailVerification(this.props.authUser) ? (
        <div className="verification-container">
          {isSent ? (
            <InfoMessage
              header="E-Mail Confirmation Sent"
              content="Check your E-Mails (Spam folder included) for a confirmation
                  E-Mail. Refresh this page once you confirmed your E-Mail."
            />
          ) : (
            <InfoMessage
              header="Verify Your E-Mail"
              content="Check your E-Mails (Spam folder included) for a confirmation
                  E-Mail. Refresh this page once you confirmed your E-Mail."
            />
          )}
          <Button
            primary
            type="button"
            onClick={this.onSendEmailVerification}
            disabled={isSent || error}
            className="mt-1"
          >
            Send confirmation E-Mail
          </Button>
          {error && <ErrorMessage content={error} className="mt-1" />}
        </div>
      ) : (
        <Component {...this.props} />
      );
    }
  }

  const mapStateToProps = (state) => ({
    authUser: state.sessionState.authUser,
  });

  return compose(withFirebase, connect(mapStateToProps))(WithEmailVerification);
};

export default withEmailVerification;
