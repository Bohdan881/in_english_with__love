/* Confirmation Alerts */
export const CONFIRMATION_REMOVE_CONTENT = {
  icon: "warning",
  title: "Are you sure?",
  text: "You won't be able to revert this!",
  confirmButtonText: "Yes, cancel the progress!",
  cancelButtonText: "Go back",
};

export const REMOVE_QUESTION_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this question/sentence ?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Go back",
};

export const REMOVE_EXERCISE_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this exercise?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Cancel",
};

export const REMOVE_STRUCTURE_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this structure?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Cancel",
};

export const QUIT_AND_SAVE_LESSON_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure you want to quit?",
  denyButtonText: "Don't save and quit it!",
  confirmButtonText: "Save and quit it!",
  cancelButtonText: "Cancel",
};

export const QUIT_ONLY_LESSON_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure you want to quit?",
  confirmButtonText: "Yes, quit it!",
  cancelButtonText: "Cancel",
};

export const REMOVE_WORD_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this word?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Cancel",
};

export const REMOVE_EXAMPLE_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this example?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Cancel",
};

export const REMOVE_BLOCK_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this block?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Cancel",
};

export const REMOVE_LESSON_ALERT_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this lesson?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Cancel",
};

export const REMOVE_IMAGE_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete this image?",
  confirmButtonText: "Yes, delete it",
  cancelButtonText: "Cancel",
};

export const CREATE_EDIT_LESSON_CONFIRAMTION = {
  icon: "warning",
  title: "Are you sure?",
  confirmButtonText: "Yes",
  cancelButtonText: "Cancel",
};

export const REMOVE_ACCOUNT_CONFIRMATION = {
  icon: "warning",
  title: "Are you sure?",
  text: "Are you sure you want to delete your account?",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "Cancel",
};

export const SIGN_UP_SUGGESTION_ALERT_CONFIRMATION = {
  icon: "success",
  title: "Good job!",
  text: "Do you want to sign up and save your progess?",
  confirmButtonText: "Sign up and save it!",
  cancelButtonText: "Go back.",
};

/* Auto Closing alerts */

export const ERROR_ALERT = {
  icon: "error",
  title: "Oops...",
  text: "Something went wrong!",
};

export const ICON_LESSON_REMOVE_ALERT = {
  icon: "success",
  text: "The icon has been deleted!",
  title: "Success!",
};

export const IMAGE_LESSON_REMOVE_ALERT = {
  icon: "success",
  text: "The image has been deleted!",
  title: "Success!",
};

export const LESSON_REMOVED_ALERT = {
  icon: "success",
  text: "The lesson has been deleted successfully!",
  title: "Success",
};

export const ICON_LESSON_ADD_ALERT = {
  icon: "success",
  text: "The Icon has been uploded successfully!",
  title: "Success!",
};

export const IMAGE_LESSON_ADD_ALERT = {
  icon: "success",
  text: "The Image has been uploded successfully!",
  title: "Success!",
};

export const LESSON_COMPLETE_CONFIRMATION = {
  icon: "success",
  text: "You successfully completed this lesson!",
  title: "Good job!",
};

export const LESSON_PROGRESS_SAVE_CONFIRMATION = {
  icon: "success",
  text: "The progress has been saved successfully",
  title: "Good job!",
};

export const SIGN_UP_SUGGESTION_ALERT = {
  icon: "success",
  text: "Do you want to sign up to save your progress?",
  title: "Good job!",
};

export const PASSWORD_CHANGE_CONFIRMATION = {
  icon: "success",
  text: "You successfully changed password!",
  title: "Success!",
};

export const UPDATE_PROFILE_CONFIRMATION = {
  icon: "success",
  text: "You successfully updated your profile!",
  title: "Success!",
};

export const REMOVE_LESSON_ALERT = {
  icon: "question",
  text: "Are you sure you want to remove this lesson?",
};

export const LESSON_STATUS_ALERT = {
  icon: "success",
  text: "The lesson has been created!",
  title: "Success!",
};
