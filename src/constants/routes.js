export const HOME = "/";
export const SIGN_UP = "/signup";
export const SIGN_IN = "/signin";
export const VOCABULARY = "/vocabulary";
export const VIDEO = "/video";
export const PHRASAL_VERBS = "/phrasal-verbs";
export const ABOUT = "/about";
export const CONTACT = "/contact";
export const ACCOUNT = "/account";
export const ADMIN = "/admin";
export const PRIVACY_POLICY = "/privacy";
export const LESSON_TOPIC_LIST = "/lessons-list";
export const LESSON_TOPIC = "/topic";
export const PASSWORD_FORGET = "/pw-forget";
// groups of routes
export const AUTH_ROUTES = [HOME, VIDEO, VOCABULARY, PHRASAL_VERBS];
export const FOOTER_ROUTES = [PRIVACY_POLICY, CONTACT];
