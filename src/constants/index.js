/*
  Constants that are shared acccros the app
*/

export const PASSWORD_SPECIAL_CHARACTERS = [
  "!",
  "#",
  "$",
  "%",
  "@",
  "^",
  "&",
  "*",
];

export const CATEGORY_ID = "-category-";
export const PASSWORD_FORGET_INIT = {
  email: "",
  error: null,
  isSent: false,
};

/* Auth */

export const SIGN_IN_INITIAL_STATE = {
  email: "",
  password: "",
  error: null,
};

export const SIGN_UP_INITIAL_STATE = {
  username: "",
  email: "",
  password: "",
  "repeat password": "",
  isAdmin: false,
  error: null,
};

export const ERROR_CODE_ACCOUNT_EXISTS =
  "auth/account-exists-with-different-credential";

export const ERROR_MSG_ACCOUNT_EXISTS = `
  An account with an E-Mail address to
  this social account already exists. 
`;

export const ERROR_MESSAGES = {
  username: "The username must be 4 characters long or more.",
  confirmPassword: "The passwords don't much",
  accountExist:
    "An account with an E-Mail address to this social account already exists. ",
};
export const SIGN_IN = "SIGN_IN";
export const SIGN_UP = "SIGN_UP";
export const SIGN_OUT = "SIGN OUT";

export const SIGN_IN_METHODS = [
  // {
  //   id: "password",
  //   icon: "lock",
  //   provider: null,
  // },
  {
    name: "Google",
    id: "google.com",
    icon: "google",
    provider: "googleProvider",
  },
  {
    name: "Facebook",
    id: "facebook.com",
    icon: "facebook f",
    provider: "facebookProvider",
  },
];

// writing component --------------------------

export const WRITING_TABLE_HEADER_CELLS = ["Type", "Example"];
export const WRITING_INIT_VALUES = {
  production: {
    description: "Choose 3 - 5 words you learned today, and use them to:",
    bulletPoints: [
      "6 - 10 separate sentences",
      "A paragraph",
      "A short story",
      "A poem or a song",
    ],
  },
  examples: {
    simple: "She likes travelling",
    compound:
      "She likes traveling, <b> and </b> she loves exploring new places.",
    complex:
      "<b> Even though </b> she likes traveling, she doesn’t travel often.",
  },
};

// --------------------------------------------

// new words component ------------------------

export const INIT_NEW_WORDS_CONTENT = {
  header: "",
  description: "",
  examples: [
    {
      exampleId: 0,
      example: "",
    },
  ],
};

// -----------------------------------------

// speaking component ------------------------

export const INIT_SPEAKING_CONTENT = {
  contentBlocks: [
    {
      blockId: 1,
      blockContent: "",
      pageNumber: 1,
    },
  ],
};

// -----------------------------------------

// components key names -------------------------

export const WRITING_PRACTICE = "Writing Practice";
export const SPEAKING_PRACTICE = "Speaking";
export const PHRASAL_VERBS = "Phrasal Verbs";
export const NEW_VOCABULARY = "Vocabulary";
// ----------------------------------------------

// admin page ------------------------------

export const PREVIEW_MODE = "Preview";
export const CONTENT_VIDEO_SOURCES = ["youtube", "vimeo"];

export const ADMIN_TABS = {
  createLesson: {
    key: "createLesson",
    content: "Create a lesson",
    icon: "add",
  },
  allLessons: { key: "allLessons ", content: "All lessons", icon: "list" },
  users: { key: "users ", content: "Users", icon: "users" },
  reset: { key: "reset ", content: "Reset", icon: "redo" },
};

export const ADMIN_DROPDOWN_TITLES = {
  category: {
    label: "Category",
    placeholder: "Select Category",
    defaultVal: "category",
    allValues: "categories",
  },
  subCategory: {
    label: "Subcategory",
    placeholder: "Select Subcategory",
    defaultVal: "subCategory",
    allValues: "subCategories",
  },

  title: {
    label: "Title",
    placeholder: "Title",
    defaultVal: "title",
  },
};

export const CREATE_LESSON_STAGES = {
  about: { key: "About", content: "About", icon: "info" },
  practice: { key: "Exercises", content: "Exercises", icon: "legal" }, //lab
  newWords: { key: "New Words", content: "New Words", icon: "bold" },
  words: { key: "New Words", content: "New Words", icon: "bold" },
  phrasalverbs: {
    key: "Phrasal Verbs",
    content: "Phrasal Verbs",
    icon: "bold",
  },
  vocabulary: { key: "Vocabulary", content: "Vocabulary", icon: "bold" },
  content: { key: "Content", content: "Content", icon: "picture" },
  writing: { key: "Writing", content: "Writing", icon: "write" },
  speaking: {
    key: SPEAKING_PRACTICE,
    content: SPEAKING_PRACTICE,
    icon: "conversation",
  },
  preview: { key: "Preview", content: "Preview", icon: "zoom" },
};

export const CATEGORIES = [
  { key: "vocabulary", text: "Vocabulary", value: "Vocabulary" },
  { key: "video", text: "Video", value: "Video" },
  { key: "phrasalverbs", text: "Phrasal Verbs", value: "Phrasal Verbs" },
];

export const SUB_CATEGORIES = [
  "Music",
  "Movies",
  "Ocean",
  "Nature & Animals",
  "Travel",
  "Art & Culture",
  "Sustainability",
  "Unusual Activities",
  "Stories",
  "Food",
  "Philosophy",
  "Mind & Body",
  "Social Issues",
  "Songs",
  "People",
];

export const OPTIMIZED_ICON_IMAGE_ID = "optimized-icon-image";
export const CARD_OPTIONS = {
  maxWidth: 260,
  maxHeight: 170,
  quality: 0.9,
};

export const ABOUT_IMAGE_OPTIONS = {
  maxWidth: 300,
  maxHeight: 250,
  quality: 0.9,
};

// exercise names ----------------------------

export const FILL_IN_THE_BLANKS = "Fill in the blanks";
export const ANOTHER_WAY_TO_SAY = "Another way to say";
export const DEFINITIONS = "Definitions";
export const WORDS_IN_CONTEXT = "Words in context";
export const CLOZE = "Cloze";

// --------------------------------------------

export const EXERCISES_NAMES = [
  {
    key: WORDS_IN_CONTEXT,
    text: WORDS_IN_CONTEXT,
    value: WORDS_IN_CONTEXT,
  },
  {
    key: FILL_IN_THE_BLANKS,
    text: FILL_IN_THE_BLANKS,
    value: FILL_IN_THE_BLANKS,
  },
  {
    key: DEFINITIONS,
    text: DEFINITIONS,
    value: DEFINITIONS,
  },
  {
    key: ANOTHER_WAY_TO_SAY,
    text: ANOTHER_WAY_TO_SAY,
    value: ANOTHER_WAY_TO_SAY,
  },
  {
    key: CLOZE,
    text: CLOZE,
    value: CLOZE,
  },
];

const TYPE_WORD_DESC = {
  key: "typeWordDesc",
  value: "Type a word from the box below into an approriate field.",
};

export const EXERCISES_DESCRIPTIONS = {
  [CLOZE]: [
    {
      key: "readTheText",
      text: TYPE_WORD_DESC.value,
      value: TYPE_WORD_DESC.value,
    },
  ],
  [FILL_IN_THE_BLANKS]: [
    {
      key: TYPE_WORD_DESC.key,
      text: TYPE_WORD_DESC.value,
      value: TYPE_WORD_DESC.value,
    },
  ],
  [DEFINITIONS]: [
    {
      key: "definitions",
      text: TYPE_WORD_DESC.value,
      value: TYPE_WORD_DESC.value,
    },
  ],
  [ANOTHER_WAY_TO_SAY]: [
    {
      key: "rewrite",
      text: TYPE_WORD_DESC.value,
      value: TYPE_WORD_DESC.value,
    },
  ],

  [WORDS_IN_CONTEXT]: [
    {
      key: "wordsInContext",
      text: TYPE_WORD_DESC.value,
      value: TYPE_WORD_DESC.value,
    },
  ],
};

export const EXERCISES_DETAILED_DESCRIPTIONS = {
  [FILL_IN_THE_BLANKS]: [
    {
      key: "changes",
      text: "You might have to change verb forms (talk – talking or talked).",
      value: "You might have to change verb forms (talk – talking or talked).",
    },
  ],
  [ANOTHER_WAY_TO_SAY]: [
    {
      key: "preposition",
      text:
        "You might have to make some changes to the sentences, or add/change a preposition.",
      value:
        "You might have to make some changes to the sentences, or add/change a preposition.",
    },
  ],
};

// lesson mapping  --------------------------

export const MULTIPLE_PAGES_CHAPTER = CREATE_LESSON_STAGES.vocabulary.key;

export const LESSON_FORMATTED_HEADERS = {
  About: {
    vocabulary: "About the Worksheet",
    phrasalverbs: "About the Worksheet",
    video: "About the Video",
  },
  Content: { video: "Watch the Video" },
};
// ------------------------------------------------

// new lesson redux ---------------------------------

export const LESSON_MODE = {
  EDIT: "edit",
  CREATE: "create",
};

export const INIT_LESSON_SHARED_KEY_VALUES = {
  uid: "",
  category: "",
  date: "",
  subCategory: SUB_CATEGORIES[0],
  title: "",
  iconPath: "",
  lessonTheoreticalContent: {},
  lessonPracticalContent: [],
  lessonMode: LESSON_MODE.CREATE,
  localStorageData: { [CREATE_LESSON_STAGES.about.key]: "" },
};

export const INIT_VOCABULARY_LESSON_KEY_VALUES = {
  lessonTheoreticalContent: {
    [CREATE_LESSON_STAGES.about.key]: "",
    [CREATE_LESSON_STAGES.speaking.key]: [],
    [WRITING_PRACTICE]: WRITING_INIT_VALUES,
    [NEW_VOCABULARY]: [],
  },
};

export const INIT_PHRASAL_VERBS_LESSON_KEY_VALUES = {
  lessonTheoreticalContent: {
    [CREATE_LESSON_STAGES.about.key]: "",
    [CREATE_LESSON_STAGES.speaking.key]: [],
    [WRITING_PRACTICE]: WRITING_INIT_VALUES,
    [NEW_VOCABULARY]: [],
  },
};

export const INIT_VIDEO_LESSON_KEY_VALUES = {
  contentVideoValues: {
    source: "",
    id: "",
  },
  lessonTheoreticalContent: {
    [CREATE_LESSON_STAGES.about.key]: "",
    [CREATE_LESSON_STAGES.content.key]: "",
    [CREATE_LESSON_STAGES.speaking.key]: [],
    [WRITING_PRACTICE]: WRITING_INIT_VALUES,
  },
  localStorageData: {
    ...INIT_LESSON_SHARED_KEY_VALUES.localStorageData,
    Content: "",
  },
};

export const INIT_LESSON_KEY_VALUES_BY_CATEGORY = {
  vocabulary: INIT_VOCABULARY_LESSON_KEY_VALUES,
  phrasalverbs: INIT_PHRASAL_VERBS_LESSON_KEY_VALUES,
  video: INIT_VIDEO_LESSON_KEY_VALUES,
};

// ----------------------------------------

export const COMPLETE_FIELDS = {
  id: { label: "Num.", placeholder: "number" },
  sentence: { label: "Sentence", placeholder: "Sentence" },
  asnwer: { label: "Answer", placeholder: "" },
  initWord: { label: "Initial Word/Phrase", placeholder: "" },
};

export const INIT_FIELDS_CONTENT = {
  [WORDS_IN_CONTEXT]: {
    id: 0,
    sentence: "",
    answer: "",
  },
  [FILL_IN_THE_BLANKS]: {
    id: 0,
    sentence: "",
    answer: "",
    initWord: "",
  },
  [ANOTHER_WAY_TO_SAY]: {
    id: 0,
    sentence: "",
    answer: "",
  },
  [CLOZE]: {
    text: "",
    answers: [],
  },

  [DEFINITIONS]: {
    id: 0,
    sentence: "",
    answer: "",
  },
};

// db values --------------------------------
export const LESSONS_BUCKET_NAME = "posts";
export const TOPICS_BUCKET_NAME = "topics";
export const USERS_BUCKET_NAME = "users";
export const DEFAULT_TOPIC_IMAGE = "default";

// ------------------------------------------

// editor page ------------------------------
export const EDIT_CREATE_LESSON_TAB_INDEX = 0;

export const EDITOR_OPTIONS = {
  fontFamily: [
    "Arial",
    "Lato",
    "Georgia",
    "Impact",
    "Tahoma",
    "Times New Roman",
    "Verdana",
  ],
};

export const EXERCISES_LABELS_COLORS = [
  "blue",
  "teal",
  "green",
  "olive",
  "brown",
  "violet",
  "pink",
  "red",
  "orange",
];

// category topics ----------------------------

export const LESSON_TYPE = {
  video: "Video",
  phrasalverbs: "Phrasal Verbs",
  vocabulary: "Vocabulary",
};

// Lessons List -------------------------------
export const ONE_PAGE_LESSONS = 10;

// Lesson View  -------------------------------

export const LESSON_SEQUENCE_BY_CATEGORY = {
  video: [
    "About",
    WORDS_IN_CONTEXT,
    FILL_IN_THE_BLANKS,
    DEFINITIONS,
    ANOTHER_WAY_TO_SAY,
    "Content",
    SPEAKING_PRACTICE,
    WRITING_PRACTICE,
  ],
  vocabulary: [
    "About",
    NEW_VOCABULARY,
    WORDS_IN_CONTEXT,
    FILL_IN_THE_BLANKS,
    DEFINITIONS,
    ANOTHER_WAY_TO_SAY,
    CLOZE,
    SPEAKING_PRACTICE,
    WRITING_PRACTICE,
  ],
  phrasalverbs: [
    "About",
    NEW_VOCABULARY,
    WORDS_IN_CONTEXT,
    FILL_IN_THE_BLANKS,
    DEFINITIONS,
    ANOTHER_WAY_TO_SAY,
    CLOZE,
    SPEAKING_PRACTICE,
    WRITING_PRACTICE,
  ],
};

export const CONTENT_VIDEO_SOURCE_VALUES = {
  vimeo: "Vimeo",
  youtube: "YouTube",
};

export const MAX_SPEAKING_SENTENCES_PER_PAGE = 10;

export const START_COLOR_UNCOMPLETED_LESSON = "rgba(0, 0, 0, 0.4)";

export const LESSON_CARD_STAR_COLORS = {
  UNCOMPLETED: "rgba(0, 0, 0, 0.4)",
  COMPLETED: "#ffb70a",
};

// --------------------------------------------
