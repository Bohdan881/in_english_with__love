import React from "react";
import ReactDOM from "react-dom";
import App from "./views/App";
import { Firebase, FirebaseContext } from "./firebase";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { persistor, store } from "./redux/store";

// style
import "./style/style.scss";
import "semantic-ui-css/semantic.min.css";
import "sweetalert2/src/sweetalert2.scss";

ReactDOM.render(
  <FirebaseContext.Provider loading={null} value={new Firebase()}>
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  </FirebaseContext.Provider>,
  document.getElementById("root")
);
