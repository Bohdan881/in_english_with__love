import { PasswordForgetPage, SignUpPage, SignInPage } from "../views/SignForm";
import About from "../views/About";
import HomePage from "../views/Home/Home";
import AccountPage from "../views/Account/AccountPage";
import AdminPage from "../views/Admin/AdminContainer";
import TopicList from "../views/TopicList/TopicList";
import LessonList from "../views/LessonList/LessonList";
import LessonView from "../views/LessonView/LessonViewContainer";
import PrivacyPolicy from "../views/PrivacyPolicy";
import Contact from "../views/Contact";

import * as ROUTES from "../constants/routes";

const routes = [
  {
    path: ROUTES.HOME,
    exact: true,
    name: "Home",
    component: HomePage,
  },
  {
    path: ROUTES.VOCABULARY,
    name: "Vocabulary",
    component: TopicList,
  },
  {
    path: ROUTES.PHRASAL_VERBS,
    name: "Phrasal verbs",
    component: TopicList,
  },
  {
    path: ROUTES.VIDEO,
    name: "Video",
    component: TopicList,
  },
  {
    path: ROUTES.LESSON_TOPIC_LIST,
    name: "Topics",
    component: LessonList,
  },
  {
    path: ROUTES.ABOUT,
    name: "About",
    component: About,
  },
  {
    path: ROUTES.ACCOUNT,
    name: "Account",
    component: AccountPage,
  },
  {
    path: ROUTES.ADMIN,
    name: "Admin",
    component: AdminPage,
  },
  {
    path: ROUTES.LESSON_TOPIC,
    name: "Lesson Topic",
    component: LessonView,
  },
  {
    path: ROUTES.PASSWORD_FORGET,
    name: "Forget Password",
    component: PasswordForgetPage,
  },
  {
    path: ROUTES.SIGN_IN,
    name: "Sign in",
    component: SignInPage,
  },
  {
    path: ROUTES.SIGN_UP,
    name: "Sign up",
    component: SignUpPage,
  },
  {
    path: ROUTES.PRIVACY_POLICY,
    component: PrivacyPolicy,
  },
  {
    path: ROUTES.CONTACT,
    component: Contact,
  },
];

export default routes;
