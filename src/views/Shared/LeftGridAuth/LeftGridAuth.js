import React from "react";
import { Grid, Image, Header } from "semantic-ui-react";

// style
import "./style.scss";

// assets
import logo from "../../../assets/images/default.png";

const LeftGridAuth = () => (
  <Grid.Column className="left-side-sign" computer={7} textAlign="right">
    <div className="left-side-sign__container">
      <div>
        <Header as="h2" textAlign="center">
          IN ENGLISH WITH <span className="style-love">LOVE</span>
        </Header>
      </div>
      <Image
        className="left-side-sign__image"
        src={logo}
        alt="logo"
        width="100%"
        height="100%"
      />
      <div>
        <Header as="h2" textAlign="center">
          LEARN NATURALLY
        </Header>
      </div>
    </div>
  </Grid.Column>
);

export default LeftGridAuth;
