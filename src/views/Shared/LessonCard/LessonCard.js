import React, { useState, useEffect } from "react";
import { Card, Icon, Image, Placeholder, Progress } from "semantic-ui-react";
import { LESSON_CARD_STAR_COLORS } from "../../../constants";

// style
import "./style.scss";

const LessonCard = ({
  currentId,
  title,
  img,
  date,
  isPreview,
  authUser = {},
}) => {
  const { lessonsCompleted = {}, lessonsInProgress = {} } = authUser || {};

  const [loading, setLoading] = useState(true);
  const [starColor, setStarColor] = useState(
    LESSON_CARD_STAR_COLORS.UNCOMPLETED
  );
  const [currentProgress, setCurrentProgress] = useState(0);
  const [currentCategoryLength, setCurrentCategoryLength] = useState(0);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, !!img);
  });

  useEffect(() => {
    if (!isPreview) {
      // set progress/completed values
      Object.entries(lessonsInProgress).length > 0 &&
        Object.entries(lessonsInProgress).forEach(
          ([lessonId, progressLessonData = {}]) => {
            const formattedLessonId = lessonId.slice(
              0,
              lessonId.indexOf("-category-")
            );

            if (formattedLessonId === currentId) {
              const {
                currentProgress,
                allChaptersNumber,
              } = calculateUserProgress(progressLessonData);

              setCurrentProgress(currentProgress);
              setCurrentCategoryLength(allChaptersNumber);
            }
          }
        );

      // set star color
      identifyStarColor();
    }

    // eslint-disable-next-line
  }, []);

  const identifyStarColor = () => {
    if (Object.entries(lessonsCompleted).length) {
      const filteredLesson = Object.keys(lessonsCompleted).filter(
        (completedLessonId = "") => {
          const formattedLessonId = completedLessonId.slice(
            0,
            completedLessonId.indexOf("-category-")
          );
          return !!currentId && formattedLessonId === currentId;
        }
      );

      // set star color if the lesson was completed
      if (filteredLesson.length) {
        setStarColor(LESSON_CARD_STAR_COLORS.COMPLETED);
      }
    }
  };

  const calculateUserProgress = (progressLessonData) => {
    let currentProgress = 0;
    let allChaptersNumber = 0;

    Object.entries(progressLessonData).forEach(([_, lessonValues]) => {
      if (lessonValues) {
        const {
          numberOfCompletedChapters,
          numberOfAllLessonChapters,
        } = lessonValues;

        if (numberOfCompletedChapters) {
          currentProgress = numberOfCompletedChapters;
        }

        if (numberOfAllLessonChapters) {
          allChaptersNumber = numberOfAllLessonChapters;
        }
      }
    });
    return { currentProgress, allChaptersNumber };
  };

  return (
    <Card className="lesson-card__container d-block m-auto h-100">
      <div className="lesson-card__image-container">
        {loading ? (
          <Placeholder className="lesson-card__placeholder">
            <Placeholder.Image square />
          </Placeholder>
        ) : (
          <Image
            src={img}
            wrapped
            ui={false}
            className={`m-auto bg-white ${
              img.includes("default") ? "lesson-card__default-image" : ""
            }`}
            alt="lesson-card"
          />
        )}
      </div>
      <div className="d-flex flex-row justify-content-between pt-1 pl-1 mb-1">
        <div className="d-flex flex-row">
          <Card.Meta>{date}</Card.Meta>
          <span className="lesson-card__icon-star">
            <Icon
              name="star"
              size="small"
              style={{ color: starColor }}
              fitted
            />
          </span>
        </div>
        {!!currentProgress && !!currentCategoryLength && (
          <div className="d-block w-50">
            <Progress
              className="lesson-card__progress-info mr-1 mb-0"
              value={currentProgress}
              total={currentCategoryLength}
              progress="ratio"
              success
            />
          </div>
        )}
      </div>

      <Card.Header as="h3">{title}</Card.Header>
    </Card>
  );
};

export default LessonCard;
