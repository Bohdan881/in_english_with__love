import React from "react";
import { Message } from "semantic-ui-react";

// style
import "./style.scss";

export const InfoMessage = ({
  header,
  content,
  size = "large",
  className = "",
}) => (
  <Message info size={size} className={`message-centered ${className}`}>
    {header && <Message.Header>{header}</Message.Header>}
    {content && <Message.Content>{content}</Message.Content>}
  </Message>
);

export const ErrorMessage = ({
  header = "Oops! something went wrong...",
  content,
  size = "large",
  className = "",
}) => (
  <Message error size={size} className={`message-centered ${className}`}>
    <Message.Header>{header}</Message.Header>
    {content && <Message.Content>{content}</Message.Content>}
  </Message>
);
