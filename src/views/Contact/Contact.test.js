import React from 'react';
import { shallow } from 'enzyme';
import Contact from './index';

describe('Contact', () => {
  it('should render correctly in "debug" mode', () => {
    const component = shallow(<Contact debug />);
  
    expect(component).toMatchSnapshot();
  });
});