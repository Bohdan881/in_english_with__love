import React from "react";
import App from "./index";
import { shallow } from "enzyme";

const enzymeWrapper = shallow(<App />);

describe("<App />", () => {
  test("should render successfully", () => {
    expect(enzymeWrapper).toBeDefined();
  });
});
