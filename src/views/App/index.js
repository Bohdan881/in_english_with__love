import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { withAuthentication } from "../../auth";
import { createBrowserHistory } from "history";
import * as ROUTES from "../../constants/routes";
import Navigation from "../Navigation/MainNavigation";
import Landing from "../Landing";
import Footer from "../Footer/Footer";
import routes from "../../routes";

const history = createBrowserHistory();

const App = () => (
  <Router history={history}>
    <Navigation />
    <Landing>
      <Switch>
        {routes.map((route) => (
          <Route
            key={route.path.slice(1)}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
        <Redirect to={ROUTES.HOME} />
      </Switch>
    </Landing>
    <Footer />
  </Router>
);

export default withAuthentication(App);
