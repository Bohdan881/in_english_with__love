import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { Link } from "react-router-dom";
import { withFirebase } from "../../firebase";
import { getAllLessons } from "../../redux/actions";
import {
  Grid,
  Card,
  Image,
  Icon,
  Loader,
  Segment,
  Dimmer,
  Header,
  Input,
  Transition,
} from "semantic-ui-react";
import { ErrorMessage, InfoMessage } from "../Shared/Message/Message";
import { LESSON_TOPIC_LIST } from "../../constants/routes";
import { LESSON_TYPE } from "../../constants";
import { assetsRoutes } from "./assetsRoutes";

// style
import "./style.scss";

// assets
import defaultImage from "../../assets/images/default.png";

class TopicList extends Component {
  state = {
    searchedTopic: "",
    searchTopicLoading: false,
    loading: false,
    error: false,
    currentCategory:
      LESSON_TYPE[
        window.location.href
          .slice(window.location.href.lastIndexOf("/") + 1)
          .split(/[^A-Za-z]/)
          .join("")
      ],
  };

  handleSearchChange = (e, { value }) => {
    const { currentCategory } = this.state;
    const { data } = this.props.lessons;
    this.setState({ searchTopicLoading: true, value });

    setTimeout(() => {
      if (this.state.value.length < 1) {
        this.setState({
          data: data[currentCategory],
          searchTopicLoading: false,
        });
      }

      this.setState({
        searchTopicLoading: false,
        data: Object.keys(data[currentCategory] || {})
          .filter(
            (item) => item && item.toLowerCase().includes(value.toLowerCase())
          )
          .reduce((obj, key) => {
            obj[key] = data[currentCategory][key];
            return obj;
          }, {}),
      });
    }, 300);
  };

  componentDidMount() {
    const { currentCategory } = this.state;
    const { data, error } = this.props.lessons;
    if (!data) {
      this.props.onGetAllLessons(this.props.firebase);
      this.setState({ loading: true });
    } else {
      this.setState({ data: data[currentCategory], loading: false, error });
    }
  }

  componentDidUpdate() {
    const { currentCategory } = this.state;
    const { data, loading, error } = this.props.lessons;

    if (!this.state.data && !error && data && data[currentCategory]) {
      this.setState({ data: data[currentCategory], loading, error });
    }
  }

  componentWillUnmount() {
    this.props.firebase.posts().off();
  }

  render() {
    const {
      searchTopicLoading,
      transitionDuration,
      data,
      loading,
      error,
    } = this.state;

    return loading ? (
      <Segment className="loader-whole-screen">
        <Dimmer active>
          <Loader size="massive">Loading </Loader>
        </Dimmer>
      </Segment>
    ) : error ? (
      <ErrorMessage content={error.message} />
    ) : (
      <Grid className="topic-list__grid">
        <Grid.Row columns={2} className="topic-list__header-row">
          <Grid.Column
            widescreen={8}
            largeScreen={8}
            computer={8}
            tablet={8}
            mobile={16}
            verticalAlign="bottom"
          >
            <Header className="topic-list__header" as="h2">
              Topics
            </Header>
          </Grid.Column>
          <Grid.Column
            className="d-flex justify-content-end pr-0"
            widescreen={8}
            largeScreen={8}
            computer={8}
            tablet={8}
            mobile={16}
          >
            <Input
              className="input-search"
              size="large"
              icon="search"
              placeholder="Search topics..."
              disabled={!Object.entries(data || {})}
              onChange={this.handleSearchChange}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row className="topic-list__cards-row">
          {searchTopicLoading ? (
            <Segment inverted className="w-100 bg-transparent">
              <Dimmer className="bg-transparent text-black" inverted active>
                <Loader className="w-100 mt-5" size="big">
                  Loading
                </Loader>
              </Dimmer>
            </Segment>
          ) : Object.entries(data || {}).length ? (
            Object.entries(data).map(([topic, lessons]) => {
              return (
                <Transition
                  visible={true}
                  animation="fade"
                  duration={transitionDuration}
                  transitionOnMount={true}
                  unmountOnHide={true}
                  key={topic}
                >
                  <Grid.Column
                    widescreen={4}
                    largeScreen={4}
                    computer={5}
                    tablet={8}
                    mobile={16}
                    className="topic-list__card-column"
                  >
                    <Link
                      to={`${LESSON_TOPIC_LIST}?category=${this.props.location.pathname.slice(
                        1
                      )}&topic=${!!topic && topic.split(" ").join("_")}`}
                    >
                      <Card fluid className="topic-list__card-container">
                        <Icon className="card-arrow" name="arrow right" />
                        <Card.Content className="topic-list__card-content">
                          <Card.Content className="topic-list__card-image">
                            <Image
                              src={assetsRoutes[topic] || defaultImage}
                              alt={topic}
                              floated="left"
                              size="tiny"
                            />
                          </Card.Content>
                          <Card.Content className="topic-list__card-text-container">
                            <Card.Header
                              as="h2"
                              className="topic-list__card-header"
                              textAlign="left"
                            >
                              {topic}
                            </Card.Header>
                            <Card.Meta>
                              <div>
                                <Icon name="pencil alternate" />
                                <span className="topic-list__card-lessons-length">
                                  {lessons.length || 0}
                                  {lessons.length && lessons.length === 1
                                    ? " Lesson "
                                    : " Lessons "}
                                </span>
                              </div>
                            </Card.Meta>
                          </Card.Content>
                        </Card.Content>
                      </Card>
                    </Link>
                  </Grid.Column>
                </Transition>
              );
            })
          ) : (
            <InfoMessage
              size="massive"
              header="No topics found..."
              content={
                <p>
                  <span className="capitalize">
                    {`${this.props.location.pathname
                      .split(/[^A-Za-z]/)
                      .join(" ")} category `}
                  </span>
                  doesn't have <b>"{`${this.state.value || `lessons yet`}`}"</b>
                </p>
              }
            />
          )}
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = ({ lessons }) => ({ lessons });

const mapDispatchToProps = (dispatch) => ({
  onGetAllLessons: (database) => dispatch(getAllLessons(database)),
});

export default compose(
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps)
)(TopicList);
