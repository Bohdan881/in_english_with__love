/* match subCategory with its own asset */
import Music from "../../assets/images/subCategories/music2.ico";
import Grammar from "../../assets/images/subCategories/grammar.ico";
import Vocabulary from "../../assets/images/subCategories/grammar.ico";
import Travel from "../../assets/images/subCategories/travel.ico";
import UnusualActivities from "../../assets/images/subCategories/travel.ico";
import Movies from "../../assets/images/subCategories/movies.ico";
import MindBody from "../../assets/images/subCategories/mind_body.ico";
import Philosophy from "../../assets/images/subCategories/philosophy.ico";
import Sustainability from "../../assets/images/subCategories/sustainability.ico";
import ArtAndCulture from "../../assets/images/subCategories/art_culture.ico";
import SocialIssues from "../../assets/images/subCategories/social_issues.jpg";
import NatureAndAnimals from "../../assets/images/subCategories/nature.jpg";
import Stories from "../../assets/images/build.jpg";

export const assetsRoutes = {
  Music,
  Grammar,
  Vocabulary,
  Travel,
  Movies,
  Sustainability,
  Philosophy,
  "Mind & Body": MindBody,
  "Art & Culture": ArtAndCulture,
  "Unusual Activities": UnusualActivities,
  Stories,
  "Social Issues": SocialIssues,
  "Nature & Animals": NatureAndAnimals,
  // "Food",
  // ocean
};
