import React, { PureComponent, Suspense } from "react";
import { compose } from "recompose";
import { connect } from "react-redux";
import {
  Grid,
  Icon,
  Segment,
  Button,
  Progress,
  Dimmer,
  Loader,
  Header,
  Label,
} from "semantic-ui-react";
import _ from "lodash";
import { setSessionValues, getAllLessons } from "../../redux/actions";
import { withFirebase } from "../../firebase";
import { confirmationAlert, infoAutoCloseAlert } from "../../utils/fireAlert";
import { HOME, SIGN_UP } from "../../constants/routes";
import routes from "../../routes";
import {
  AboutView,
  AnotherWay,
  ContentView,
  ClozeTest,
  FillInTheBlanksView,
  NewVocabulary,
  SpeakingView,
  WritingPractice,
} from "./Components";
import {
  USERS_BUCKET_NAME,
  CATEGORY_ID,
  LESSON_SEQUENCE_BY_CATEGORY,
  FILL_IN_THE_BLANKS,
  ANOTHER_WAY_TO_SAY,
  WORDS_IN_CONTEXT,
  DEFINITIONS,
  CLOZE,
  PREVIEW_MODE,
  CREATE_LESSON_STAGES,
  WRITING_PRACTICE,
  MULTIPLE_PAGES_CHAPTER,
  LESSON_TYPE,
  SPEAKING_PRACTICE,
  LESSON_FORMATTED_HEADERS,
} from "../../constants";
import {
  LESSON_COMPLETE_CONFIRMATION,
  SIGN_UP_SUGGESTION_ALERT_CONFIRMATION,
  QUIT_AND_SAVE_LESSON_CONFIRMATION,
  QUIT_ONLY_LESSON_CONFIRMATION,
  LESSON_PROGRESS_SAVE_CONFIRMATION,
  ERROR_ALERT,
} from "../../constants/alertContent";
import { InfoMessage, ErrorMessage } from "../Shared/Message/Message";

// style
import "./style.scss";

const SharedErrorMessage = ({ topic }) => (
  <ErrorMessage
    className="lesson-view__error-message"
    size="big"
    content={` Unfortunately,
        ${topic ? topic.split("-").join(" ") : "the lesson"}
        can't be loaded...`}
  />
);

class LessonView extends PureComponent {
  state = {
    formattedTitle: unescape(
      window.location.href.slice(window.location.href.lastIndexOf("/") + 1)
    ),
    fullLesson: {},
    currentChapter: "",
    currentStep: 1,
    isLoadingLesson: false,
    isPreviousDisabled: true,
    isNextDisabled: false,
    error: false,
    chaptersComponents: {
      About: AboutView,
      Content: ContentView,
      [SPEAKING_PRACTICE]: SpeakingView,
      [ANOTHER_WAY_TO_SAY]: AnotherWay,
      [FILL_IN_THE_BLANKS]: FillInTheBlanksView,
      [WORDS_IN_CONTEXT]: FillInTheBlanksView,
      [DEFINITIONS]: FillInTheBlanksView,
      [CLOZE]: ClozeTest,
      [WRITING_PRACTICE]: WritingPractice,
      [CREATE_LESSON_STAGES.vocabulary.key]: NewVocabulary,
    },
  };

  identifyLessonProgress = (currentLessonId) => {
    const { formattedTitle } = this.state;
    const { mode, sessionState } = this.props;
    const { lessonProgress, authUser } = sessionState;

    if (authUser && mode !== PREVIEW_MODE) {
      // check values in localStorage
      if (lessonProgress[formattedTitle]) {
        const completedChapters =
          lessonProgress[formattedTitle].numberOfCompletedChapters || 0;

        // delete unnecessary fields
        if (lessonProgress[formattedTitle].numberOfAllLessonChapters) {
          delete lessonProgress[formattedTitle].numberOfAllLessonChapters;
        }
        if (lessonProgress[formattedTitle].numberOfCompletedChapters) {
          delete lessonProgress[formattedTitle].numberOfCompletedChapters;
        }

        return {
          currentProgress: lessonProgress[formattedTitle],
          numberOfCompletedChapters: completedChapters,
        };
        // check values from DB if there's no values in localStorage
      } else if (Object.entries(authUser.lessonsInProgress || {}).length) {
        // const progress = Object.entries(
        //   authUser.lessonsInProgress || {}
        // ).filter(([lessonId]) => lessonId.includes(currentLessonId));
        const progress = Object.entries(authUser.lessonsInProgress).filter(
          ([progressLessonId]) => {
            const formattedLessonId = progressLessonId.slice(
              0,
              progressLessonId.indexOf("-category-")
            );
            return !!currentLessonId && formattedLessonId === currentLessonId;
          }
        );

        if (progress.length > 0 && progress[0].length >= 1) {
          let progressData = progress[0][1][formattedTitle];

          const completedChapters = progressData.numberOfCompletedChapters || 0;
          // delete unnecessary fields
          if (progressData.numberOfAllLessonChapters) {
            delete progressData.numberOfAllLessonChapters;
          }
          if (progressData.numberOfCompletedChapters) {
            delete progressData.numberOfCompletedChapters;
          }

          return {
            currentProgress: progressData,
            numberOfCompletedChapters: completedChapters,
          };
        }
      }
    }

    return { currentProgress: {}, numberOfCompletedChapters: 0 };
  };

  combineTheoreticalAndPracticalContent = (newLessonState) => {
    const { lessonPracticalContent, lessonTheoreticalContent } = newLessonState;

    let allLessonContent = { ...lessonTheoreticalContent };

    // inject practical content into allLessonsContent
    lessonPracticalContent &&
      lessonPracticalContent.forEach((el) => (allLessonContent[el.name] = el));

    return allLessonContent;
  };

  setSequenceOfChapters = (allLessonContent) => {
    /* Set Sequence */
    let initLessonChapterSequence = LESSON_SEQUENCE_BY_CATEGORY[
      this.idenfityCurrentCategory()
    ].filter((v) => Object.keys(allLessonContent).includes(v));

    /* Divide New Words/Verbs chapter into different pages 
       by multipling its values in the array 
       for example
       ['New Words'] => ['New Words', 'New Words', 'New Words']
       depending on the amount of words in the lesson. 
      */

    const multiplePageChapterIndex = initLessonChapterSequence.findIndex((v) =>
      MULTIPLE_PAGES_CHAPTER.includes(v)
    );

    const multiplePageChapterName = initLessonChapterSequence.find((v) =>
      MULTIPLE_PAGES_CHAPTER.includes(v)
    );

    let finalLessonChapterSequence = Array.from(initLessonChapterSequence);

    if (
      multiplePageChapterIndex !== -1 &&
      allLessonContent[multiplePageChapterName]
    ) {
      const arrayOfMultiplePageChapters = Array(
        allLessonContent[multiplePageChapterName].length
      ).fill(multiplePageChapterName);

      finalLessonChapterSequence.splice(
        multiplePageChapterIndex,
        1,
        ...arrayOfMultiplePageChapters
      );
    }

    /* Divide Speaking practice in separate pages 
       if it contains lots of questions 
    */

    let numberOfSpeakingPages = 1;

    const speakingContent = allLessonContent[SPEAKING_PRACTICE];

    if (speakingContent && speakingContent.length)
      numberOfSpeakingPages =
        speakingContent[speakingContent.length - 1].maxPage;

    if (numberOfSpeakingPages > 1) {
      const arrayOfSpeakingSections = Array(numberOfSpeakingPages).fill(
        SPEAKING_PRACTICE
      );

      const speakingSectionIndex = finalLessonChapterSequence.findIndex(
        (v) => v === SPEAKING_PRACTICE
      );

      finalLessonChapterSequence.splice(
        speakingSectionIndex,
        1,
        ...arrayOfSpeakingSections
      );
    }

    return finalLessonChapterSequence;
  };

  setSelectedLesson = (selectedLesson = {}) => {
    const { formattedTitle } = this.state;
    const { lessonProgress } = this.props.sessionState;

    if (Object.entries(selectedLesson).length) {
      // check if the lesson in progress or finished
      const {
        currentProgress,
        numberOfCompletedChapters,
      } = this.identifyLessonProgress(selectedLesson.uid);

      // clone the lesson to avoid immutability
      const clonedLesson = Object.assign({}, selectedLesson);

      // compbine theory and practice into one key
      clonedLesson.allLessonContent = this.combineTheoreticalAndPracticalContent(
        clonedLesson
      );

      const chapterSequence = this.setSequenceOfChapters(
        clonedLesson.allLessonContent
      );

      // sort the progress in the right order
      const sortedProgress = Object.keys(currentProgress || {}).sort(
        (a, b) => chapterSequence.indexOf(a) - chapterSequence.indexOf(b)
      );

      // Identify chapter based on the last user's progress
      const currentChapter =
        sortedProgress.length > 0
          ? sortedProgress[sortedProgress.length - 1]
          : chapterSequence[0];

      // Identify chapter index based on the current chapter
      const currentChapterIndex = chapterSequence.findIndex(
        (name) => name === currentChapter
      );

      this.setState({
        filteredLessonItems: chapterSequence,
        fullLesson: clonedLesson,
        currentChapter,
        startedSections: sortedProgress,
        currentStep: numberOfCompletedChapters
          ? numberOfCompletedChapters
          : currentChapterIndex,
        isNextDisabled: currentChapterIndex === chapterSequence.length - 1,
        isPreviousDisabled: currentChapterIndex === 0,
        isLoadingLesson: false,
      });

      // set session values
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [formattedTitle]: {
            ...lessonProgress[formattedTitle],
            ...currentProgress,
            [chapterSequence[0]]: true,
            numberOfAllLessonChapters: chapterSequence.length,
          },
        },
      });
    } else {
      this.setState({
        fullLesson: {},
        isLoadingLesson: false,
        error: { message: `${this.state.title} can't be visualized.` },
      });
    }
  };

  idenfityCurrentCategory = () => {
    const curLocation = window.location.href;
    const { category } = this.props.newLessonState;
    const { sectionKey } = this.props;

    return sectionKey === PREVIEW_MODE
      ? category &&
          category
            .toLowerCase()
            .split(/[^A-Za-z]/)
            .join("")
      : curLocation
          .slice(
            curLocation.indexOf("category=") + "category=".length,
            curLocation.lastIndexOf("topic") - 1
          )
          .toLowerCase()
          .split(/[^A-Za-z]/)
          .join("");
  };

  goToPreviousPage = () => {
    const currentRoute = routes.filter(
      ({ name = "" }) =>
        name
          .toLowerCase()
          .split(/[^A-Za-z]/)
          .join("") === this.idenfityCurrentCategory()
    );

    this.props.history.push(currentRoute[0] ? currentRoute[0].path : HOME);
  };

  visualizeChapterContent = (currentChapter) => {
    const {
      fullLesson = {},
      formattedTitle = "",
      chaptersComponents = {},
      startedSections = [],
      filteredLessonItems,
      currentStep,
    } = this.state;
    const { mode, sectionKey } = this.props;

    const lessonChapter = fullLesson.allLessonContent[currentChapter];

    const MatchedExerciseComponent = chaptersComponents[currentChapter];

    return lessonChapter ? (
      <Suspense
        fallback={
          <Segment className="lesson-view__loader">
            <Dimmer active>
              <Loader size="massive" inverted>
                Loading
              </Loader>
            </Dimmer>
          </Segment>
        }
      >
        <MatchedExerciseComponent
          lessonValues={lessonChapter}
          currentStep={currentStep}
          currentTopic={formattedTitle}
          chapterName={currentChapter}
          chapterSequence={filteredLessonItems}
          isSectionStarted={startedSections.includes(currentChapter)}
          mode={mode}
          contentVideoValues={fullLesson.contentVideoValues}
          assets={fullLesson.assets}
        />
      </Suspense>
    ) : sectionKey === PREVIEW_MODE ? (
      <InfoMessage
        className="lesson-view__error-message"
        header="You have nothing to preview."
      />
    ) : (
      <SharedErrorMessage currentTopic={currentChapter} />
    );
  };

  saveChapterProgress = (lessonProgress, chapterName) => {
    const { fullLesson, formattedTitle } = this.state;
    const { lessonTheoreticalContent } = fullLesson;

    const currentProgress = lessonProgress[formattedTitle][chapterName];

    // assign to true chapters that the user can only read
    if (Object.keys(lessonTheoreticalContent || {}).includes(chapterName)) {
      return true;
    } else {
      // save full user's progress if it's a practice exercise
      return currentProgress ? currentProgress : {};
    }
  };

  onNextChapter = () => {
    const { filteredLessonItems, formattedTitle, currentStep } = this.state;
    const { lessonProgress } = this.props.sessionState;

    const nextChapterIndex = currentStep + 1;
    const nextChapter = filteredLessonItems[nextChapterIndex];

    this.setState({
      currentStep: nextChapterIndex,
      isPreviousDisabled: nextChapterIndex === 0,
      isNextDisabled: nextChapterIndex === filteredLessonItems.length - 1,
      currentChapter: nextChapter,
    });

    this.props.onSetSessionValues({
      lessonProgress: {
        ...lessonProgress,
        [formattedTitle]: {
          ...lessonProgress[formattedTitle],
          [nextChapter]: this.saveChapterProgress(lessonProgress, nextChapter),
          // save completed chapter
          ...(nextChapterIndex >
            (lessonProgress[formattedTitle].numberOfCompletedChapters || 0) && {
            numberOfCompletedChapters: nextChapterIndex,
          }),
        },
      },
    });
  };

  onPreviousChapter = () => {
    const { filteredLessonItems, currentStep } = this.state;

    const prevStep = currentStep - 1;

    this.setState({
      currentStep: prevStep,
      isNextDisabled: false,
      isPreviousDisabled: prevStep === 0,
      currentChapter: filteredLessonItems[prevStep],
    });
  };

  sendCompletedLessonToDb = () => {
    const { fullLesson, formattedTitle } = this.state;
    const { uid, category } = fullLesson;
    const { sessionState } = this.props;
    const { authUser } = sessionState;
    const { lessonsInProgress, lessonsCompleted } = authUser || {};

    const lessonDatabaseKey = `${uid}${CATEGORY_ID}${category}`;

    // check if user is signed
    if (authUser) {
      // send the lesson to db if user hasn't completed it yet
      if (
        !lessonsCompleted ||
        !Object.keys(lessonsCompleted || {}).some((lessonId) =>
          lessonId.includes(uid)
        )
      ) {
        let clonedProgressLessons = _.cloneDeep(lessonsInProgress);

        // remove the lesson from progress object because it was completed
        if (clonedProgressLessons) {
          delete clonedProgressLessons[lessonDatabaseKey];
        }

        this.props.firebase.db
          .ref(`${USERS_BUCKET_NAME}/${authUser.uid}`)
          .update({
            ...authUser,
            ...(clonedProgressLessons && {
              lessonsInProgress: clonedProgressLessons,
            }),
            lessonsCompleted: {
              ...lessonsCompleted,
              [lessonDatabaseKey]: new Date().getTime(),
            },
          })
          .then(() => {
            this.props.onSetSessionValues({
              authUser: {
                ...authUser,
                lessonsCompleted: {
                  ...lessonsCompleted,
                  [uid]: new Date().getTime(),
                },
              },
              lessonProgress: {
                ...sessionState.lessonProgress,
                [lessonDatabaseKey]: {},
              },
            });

            infoAutoCloseAlert(LESSON_COMPLETE_CONFIRMATION).then(() => {
              // once the lesson completed, user will be taken to the previous page
              this.goToPreviousPage();
            });
          })
          .catch(({ text }) => infoAutoCloseAlert({ ...ERROR_ALERT, text }));
        // show the confirmation modal if user completed a lesson earlier
      } else {
        infoAutoCloseAlert(LESSON_COMPLETE_CONFIRMATION).then(() => {
          // once a lesson completed, a user will be taken to the home page
          this.goToPreviousPage();
          // remove the progress because the lesson has been created
          this.props.onSetSessionValues({
            lessonProgress: {
              ...sessionState.lessonProgress,
              [formattedTitle]: {},
            },
          });
        });
      }
    } else {
      confirmationAlert(SIGN_UP_SUGGESTION_ALERT_CONFIRMATION).then(
        ({ dismiss }) => {
          if (dismiss) {
            this.goToPreviousPage();
          } else {
            // set completed lesson to locale storage to push it in db later on
            localStorage.setItem(
              "firstCompletedLesson",
              JSON.stringify({
                [lessonDatabaseKey]: new Date().getTime(),
              })
            );

            // remove the progress because the lesson has been completed
            this.props.onSetSessionValues({
              lessonProgress: {
                ...sessionState.lessonProgress,
                [formattedTitle]: {},
              },
            });

            this.props.history.push(SIGN_UP);
          }
        }
      );
    }
  };

  isLessonCompleted = (currentLessonId) => {
    const { lessonsCompleted = {} } = this.props.sessionState.authUser || {};

    return (
      Object.entries(lessonsCompleted).length &&
      Object.keys(lessonsCompleted).some((lessonId) =>
        lessonId.includes(currentLessonId)
      )
    );
  };

  sendInProgressLessonToDb = () => {
    const { fullLesson, formattedTitle } = this.state;
    const { sessionState } = this.props;
    const { authUser } = sessionState;
    const lessonDatabaseKey = `${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`;

    // check if user signed up
    if (sessionState.authUser) {
      // check if user hasn't completed this lesson before

      if (!this.isLessonCompleted(fullLesson.uid)) {
        // send the lesson in progress to db if user hasn't finishied working on it yet
        const currentProgress = sessionState.lessonProgress[formattedTitle];

        this.props.firebase.db
          .ref(`${USERS_BUCKET_NAME}/${authUser.uid}`)
          .update({
            ...authUser,
            lessonsInProgress: {
              ...authUser.lessonsInProgress,
              [lessonDatabaseKey]: { [formattedTitle]: currentProgress },
            },
          })
          // update redux
          .then(() => {
            this.props.onSetSessionValues({
              authUser: {
                ...authUser,

                lessonsInProgress: {
                  ...authUser.lessonsInProgress,
                  [lessonDatabaseKey]: { [formattedTitle]: currentProgress },
                },
              },
            });
            // confirm that it was saved in db succesfully
            infoAutoCloseAlert(LESSON_PROGRESS_SAVE_CONFIRMATION).then(() => {
              // return user to the previous page
              this.goToPreviousPage();
            });
          })
          .catch(({ text }) => infoAutoCloseAlert({ ...ERROR_ALERT, text }));
      } else {
        // just go back and remove session progress if user already completed this lesson
        this.goToPreviousPage();
      }
    } else {
      // set the progress to the localStorage if user didn't sign up
      confirmationAlert(SIGN_UP_SUGGESTION_ALERT_CONFIRMATION).then(
        ({ dismiss }) => {
          if (dismiss) {
            this.goToPreviousPage();
          } else {
            // set completed lesson to locale storage to push in db later on
            localStorage.setItem(
              "firstLessonInProgress",
              JSON.stringify({
                [lessonDatabaseKey]: {
                  [formattedTitle]: {
                    ...sessionState.lessonProgress[formattedTitle],
                  },
                },
              })
            );

            this.props.history.push(SIGN_UP);
          }
        }
      );
    }
  };

  componentDidMount() {
    const { mode = "", newLessonState, sessionState } = this.props;
    const { data } = this.props.lessons;
    const { currentTopic, title } = sessionState.lessonInfo || {};

    const identifiedCurrentCategory =
      LESSON_TYPE[this.idenfityCurrentCategory()];

    this.setState({
      currentCategory: identifiedCurrentCategory,
      currentTopic,
      title,
      isLoadingLesson: true,
    });

    // if the lesson in preview mode, we upload the data from the redux
    if (mode && mode === PREVIEW_MODE) {
      this.setSelectedLesson(newLessonState);
    } else if (!data) {
      // fetch data if it doesn't exist atm
      this.props.onGetAllLessons(this.props.firebase);
    } else {
      // find a lesson by title and current category
      const currentLesson = data[identifiedCurrentCategory][
        currentTopic
      ].filter((obj) => obj.title === title);

      this.setSelectedLesson(currentLesson[0]);
    }
  }

  componentDidUpdate() {
    const { data } = this.props.lessons;
    const {
      currentCategory,
      currentTopic,
      title,
      fullLesson,
      error,
      isLoadingLesson,
    } = this.state;

    if (data && data[currentCategory])
      if (
        Object.entries(fullLesson).length === 0 &&
        !error &&
        data &&
        data[currentCategory] &&
        data[currentCategory][currentTopic]
      ) {
        const currentLesson = data[currentCategory][currentTopic].filter(
          (obj) => obj.title === title
        );
        this.setState({ isLoadingLesson: true });
        this.setSelectedLesson(currentLesson[0]);
      } else {
        if (isLoadingLesson) {
          this.setState({ isLoadingLesson: false });
        }
      }
  }

  componentWillUnmount() {
    this.props.firebase.posts().off();
  }

  render() {
    const {
      currentStep,
      formattedTitle,
      fullLesson,
      currentChapter,
      filteredLessonItems,
      isNextDisabled,
      isPreviousDisabled,
      isLoadingLesson,
      error,
      title,
    } = this.state;
    const { mode, sectionKey, sessionState } = this.props;

    const isLessonCompleted = this.isLessonCompleted(fullLesson.uid);
    const currentCategory = this.idenfityCurrentCategory();

    return isLoadingLesson && !error ? (
      <Segment className="lesson-view__loader">
        <Dimmer active>
          <Loader size="massive" inverted>
            Loading
          </Loader>
        </Dimmer>
      </Segment>
    ) : error && !isLoadingLesson ? (
      <div className="lesson-view__error-container">
        <ErrorMessage
          className="lesson-view__error-message"
          size="big"
          content={error.message}
        />
        <Button primary onClick={() => this.goToPreviousPage()}>
          Go Back
        </Button>
      </div>
    ) : !Object.entries(fullLesson || {}).length ? (
      <div className="lesson-view__error-container">
        {sectionKey === PREVIEW_MODE ? (
          <InfoMessage
            className="lesson-view__error-message"
            header="You have nothing to preview."
          />
        ) : (
          <SharedErrorMessage currentTopic={formattedTitle} />
        )}
        <Button primary onClick={() => this.goToPreviousPage()}>
          Go Back
        </Button>
      </div>
    ) : (
      <div
        className={`lesson-view__container${
          mode === PREVIEW_MODE ? "-preview" : ""
        }`}
      >
        {mode !== PREVIEW_MODE && (
          <>
            <Label
              color="blue"
              size="big"
              className="lesson-view__side-label-chapter"
            >
              {LESSON_FORMATTED_HEADERS[currentChapter]
                ? LESSON_FORMATTED_HEADERS[currentChapter][currentCategory]
                : currentChapter}
            </Label>

            <Label size="big" className="lesson-view__side-label-title">
              {title}
            </Label>
          </>
        )}
        <Grid
          className={` ${
            mode === PREVIEW_MODE
              ? "lesson-view__grid-preview"
              : "lesson-view__grid"
          }`}
        >
          <Grid.Row>
            <Grid.Column>
              <Progress
                className="lesson-view__progress-bar"
                value={currentStep + 1}
                total={filteredLessonItems ? filteredLessonItems.length : 1}
                progress="ratio"
                color={isNextDisabled ? "green" : "grey"}
              />
              <Button
                className="lesson-view__button-close"
                icon="close"
                size="medium"
                onClick={() => {
                  if (mode !== PREVIEW_MODE) {
                    /* 
                      Show deny button is user exists 
                      and lesson wasn't completed earlier
                    */

                    const showDenyButton = !!(
                      !isLessonCompleted && sessionState.authUser
                    );
                    const alertValue = showDenyButton
                      ? QUIT_AND_SAVE_LESSON_CONFIRMATION
                      : QUIT_ONLY_LESSON_CONFIRMATION;

                    confirmationAlert({ ...alertValue, showDenyButton }).then(
                      ({ isConfirmed, isDenied, isDismissed }) => {
                        // save progress to db
                        if (isConfirmed && !isLessonCompleted) {
                          this.sendInProgressLessonToDb();
                          // don't save it, just leave the page
                        } else if (
                          !isDismissed &&
                          (isDenied || isLessonCompleted)
                        ) {
                          this.goToPreviousPage();
                          this.props.onSetSessionValues({
                            lessonProgress: {
                              ...sessionState.lessonProgress,
                              [formattedTitle]: undefined,
                            },
                          });
                        }
                      }
                    );
                  }
                }}
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row className="pt-0">
            <Grid.Column>
              <Header
                as="h2"
                className={`lesson-view__task-header ${
                  mode === PREVIEW_MODE ? "d-block" : ""
                }`}
              >
                {LESSON_FORMATTED_HEADERS[currentChapter]
                  ? LESSON_FORMATTED_HEADERS[currentChapter][currentCategory]
                  : currentChapter}
              </Header>
              <div className="lesson-view__chapter-container">
                {this.visualizeChapterContent(currentChapter)}
              </div>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row className="mt-5">
            <Grid.Column>
              <div className="lesson-view__button-group">
                <Button
                  icon
                  basic
                  color="grey"
                  labelPosition="left"
                  disabled={isPreviousDisabled}
                  onClick={() => this.onPreviousChapter()}
                >
                  <Icon name="left arrow" />
                  Back
                </Button>

                <Button
                  basic
                  color="teal"
                  icon
                  labelPosition="right"
                  onClick={() => {
                    !isNextDisabled
                      ? this.onNextChapter()
                      : mode !== PREVIEW_MODE && this.sendCompletedLessonToDb();
                  }}
                  disabled={mode === PREVIEW_MODE && isNextDisabled}
                >
                  <span> {isNextDisabled ? "Finish Up" : "Next"}</span>
                  <Icon name="right arrow" />
                </Button>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({ lessons, newLessonState, sessionState }) => ({
  lessons,
  newLessonState,
  sessionState,
});

const mapDispatchToProps = (dispatch) => ({
  onSetSessionValues: (values) => dispatch(setSessionValues(values)),
  onGetAllLessons: (database) => dispatch(getAllLessons(database)),
});

export default compose(
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps)
)(LessonView);
