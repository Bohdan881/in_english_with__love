import React, { PureComponent } from "react";
import { compose } from "recompose";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { setSessionValues, getAllLessons } from "../../redux/actions";
import { withFirebase } from "../../firebase";
import routes from "../../routes";
import {
  USERS_BUCKET_NAME,
  CATEGORY_ID,
  LESSON_SECTIONS_DESCRIPTION,
  LESSON_SEQUENCE_BY_CATEGORY,
  FILL_IN_THE_BLANKS,
  ANOTHER_WAY_TO_SAY,
  WORDS_IN_CONTEXT,
  DEFINITIONS,
  CLOZE,
  PREVIEW_MODE,
  CREATE_LESSON_STAGES,
  WRITING_PRACTICE,
  MULTIPLE_PAGES_CHAPTERS,
  LESSON_TYPE,
  SPEAKING_PRACTICE,
} from "../../constants";
import {
  LESSON_COMPLETE_CONFIRMATION,
  CONFIRMATION_ALERT,
  SIGN_UP_SUGGESTION_CONFIRMATION,
  QUIT_AND_SAVE_LESSON_CONFIRMATION,
  QUIT_ONLY_LESSON_CONFIRMATION,
  LESSON_PROGRESS_SAVE_CONFIRMATION,
} from "../../constants/alertContent";
import {
  Grid,
  Icon,
  Segment,
  Button,
  Progress,
  Message,
  Dimmer,
  Loader,
  Header,
} from "semantic-ui-react";
import { fireAlert } from "../../utils";
import { HOME, SIGN_UP } from "../../constants/routes";
import {
  AnotherWay,
  WritingPractice,
  NewWords,
  ClozeTest,
  AboutView,
  SpeakingView,
  ContentView,
  FillInTheBlanksView,
} from "./Components";

// style
import "./style.scss";

const PreviewMessage = () => {
  return (
    <Message className="lesson-view-error-message" info>
      <Message.Header>You have nothing to preview.</Message.Header>
    </Message>
  );
};

const ErrorMessage = ({ currentTopic }) => {
  return (
    <Message className="lesson-view-error-message" size="big" negative>
      <Message.Header>Oops! Something went wrong...</Message.Header>
      <Message.Content>
        Unfortunately,
        {` ${currentTopic ? currentTopic.split("-").join(" ") : "the lesson"} `}
        can't be loaded...
      </Message.Content>
    </Message>
  );
};

class LessonView extends PureComponent {
  state = {
    formattedTitle: unescape(
      window.location.href.slice(window.location.href.lastIndexOf("/") + 1)
    ),
    isLoadingLesson: false,
    currentStep: 1,
    currentMultipleChapterIndex: 0,
    currentMultipleChapterPagesLength: 0,
    fullLesson: {},
    currentChapter: "",
    isPreviousDisabled: true,
    isNextDisabled: false,
    error: false,
    chaptersComponents: {
      About: AboutView,
      Content: ContentView,
      [SPEAKING_PRACTICE]: SpeakingView,
      [ANOTHER_WAY_TO_SAY]: AnotherWay,
      [FILL_IN_THE_BLANKS]: FillInTheBlanksView,
      [WORDS_IN_CONTEXT]: FillInTheBlanksView,
      [DEFINITIONS]: FillInTheBlanksView,
      [CLOZE]: ClozeTest,
      [WRITING_PRACTICE]: WritingPractice,
      [CREATE_LESSON_STAGES.phrasalverbs.key]: NewWords,
      [CREATE_LESSON_STAGES.newWords.key]: NewWords,
    },
  };

  setSelectedLesson = (selectedLesson = []) => {
    const { formattedTitle } = this.state;
    const { lessonProgress, authUser } = this.props.sessionState;

    const currentLessonId = selectedLesson[0] ? selectedLesson[0].uid : "";

    let initProgress = {};
    let initCurrentMultipleChapterIndex = 0;

    if (authUser && !this.isLessonCompleted()) {
      if (Object.entries(lessonProgress || {}) > 0) {
        const currentLessonProgress = Object.entries(lessonProgress).filter(
          ([lessonTopic]) => {
            return lessonTopic.toLowerCase() === formattedTitle.toLowerCase();
          }
        );
        // assign the latest progress
        if (
          currentLessonProgress.length > 0 &&
          currentLessonProgress[0].length >= 1
        ) {
          initProgress = lessonProgress[formattedTitle];
        }
      } else if (Object.entries(authUser.lessonsInProgress || {}).length > 0) {
        const clonedProgress = Array.from(
          Object.entries(authUser.lessonsInProgress || {})
        ).filter(([lessonId, lessonData]) => {
          return lessonId.indexOf(CATEGORY_ID) === -1
            ? lessonId
            : lessonId.slice(0, lessonId.indexOf(CATEGORY_ID)) ===
                currentLessonId;
        });
        // assign a progress if the user started the lesson before
        if (clonedProgress.length > 0 && clonedProgress[0].length >= 1) {
          initProgress = clonedProgress[0][1];

          Object.values(clonedProgress[0][1]).forEach((obj) => {
            Object.entries(obj).forEach(([chapterName, chapterData]) => {
              if (MULTIPLE_PAGES_CHAPTERS.includes(chapterName)) {
                initCurrentMultipleChapterIndex += +Object.keys(chapterData)[0];
              }
            });
          });
        }
      }
    }

    if (!!selectedLesson.length) {
      // clone the lesson to avoid immutability
      const clonedLesson = Object.assign({}, selectedLesson[0], {
        postAllContent: { ...selectedLesson[0].postTheoreticalContent },
      });

      // clone the exercises
      const exercises = Array.from(clonedLesson.postPracticalContent || []);

      if (!!exercises.length) {
        exercises.forEach(
          (obj) => (clonedLesson.postAllContent[obj.name] = obj)
        );
      }

      let initFilteredLessonItems =
        LESSON_SEQUENCE_BY_CATEGORY[this.idenfityCurrentCategory()];

      // add chapters with multple pages ------------------------------------
      const multiplePageChapterIndex = initFilteredLessonItems.findIndex((v) =>
        MULTIPLE_PAGES_CHAPTERS.includes(v)
      );

      const multiplePageChapterName = initFilteredLessonItems.find((v) =>
        MULTIPLE_PAGES_CHAPTERS.includes(v)
      );

      let finalFilteredItems = Array.from(initFilteredLessonItems);

      if (
        multiplePageChapterIndex !== -1 &&
        clonedLesson.postTheoreticalContent[multiplePageChapterName]
      ) {
        const arrayOfMultiplePageChapters = Array(
          clonedLesson.postTheoreticalContent[multiplePageChapterName].length
        ).fill(multiplePageChapterName);

        finalFilteredItems.splice(
          multiplePageChapterIndex,
          1,
          ...arrayOfMultiplePageChapters
        );

        this.setState({
          currentMultipleChapterPagesLength:
            arrayOfMultiplePageChapters.length - 1,
        });
      }
      // ------------------------------------

      // sort the progress in the right order
      const initProgressList = Object.keys(
        initProgress[formattedTitle] || {}
      ).sort(
        (a, b) => finalFilteredItems.indexOf(a) - finalFilteredItems.indexOf(b)
      );

      const currentChapter =
        initProgressList.length > 0
          ? initProgressList[initProgressList.length - 1]
          : finalFilteredItems[0];

      const findCurrentChapterIndex = finalFilteredItems.findIndex(
        (name) => name === currentChapter
      );

      this.setState({
        filteredLessonItems: finalFilteredItems,
        fullLesson: clonedLesson,
        isLoadingLesson: false,
        currentChapter,
        startedSections: initProgressList,
        currentStep: MULTIPLE_PAGES_CHAPTERS.includes(currentChapter)
          ? findCurrentChapterIndex + initCurrentMultipleChapterIndex + 1
          : findCurrentChapterIndex + 1,
        currentMultipleChapterIndex: initCurrentMultipleChapterIndex,
        isNextDisabled:
          findCurrentChapterIndex === finalFilteredItems.length - 1,
        isPreviousDisabled: findCurrentChapterIndex === 0,
      });

      // set session values
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [formattedTitle]: {
            ...lessonProgress[formattedTitle],
            ...initProgress[formattedTitle],
            [finalFilteredItems[0]]: true,
          },
        },
      });
    } else {
      this.setState({
        fullLesson: {},
        isLoadingLesson: false,
        error: { message: `${this.state.title} can't be visualized.` },
      });
    }
  };

  componentDidMount() {
    const { mode = "", newPostState, sessionState } = this.props;
    const { data } = this.props.lessons;
    const { currentTopic, title } = sessionState.lessonInfo || {};

    // const identifiedCurrentCategory =
    //   mode === PREVIEW_MODE || !currentCategory
    //     ? LESSON_TYPE[this.idenfityCurrentCategory()]
    //     : currentCategory;

    const identifiedCurrentCategory =
      LESSON_TYPE[this.idenfityCurrentCategory()];

    this.setState({
      currentCategory: identifiedCurrentCategory,
      currentTopic,
      title,
      isLoadingLesson: true,
    });

    if (mode && mode === PREVIEW_MODE) {
      this.setSelectedLesson([newPostState]);
    } else if (!data) {
      this.props.onGetAllLessons(this.props.firebase);
    } else {
      const currentLesson = data[identifiedCurrentCategory][
        currentTopic
      ].filter((obj) => obj.title === title);
      this.setSelectedLesson(currentLesson);
    }
  }

  componentDidUpdate() {
    const { data } = this.props.lessons;
    const {
      currentCategory,
      currentTopic,
      title,
      fullLesson,
      error,
    } = this.state;

    if (
      Object.entries(fullLesson).length === 0 &&
      !error &&
      data &&
      data[currentCategory] &&
      data[currentCategory][currentTopic]
    ) {
      const currentLesson = data[currentCategory][currentTopic].filter(
        (obj) => obj.title === title
      );

      this.setState({ isLoadingLesson: true });
      this.setSelectedLesson(currentLesson);
    }
  }

  idenfityCurrentCategory = () => {
    const curLocation = window.location.href;
    const { category } = this.props.newPostState;
    const { sectionKey } = this.props;

    return sectionKey === PREVIEW_MODE
      ? !!category &&
          category
            .toLowerCase()
            .split(/[^A-Za-z]/)
            .join("")
      : curLocation
          .slice(
            curLocation.indexOf("category=") + "category=".length,
            curLocation.lastIndexOf("topic") - 1
          )
          .toLowerCase()
          .split(/[^A-Za-z]/)
          .join("");
  };

  goToPreviousPage = () => {
    const currentRoute = routes.filter(
      ({ name = "" }) =>
        name
          .toLowerCase()
          .split(/[^A-Za-z]/)
          .join("") === this.state.currentCategory
    );

    this.props.history.push(currentRoute[0] ? currentRoute[0].path : HOME);
  };

  setCurrentChapter = (chapter) => {
    const { filteredLessonItems, currentMultipleChapterIndex } = this.state;
    const findCurrentChapterIndex = filteredLessonItems.findIndex(
      (name) => name === chapter
    );

    this.setState({
      currentChapter: chapter,
      currentStep: findCurrentChapterIndex + 1,
      currentMultipleChapterIndex: MULTIPLE_PAGES_CHAPTERS.some(
        (v) => v === chapter
      )
        ? currentMultipleChapterIndex + 1
        : currentMultipleChapterIndex,
      isNextDisabled:
        findCurrentChapterIndex === filteredLessonItems.length - 1,
      isPreviousDisabled: findCurrentChapterIndex === 0,
    });
  };

  visualizeChapterContent = (currentChapter) => {
    const {
      fullLesson = {},
      formattedTitle = "",
      chaptersComponents = {},
      startedSections = [],
      currentMultipleChapterIndex,
    } = this.state;
    const { mode, sectionKey } = this.props;

    const lessonChapter = fullLesson.postAllContent[currentChapter];

    const MatchedExerciseComponent = chaptersComponents[currentChapter];

    return !!lessonChapter ? (
      <MatchedExerciseComponent
        lessonValues={lessonChapter}
        currentTopic={formattedTitle}
        chapterName={currentChapter}
        isSectionStarted={startedSections.includes(currentChapter)}
        mode={mode}
        currentMultipleChapterIndex={currentMultipleChapterIndex}
        fullLesson={fullLesson}
      />
    ) : sectionKey === PREVIEW_MODE ? (
      <PreviewMessage />
    ) : (
      <ErrorMessage currentTopic={currentChapter} />
    );
  };

  componentWillMount() {
    this.props.firebase.posts().off();
  }

  onNextChapter = () => {
    const {
      filteredLessonItems,
      currentChapter,
      formattedTitle,
      fullLesson,
      currentMultipleChapterIndex,
      currentMultipleChapterPagesLength,
    } = this.state;
    const { lessonProgress } = this.props.sessionState;

    // find index and set current step
    let findCurrentChapterIndex = filteredLessonItems.findIndex(
      (chapter) => chapter === currentChapter
    );

    // check if it's a multiple pages chapter
    const isMultiplePagesChapter = MULTIPLE_PAGES_CHAPTERS.some(
      (v) => v === currentChapter
    );

    if (isMultiplePagesChapter) {
      findCurrentChapterIndex += currentMultipleChapterIndex;
    }

    const nextChapterIndex =
      findCurrentChapterIndex === filteredLessonItems.length - 1
        ? filteredLessonItems.length - 1
        : findCurrentChapterIndex + 1;

    const nextChapter = filteredLessonItems[nextChapterIndex];

    this.setState({
      currentStep: nextChapterIndex + 1,
      currentMultipleChapterIndex:
        isMultiplePagesChapter &&
        currentMultipleChapterIndex < currentMultipleChapterPagesLength
          ? currentMultipleChapterIndex + 1
          : currentMultipleChapterIndex,
      isPreviousDisabled: nextChapterIndex === 0,
      isNextDisabled: nextChapterIndex === filteredLessonItems.length - 1,
      currentChapter: nextChapter,
    });

    // set to true topics which a user can just react like about/before
    this.props.onSetSessionValues({
      lessonProgress: {
        ...lessonProgress,
        [formattedTitle]: {
          ...lessonProgress[formattedTitle],
          [nextChapter]: Object.keys(
            fullLesson.postLocalStorage || {}
          ).includes(nextChapter)
            ? true
            : lessonProgress[formattedTitle][nextChapter] || {},
        },
      },
    });
  };

  onPreviousChapter = () => {
    const {
      filteredLessonItems,
      currentChapter,
      currentMultipleChapterIndex,
    } = this.state;

    // find an index and set up the current step
    let findCurrentChapterIndex = filteredLessonItems.findIndex(
      (chapter) => chapter === currentChapter
    );

    const isMultiplePagesChapter = MULTIPLE_PAGES_CHAPTERS.some(
      (v) => v === currentChapter
    );

    if (isMultiplePagesChapter && currentMultipleChapterIndex > 0) {
      findCurrentChapterIndex += currentMultipleChapterIndex;
    }

    const previousChapterIndex =
      findCurrentChapterIndex > 0 ? findCurrentChapterIndex - 1 : 0;

    this.setState({
      currentStep: previousChapterIndex === 0 ? 1 : previousChapterIndex + 1,
      currentMultipleChapterIndex:
        isMultiplePagesChapter && currentMultipleChapterIndex > 0
          ? currentMultipleChapterIndex - 1
          : currentMultipleChapterIndex,
      isNextDisabled: false,
      isPreviousDisabled: previousChapterIndex === 0,
      currentChapter: filteredLessonItems[previousChapterIndex],
    });
  };

  sendCompletedLessonToDb = () => {
    const { fullLesson, formattedTitle } = this.state;
    const { sessionState } = this.props;

    if (sessionState.authUser) {
      // send the lesson to db if the user hasn't completed it yet
      if (
        !sessionState.authUser.lessonsCompleted ||
        !Object.keys(
          sessionState.authUser.lessonsCompleted || {}
        ).some((lessonId) => lessonId.includes(fullLesson.uid))
      ) {
        let clonedProgressLessons = {
          ...sessionState.authUser.lessonsInProgress,
        };
        // remove the lesson from progress object because it was completed
        delete clonedProgressLessons[
          `${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`
        ];

        this.props.firebase.db
          .ref(`${USERS_BUCKET_NAME}/${sessionState.authUser.uid}`)
          .update({
            ...sessionState.authUser,
            lessonsInProgress: clonedProgressLessons,
            lessonsCompleted: {
              ...sessionState.authUser.lessonsCompleted,
              [`${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`]: new Date().getTime(),
            },
          })
          .then(() => {
            this.props.onSetSessionValues({
              authUser: {
                ...sessionState.authUser,
                lessonsCompleted: {
                  ...sessionState.authUser.lessonsCompleted,

                  [fullLesson.uid]: new Date().getTime(),
                },
              },
              lessonProgress: {
                ...sessionState.lessonProgress,
                [`${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`]: {},
              },
            });
            fireAlert({
              state: true,
              values: LESSON_COMPLETE_CONFIRMATION,
            }).then(() => {
              // once the lesson completed, the user will be taken to the home page
              this.props.history.push(HOME);
            });
          })
          .catch((error) => {
            let values = LESSON_COMPLETE_CONFIRMATION;
            values.text.error = error.text;
            fireAlert({ state: false, values });
          });
        // just show the confirmation modal if a user has already completed a lesson
      } else {
        fireAlert({
          state: true,
          values: LESSON_COMPLETE_CONFIRMATION,
        }).then(() => {
          // once a lesson completed, a user will be taken to the home page
          this.goToPreviousPage();
          // remove the progress because the lesson has been created
          this.props.onSetSessionValues({
            lessonProgress: {
              ...sessionState.lessonProgress,
              [formattedTitle]: {},
            },
          });
        });
      }
    } else {
      fireAlert({
        state: true,
        type: CONFIRMATION_ALERT,
        values: SIGN_UP_SUGGESTION_CONFIRMATION,
      }).then((response) => {
        if (response.dismiss) {
          this.props.history.push(HOME);
        } else {
          // set completed lesson to locale storage to push in db later on
          localStorage.setItem(
            "firstCompletedLesson",
            JSON.stringify({
              [`${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`]: new Date().getTime(),
            })
          );

          // remove the progress because the lesson has been completed
          this.props.onSetSessionValues({
            lessonProgress: {
              ...sessionState.lessonProgress,
              [formattedTitle]: {},
            },
          });

          this.props.history.push(SIGN_UP);
        }
      });
    }
  };

  isLessonCompleted = () => {
    const { fullLesson } = this.state;
    const { lessonsCompleted = {} } = this.props.sessionState.authUser || {};
    return (
      Object.entries(lessonsCompleted).length > 0 &&
      Object.keys(lessonsCompleted).some((lessonId) =>
        lessonId.includes(fullLesson.uid)
      )
    );
  };

  sendInProgressLessonToDb = () => {
    const { fullLesson, formattedTitle } = this.state;
    const { sessionState } = this.props;

    // check if the user signed up
    if (sessionState.authUser) {
      // check if the user hasn't completed this lesson before
      if (!this.isLessonCompleted()) {
        // send a lesson in progress to db if a user hasn't started working on it yet
        const clonedProgress = Object.assign(
          sessionState.lessonProgress[formattedTitle],
          {}
        );

        this.props.firebase.db
          .ref(`${USERS_BUCKET_NAME}/${sessionState.authUser.uid}`)
          .update({
            ...sessionState.authUser,
            lessonsInProgress: {
              ...sessionState.authUser.lessonsInProgress,
              [`${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`]: {
                [formattedTitle]: clonedProgress,
              },
            },
          })
          .then(() => {
            this.props.onSetSessionValues({
              authUser: {
                ...sessionState.authUser,

                lessonsInProgress: {
                  ...sessionState.authUser.lessonsInProgress,
                  [`${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`]: {
                    [formattedTitle]: clonedProgress,
                  },
                },
              },
            });
            fireAlert({
              state: true,
              values: LESSON_PROGRESS_SAVE_CONFIRMATION,
            }).then(() => {
              this.goToPreviousPage();
            });
          })
          .catch((error) => {
            let values = LESSON_PROGRESS_SAVE_CONFIRMATION;
            values.text.error = error.text;
            fireAlert({ state: false, values });
          });
      } else {
        // just go back and remove session progress if a user already completed this lesson
        this.goToPreviousPage();
      }
    } else {
      // set the progress to the localStorage if a user didn't sign up
      fireAlert({
        state: true,
        type: CONFIRMATION_ALERT,
        values: SIGN_UP_SUGGESTION_CONFIRMATION,
      }).then((response) => {
        if (response.dismiss) {
          this.goToPreviousPage();
        } else {
          // set completed lesson to locale storage to push in db later on
          localStorage.setItem(
            "firstLessonInProgress",
            JSON.stringify({
              [`${fullLesson.uid}${CATEGORY_ID}${fullLesson.category}`]: {
                [formattedTitle]: {
                  ...sessionState.lessonProgress[formattedTitle],
                },
              },
            })
          );

          this.props.history.push(SIGN_UP);
        }
      });
    }
  };

  render() {
    const {
      currentStep,
      formattedTitle,
      fullLesson,
      currentChapter,
      filteredLessonItems,
      isNextDisabled,
      isPreviousDisabled,
      isLoadingLesson,
      error,
    } = this.state;
    const { mode, sectionKey, sessionState } = this.props;

    const currentCategory = this.idenfityCurrentCategory();

    const isLessonCompleted = this.isLessonCompleted();

    return isLoadingLesson && !error ? (
      <Segment className="lesson-view-loader">
        <Dimmer active>
          <Loader size="massive" inverted>
            Loading
          </Loader>
        </Dimmer>
      </Segment>
    ) : error && !isLoadingLesson ? (
      <Message className="lesson-view-error-message" size="big" negative>
        <Message.Header>Oops! Something went wrong...</Message.Header>
        <Message.Content>{error.message}</Message.Content>
      </Message>
    ) : !Object.entries(fullLesson || {}).length ? (
      <div className="lesson-view-error-container">
        {sectionKey === PREVIEW_MODE ? (
          <PreviewMessage />
        ) : (
          <ErrorMessage currentTopic={formattedTitle} />
        )}
        <Button primary>
          <Link to={HOME}>Go Back Home</Link>
        </Button>
      </div>
    ) : (
      <div
        className={`lesson-view-wrapper${
          mode === PREVIEW_MODE ? "-preview" : ""
        }`}
      >
        {/* {mode !== PREVIEW_MODE && (
          <div className="lesson-view-description-container">
            <div className="lesson-view-description-block">
              <p className="lesson-view-title">{fullLesson.title}</p>
            </div>
          </div>
        )} */}
        <Grid
          className={`lesson-view-grid  lesson-view-grid-${
            mode === PREVIEW_MODE ? "preview" : ""
          }`}
        >
          <Grid.Row>
            <Grid.Column className="lesson-view-progress-bar-container">
              <Button
                className="lesson-view-close"
                icon="close"
                size="medium"
                onClick={() => {
                  fireAlert({
                    state: true,
                    denyButton: !isLessonCompleted && !!sessionState.authUser,
                    type: CONFIRMATION_ALERT,
                    values:
                      isLessonCompleted || !sessionState.authUser
                        ? QUIT_ONLY_LESSON_CONFIRMATION
                        : QUIT_AND_SAVE_LESSON_CONFIRMATION,
                  }).then(({ isConfirmed, isDenied }) => {
                    if (isConfirmed && !isLessonCompleted) {
                      this.sendInProgressLessonToDb();
                    } else if (isDenied || isLessonCompleted) {
                      this.goToPreviousPage();
                      this.props.onSetSessionValues({
                        lessonProgress: {
                          ...sessionState.lessonProgress,
                          [formattedTitle]: undefined,
                        },
                      });
                    }
                  });
                }}
              />
              <Progress
                className="lesson-view-progress-bar"
                value={currentStep}
                total={filteredLessonItems ? filteredLessonItems.length : 1}
                progress="ratio"
                color={isNextDisabled ? "green" : "grey"}
              />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column>
              <Header as="h3" className="lesson-view-label">
                {(!!LESSON_SECTIONS_DESCRIPTION[currentCategory] &&
                  LESSON_SECTIONS_DESCRIPTION[currentCategory][
                    currentChapter
                  ]) ||
                  currentChapter}
              </Header>
              <div
                className={`chapter-block 
                    chapter-${
                      mode === PREVIEW_MODE
                        ? currentChapter
                          ? `${currentChapter.toLocaleLowerCase()}-preview`
                          : "preview"
                        : currentChapter
                        ? currentChapter.toLocaleLowerCase()
                        : null
                    }
                   
                    `}
              >
                {this.visualizeChapterContent(currentChapter)}
              </div>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row className="lesson-view-button-group-row">
            <Grid.Column>
              <div className="lesson-view-button-group">
                <Button
                  icon
                  basic
                  color="grey"
                  labelPosition="left"
                  disabled={isPreviousDisabled}
                  onClick={() => this.onPreviousChapter()}
                >
                  <Icon name="left arrow" />
                  Back
                </Button>

                <Button
                  basic
                  color="teal"
                  icon
                  labelPosition="right"
                  onClick={() => {
                    !isNextDisabled
                      ? this.onNextChapter()
                      : mode !== PREVIEW_MODE && this.sendCompletedLessonToDb();
                  }}
                  disabled={mode === PREVIEW_MODE && isNextDisabled}
                >
                  <span> {isNextDisabled ? "Finish Up" : "Next"}</span>
                  <Icon name="right arrow" />
                </Button>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { lessons, newPostState, sessionState } = state;
  return { lessons, newPostState, sessionState };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSetSessionValues: (values) => dispatch(setSessionValues(values)),
    onGetAllLessons: (database) => dispatch(getAllLessons(database)),
  };
};

export default compose(
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps)
)(LessonView);
