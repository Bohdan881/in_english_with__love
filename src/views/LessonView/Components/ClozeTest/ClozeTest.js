import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Header, Icon, Input, Popup } from "semantic-ui-react";
import { setSessionValues } from "../../../../redux/actions";
import { EXERCISES_DESCRIPTIONS, PREVIEW_MODE } from "../../../../constants";
import { ErrorMessage } from "../../../Shared/Message/Message";
import { shuffleArray } from "../../../../utils";

const ClozeTest = ({
  lessonValues,
  currentTopic,
  chapterName,
  mode,
  isSectionStarted,
}) => {
  // redux ---------------------------------
  const { sessionState } = useSelector((db) => db);

  const dispatch = useDispatch();
  // ----------------------------------------

  // states ---------------------------------

  const [answersValues, setAnswersValues] = useState([]);
  // ----------------------------------------

  // handlers -------------------------------

  const setInitValues = () => {
    const { lessonProgress } = sessionState;

    const answers =
      lessonValues.content &&
      lessonValues.content[0] &&
      lessonValues.content[0].answers;

    dispatch(
      setSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              lessonId: lessonValues.id,
              content: lessonValues.content.map((obj) => ({
                ...obj,
                ...(!isSectionStarted && {
                  isCorrect: answers && answers.map(() => false),
                }),
                ...(!isSectionStarted && {
                  userValues: answers && answers.map(() => ""),
                }),
              })),
              exerciseDescription: EXERCISES_DESCRIPTIONS[lessonValues.name]
                ? EXERCISES_DESCRIPTIONS[lessonValues.name][0].text
                : null,
              answerList: answers && answers.map((answer) => answer.trim()),
            },
          },
        },
      })
    );
  };

  const onChangeUserValues = (value, id) => {
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content && content.length && content[0].userValues) {
      dispatch(
        setSessionValues({
          lessonProgress: {
            ...lessonProgress,
            [currentTopic]: {
              ...lessonProgress[currentTopic],
              [chapterName]: {
                ...lessonProgress[currentTopic][chapterName],
                content: [
                  {
                    ...content[0],
                    userValues: content[0].userValues.map(
                      (currentUserValue, key) =>
                        key === id ? value : currentUserValue
                    ),
                  },
                ],
              },
            },
          },
        })
      );
    }
  };

  const checkTask = (isDisabled) => {
    const { lessonProgress } = sessionState;
    const { content = [] } = lessonProgress[currentTopic][chapterName];

    if (!isDisabled && content.length) {
      dispatch(
        setSessionValues({
          lessonProgress: {
            ...lessonProgress,
            [currentTopic]: {
              ...lessonProgress[currentTopic],
              [chapterName]: {
                ...lessonProgress[currentTopic][chapterName],
                isChecked: true,

                content: [
                  {
                    ...content[0],
                    isCorrect:
                      content[0] &&
                      content[0].isCorrect.map(
                        (_, key) =>
                          content[0].userValues[key].toLowerCase().trim() ===
                          content[0].answers[key].toLowerCase().trim()
                      ),
                  },
                ],
              },
            },
          },
        })
      );
    }
  };

  const showSolution = () => {
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      dispatch(
        setSessionValues({
          lessonProgress: {
            ...lessonProgress,
            [currentTopic]: {
              ...lessonProgress[currentTopic],
              [chapterName]: {
                ...lessonProgress[currentTopic][chapterName],
                isShowingSolution: true,
                isChecked: true,
                content: [
                  {
                    ...content[0],
                    isCorrect: content[0].isCorrect.map(() => true),
                    userValues: content[0].answers,
                  },
                ],
              },
            },
          },
        })
      );
    }
  };

  const retryTask = () => {
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      dispatch(
        setSessionValues({
          lessonProgress: {
            ...lessonProgress,
            [currentTopic]: {
              ...lessonProgress[currentTopic],
              [chapterName]: {
                ...lessonProgress[currentTopic][chapterName],
                isShowingSolution: false,
                isChecked: false,
                content: [
                  {
                    ...content[0],
                    userValues: content[0].answers.map(() => ""),
                  },
                ],
              },
            },
          },
        })
      );
    }
  };
  // ----------------------------------------

  // effects --------------------------------

  useEffect(() => {
    const topic = sessionState.lessonProgress[currentTopic];
    // set init values if a user hasn't started an exercise yet
    if (lessonValues) {
      if (mode === PREVIEW_MODE) {
        setInitValues();
      } else if (!topic || !Object.entries(topic[chapterName] || {}).length) {
        setInitValues();
      }
    }

    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (
      answersValues.length === 0 &&
      lessonValues.content &&
      lessonValues.content[0]
    ) {
      setAnswersValues(
        shuffleArray(Array.from(lessonValues.content[0].answers))
      );
    }
    // eslint-disable-next-line
  }, []);

  // ----------------------------------------

  // constants ------------------------------
  const {
    content = [],
    isChecked = false,
    isShowingSolution = false,
    exerciseDescription = "",
    answerList = [],
  } = sessionState.lessonProgress[currentTopic]
    ? sessionState.lessonProgress[currentTopic][chapterName]
    : {};

  // a user can check a result after he/she fills all the fields
  const isCheckedDisabled =
    content.length && content[0].userValues.some((v) => v.trim() === "");

  // ----------------------------------------

  const wordsInCurlyBraces = /\{.*?\}/g;
  const wordsInNormalBraces = /\(.*?\)/g;

  const contentText = content && content[0] && content[0].text;
  // find answers
  const answerValues =
    (contentText && contentText.match(wordsInCurlyBraces)) || [];

  // find tooltips

  const tooltipValues =
    (contentText && contentText.match(wordsInNormalBraces)) || [];

  // clone text
  let clonedText = content && content[0] && content[0].text;

  // replace answers with stars
  answerValues.length &&
    answerValues.forEach((rx) => (clonedText = clonedText.replace(rx, "** ")));

  // remove tooltips
  tooltipValues.length &&
    tooltipValues.forEach(
      (rx) => (clonedText = clonedText.replace(` ${rx}`, ``))
    );

  clonedText = clonedText && clonedText.split("**");

  return content.length ? (
    <div>
      <Header as="h2">{exerciseDescription}</Header>

      <div className="lesson-view__exercise-answers-container">
        {answerList && answerList.length > 0 && (
          <div className="lesson-view-exercise__answers-list">
            {answersValues.map((answerWord) => {
              const isWordWritten = content[0].userValues.some(
                (userValue = "") => userValue.trim() === answerWord.trim()
              );

              return (
                <div
                  key={answerWord}
                  className="lesson-view__exercise-answer-field noselect"
                  style={{
                    textDecoration: isWordWritten ? "line-through" : "",
                  }}
                >
                  {answerWord.toLowerCase()}
                </div>
              );
            })}
          </div>
        )}
      </div>
      <div>
        {clonedText &&
          clonedText.length &&
          clonedText.map((text, key) => (
            <React.Fragment key={key}>
              <span
                dangerouslySetInnerHTML={{
                  __html: text,
                }}
              />
              {key <= answerList.length - 1 && (
                <Input
                  className={`lesson-view-exercise__input  ${chapterName.toLowerCase()}-answer-input ${
                    isChecked
                      ? content &&
                        content[0] &&
                        content[0] &&
                        content[0].isCorrect[key]
                        ? "lesson-view-exercise__input--correct"
                        : "lesson-view-exercise__input--incorrect"
                      : ""
                  }
                  `}
                  disabled={isChecked}
                  value={content && content[0] && content[0].userValues[key]}
                  /* placeholder={
                    tooltipValues[key] &&
                    tooltipValues[key].slice(1, tooltipValues[key].length - 1)
                  } */
                  onChange={(e, { value }) => onChangeUserValues(value, key)}
                />
              )}
            </React.Fragment>
          ))}
      </div>
      {isChecked ? (
        <div className="lesson-view-exercise__check-container">
          <Button
            primary
            className="lesson-view-exercise__check-button"
            onClick={retryTask}
          >
            Retry
            <Icon
              fitted
              className="lesson-view-exercise__button-icon"
              name="repeat"
            />
          </Button>
          <Button
            disabled={isShowingSolution}
            color="teal"
            className="lesson-view-exercise__check-button"
            onClick={showSolution}
          >
            Solution
            <Icon
              fitted
              className="lesson-view-exercise__button-icon"
              name={isShowingSolution ? "lock open" : "lock"}
            />
          </Button>
        </div>
      ) : (
        <div className="lesson-view-exercise__check-container">
          <Popup
            inverted
            on="hover"
            disabled={!isCheckedDisabled}
            content={"Please fill all the empty fields to check your result."}
            trigger={
              <Button
                disabled={
                  content && content[0] && content[0].userValues.includes("")
                }
                primary
                onClick={() => checkTask(isCheckedDisabled)}
                className={` lesson-view-exercise__check-button ${
                  isCheckedDisabled ? "button-disabled" : ""
                }  `}
              >
                Check
                <Icon
                  fitted
                  className="lesson-view-exercise__button-icon"
                  name="check"
                />
              </Button>
            }
          />
        </div>
      )}
    </div>
  ) : (
    <ErrorMessage
      size="big"
      content="Unfortunately this exercise can't be loaded."
    />
  );
};

export default ClozeTest;
