import React from "react";
import { Header } from "semantic-ui-react";
import { MULTIPLE_PAGES_CHAPTER, PREVIEW_MODE } from "../../../../constants";
import { ErrorMessage, InfoMessage } from "../../../Shared/Message/Message";

// style
import "./style.scss";

const NewWords = ({
  lessonValues = [],
  mode,
  currentStep,
  chapterSequence,
}) => {
  const chapterIndex = chapterSequence.findIndex((v) =>
    MULTIPLE_PAGES_CHAPTER.includes(v)
  );

  const currentIndex = currentStep - chapterIndex;

  const { header, description, examples, structures } =
    lessonValues[currentIndex] || {};

  return header ? (
    <div key={header}>
      <Header as="h1" className="capitalize italic">
        {`${currentIndex + 1}. ${header}`}
      </Header>
      <p>{description}</p>
      {examples && examples.length && (
        <>
          <Header as="h2" className="capitalize">
            Examples
          </Header>
          {examples.map(({ exampleId, example }) => (
            <p key={exampleId} dangerouslySetInnerHTML={{ __html: example }} />
          ))}
        </>
      )}
      {structures && structures.length && (
        <>
          <Header as="h2" className="new-vocabulary__structures capitalize">
            Common Structures
          </Header>
          {structures.map(
            ({ structureId, firstWord, secondWord, structureExample }) => (
              <p key={structureId}>
                <span className="italic">{firstWord} </span>
                <span className="new-vocabulary__arrow-character">+</span>
                <span className="italic">{secondWord} </span>
                <span className="new-vocabulary__arrow-character">→</span>
                <span
                  className="italic"
                  dangerouslySetInnerHTML={{ __html: structureExample }}
                />
              </p>
            )
          )}
        </>
      )}
    </div>
  ) : mode === PREVIEW_MODE ? (
    <InfoMessage
      className="lesson-view__error-message"
      header="You have nothing to preview."
    />
  ) : (
    <ErrorMessage
      size="massive"
      content="Unfortunately this section can't be loaded."
    />
  );
};

export default NewWords;
