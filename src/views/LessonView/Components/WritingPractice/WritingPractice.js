import React, { useEffect } from "react";
import { Header, List, Table, Grid } from "semantic-ui-react";
import { PREVIEW_MODE, WRITING_INIT_VALUES } from "../../../../constants";

// style
import "./style.scss";

const WritingPractice = ({ lessonValues, mode }) => {
  const { production, examples } = lessonValues;
  const { simple, compound, complex } = examples || {};
  const { description, bulletPoints = [] } = production || {};

  useEffect(() => {
    if (mode !== PREVIEW_MODE) {
      window.scrollTo(0, 0);
    }
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <Header as="h2" className="writing-task__sub-header uppercase">
        Production
      </Header>

      <p>{description}</p>

      <List>
        {!!bulletPoints.length &&
          bulletPoints.map((bulletPoint, index) => (
            <List.Item
              className="writing-lesson-view__bullet-point"
              key={bulletPoint}
            >
              <span
                dangerouslySetInnerHTML={{
                  __html: `${index + 1}. ${bulletPoint}`,
                }}
              />
            </List.Item>
          ))}
      </List>

      <Header as="h2" className="writing-task__sub-header uppercase">
        USE A VARIETY OF SENTENCE STYLES
      </Header>
      <p>
        Avoid using simple sentences. Instead, try to use a variety of sentence
        styles and lengths:
      </p>
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Type</Table.HeaderCell>
            <Table.HeaderCell>Definition</Table.HeaderCell>
            <Table.HeaderCell>Example</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          <Table.Row>
            <Table.Cell singleLine>
              <b> Simple </b>
            </Table.Cell>
            <Table.Cell>Consists of only one clause.</Table.Cell>
            <Table.Cell>
              <i>
                <span
                  dangerouslySetInnerHTML={{
                    __html: simple || WRITING_INIT_VALUES.examples.simple,
                  }}
                />
              </i>
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell singleLine>
              <b> Compound </b>
            </Table.Cell>
            <Table.Cell>
              Combines two independent clauses with: and, but, yet, so, etc.
            </Table.Cell>
            <Table.Cell>
              <i>
                <span
                  dangerouslySetInnerHTML={{
                    __html: compound || WRITING_INIT_VALUES.examples.compound,
                  }}
                />
              </i>
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell singleLine>
              <b>Complex </b>
            </Table.Cell>
            <Table.Cell>
              Combines one main clause and one dependent clause with: if, even
              though, because, unless etc.
            </Table.Cell>
            <Table.Cell>
              <i>
                <span
                  dangerouslySetInnerHTML={{
                    __html: complex || WRITING_INIT_VALUES.examples.complex,
                  }}
                />
              </i>
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
      <Header as="h2" className="writing-task__sub-header uppercase">
        USE TRANSITION WORDS AND CONJUNCTIONS
      </Header>
      <p>
        Elaborate with examples or stories whenever possible. You can use the
        following transition words, phrases and conjunctions to connect your
        ideas:
      </p>

      <Grid columns={3}>
        <Grid.Row className={mode === PREVIEW_MODE ? "p-1" : ""}>
          <Grid.Column>
            <List as="ul">
              <List.Item as="li"> For example</List.Item>
              <List.Item as="li"> Even though/if</List.Item>
              <List.Item as="li"> Since...</List.Item>
              <List.Item as="li"> If..., then...</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column>
            <List as="ul">
              <List.Item as="li"> For instance,</List.Item>
              <List.Item as="li"> I remember when...</List.Item>
              <List.Item as="li"> Unless...</List.Item>
              <List.Item as="li"> Not only..., but also...</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column>
            <List as="ul">
              <List.Item as="li"> Let me explain.</List.Item>
              <List.Item as="li"> This reminds me of...</List.Item>
              <List.Item as="li"> Until...</List.Item>
              <List.Item as="li"> Besides...</List.Item>
            </List>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default WritingPractice;
