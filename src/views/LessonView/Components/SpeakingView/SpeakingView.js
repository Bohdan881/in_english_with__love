import React, { useEffect } from "react";
import { Header } from "semantic-ui-react";
import { PREVIEW_MODE, SPEAKING_PRACTICE } from "../../../../constants";
import { ErrorMessage, InfoMessage } from "../../../Shared/Message/Message";

// style
import "./style.scss";

const SpeakingView = ({
  lessonValues = [],
  chapterSequence,
  currentStep,
  mode,
}) => {
  useEffect(() => {
    if (mode !== PREVIEW_MODE) {
      window.scrollTo(0, 0);
    }
    // eslint-disable-next-line
  }, []);

  const chapterInd = chapterSequence.findIndex((v) => v === SPEAKING_PRACTICE);

  const currentIndex = currentStep - chapterInd + 1;

  return lessonValues && lessonValues.length ? (
    <>
      <Header as="h2" className="lesson-view__exercise-description">
        Discuss the following questions.
      </Header>

      <Header as="h3" className="lesson-view__exercise-detailed-description">
        Try to give as much detail as you can. Elaborate with examples or
        stories whenever possible.
      </Header>

      {lessonValues.map(
        ({ contentBlocks, id, maxPage }, key) =>
          currentIndex === maxPage && (
            <div key={id} className="mb-2">
              <span
                className={`position-absolute ${
                  key >= 9
                    ? "speaking-practice-view__bullet-point--two-digits"
                    : ""
                }`}
              >{`${key + 1}.`}</span>
              {contentBlocks &&
                contentBlocks.length &&
                contentBlocks.map(({ blockId, blockContent }) => (
                  <p
                    key={blockId}
                    className="speaking-practice-view__paragraph"
                  >
                    <span dangerouslySetInnerHTML={{ __html: blockContent }} />
                  </p>
                ))}
            </div>
          )
      )}
    </>
  ) : mode === PREVIEW_MODE ? (
    <InfoMessage
      className="lesson-view__error-message"
      header="You have nothing to preview."
    />
  ) : (
    <ErrorMessage
      size="massive"
      content="Unfortunately this section can't be loaded."
    />
  );
};

export default SpeakingView;
