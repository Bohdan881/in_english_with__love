export const content = [
  {
    answer: "merge",
    id: 0,
    sentence:
      "We can {merge} (marge) our two small businesses into a bigger one.",
    userValue: "",
    isCorrect: false,
  },
  {
    answer: "overlook",
    id: 1,
    sentence:
      "It’s easy to {overlook} (overlook) a small detail like this one.",
    userValue: "",
    isCorrect: false,
  },

  {
    answer: "daredevil",
    id: 3,
    sentence:
      "She’s a bit of a {daredevil} (daredevil). She loves climbing buildings and mountains.",
    userValue: "",
    isCorrect: false,
  },
  {
    answer: "defy",
    id: 4,
    sentence:
      "Importing food that we can grow here {defy} (defy) common sense.",
    userValue: "",
    isCorrect: false,
  },
  {
    answer: "antsy",
    id: 5,
    sentence: "I feel {antsy} (antsy) today, I don’t know why.",
    userValue: "",
    isCorrect: false,
  },
  {
    answer: "tenacity",
    id: 6,
    sentence:
      "We’ve always admired him for his {tenacity} (tenacity) and dedication.",
    userValue: "",
    isCorrect: false,
  },
];
