import React, { Component } from "react";
import { connect } from "react-redux";
import { List, Input, Button, Icon, Popup, Header } from "semantic-ui-react";
import { setSessionValues } from "../../../../redux/actions";
import {
  EXERCISES_DESCRIPTIONS,
  EXERCISES_DETAILED_DESCRIPTIONS,
  PREVIEW_MODE,
} from "../../../../constants";
import { ErrorMessage } from "../../../Shared/Message/Message";

class AnotherWay extends Component {
  setInitValues = () => {
    const {
      lessonValues,
      chapterName,
      sessionState,
      currentTopic,
      isSectionStarted,
    } = this.props;
    const { lessonProgress } = sessionState;

    this.props.onSetSessionValues({
      lessonProgress: {
        ...lessonProgress,
        [currentTopic]: {
          ...lessonProgress[currentTopic],
          [chapterName]: {
            lessonId: lessonValues.id,
            content: lessonValues.content.map((obj) => ({
              ...obj,

              isCorrect: false,
              ...(!isSectionStarted && { userValue: "" }),
            })),
            exerciseDescription: EXERCISES_DESCRIPTIONS[lessonValues.name]
              ? EXERCISES_DESCRIPTIONS[lessonValues.name][0].text
              : null,
            detailedDescription: EXERCISES_DETAILED_DESCRIPTIONS[
              lessonValues.name
            ]
              ? EXERCISES_DETAILED_DESCRIPTIONS[lessonValues.name][0].text
              : null,
          },
        },
      },
    });
  };

  componentDidMount() {
    const { currentTopic, chapterName, mode } = this.props;

    const { lessonProgress } = this.props.sessionState;
    const topic = lessonProgress[currentTopic];

    // set init values if a user hasn't started an exercise yet
    this.setState({});
    if (mode === PREVIEW_MODE) {
      this.setInitValues();
    } else if (!topic || !Object.entries(topic[chapterName] || {}).length) {
      this.setInitValues();
    }
  }

  componentDidUpdate() {
    const { lessonProgress } = this.props.sessionState;
    const { lessonValues, currentTopic, chapterName } = this.props;

    /* there's might be an exercise with the same name  
       so, check by id if it's a new exercise with the same name
     */

    if (
      lessonProgress[currentTopic][chapterName].lessonId === undefined ||
      lessonValues.id !== lessonProgress[currentTopic][chapterName].lessonId
    ) {
      this.setInitValues();
    }
  }

  onChangeUserValue = (value, id) => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      const findEditedObjById = content.findIndex((obj) => obj.id === id);
      const clonedContent = Array.from(content);

      clonedContent[findEditedObjById].userValue = value;

      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              content: clonedContent,
            },
          },
        },
      });
    }
  };

  checkTask = (isDisabled) => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (!isDisabled && content.length) {
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              isChecked: true,
              content: content.map((obj) =>
                obj.userValue.trim().toLowerCase() === obj.answer
                  ? { ...obj, isCorrect: true }
                  : { ...obj, isCorrect: false }
              ),
            },
          },
        },
      });
    }
  };

  retryTask = () => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              isShowingSolution: false,
              isChecked: false,
              content: content.map((obj) => ({ ...obj, userValue: "" })),
            },
          },
        },
      });
    }
  };

  showSolution = () => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              isShowingSolution: true,
              isChecked: true,
              content: content.map((obj) => ({
                ...obj,
                userValue: obj.answer,
                isCorrect: true,
              })),
            },
          },
        },
      });
    }
  };

  render() {
    const { currentTopic, chapterName, sessionState } = this.props;

    const {
      content = [],
      isChecked = false,
      isShowingSolution = false,
      exerciseDescription = "",
      detailedDescription = "",
    } =
      (sessionState.lessonProgress &&
        sessionState.lessonProgress[currentTopic] &&
        sessionState.lessonProgress[currentTopic][chapterName]) ||
      {};

    const wordsInCurlyBraces = /\{.*?\}/g;
    const wordsInNormalBraces = /\(.*?\)/g;

    // a user can check result after he or she fills all the fields
    const isCheckedDisabled =
      content.length &&
      content.some(({ userValue = "" }) => userValue.trim() === "");

    return content.length ? (
      <div>
        <Header as="h2">{exerciseDescription}</Header>

        {detailedDescription && (
          <Header
            as="h3"
            className="lesson-view__exercise-detailed-description"
          >
            {detailedDescription}
          </Header>
        )}
        <List>
          {content.map((obj, key) => {
            /*  Identify answer and tooltip values
                the answer's surrounded by curly braces { }
                the tooltip's surrounded by brackets
            */
            const answerValues =
              (obj.sentence && obj.sentence.match(wordsInCurlyBraces)) || [];
            const tooltipValues = obj.sentence.match(wordsInNormalBraces) || [];

            let clonedSentence = obj.sentence ? obj.sentence : "";

            tooltipValues.length &&
              tooltipValues.forEach(
                (rx) => (clonedSentence = clonedSentence.replace(rx, "** "))
              );

            answerValues.length &&
              answerValues.forEach(
                (rx) =>
                  (clonedSentence = clonedSentence.replace(
                    ` ${rx}`,
                    tooltipValues.length ? "" : "** "
                  ))
              );

            clonedSentence = clonedSentence.split("**");

            return (
              <List.Item
                key={obj.answer}
                className="lesson-view-exercise__sentence "
                as="li"
              >
                {`${key + 1}. `}
                {clonedSentence[0]}
                <Input
                  className={`lesson-view-exercise__input ${
                    isChecked
                      ? obj.isCorrect
                        ? "lesson-view-exercise__input--correct"
                        : "lesson-view-exercise__input--incorrect"
                      : ""
                  }`}
                  placeholder={
                    tooltipValues && tooltipValues.length
                      ? tooltipValues[0].slice(1, tooltipValues[0].length - 1)
                      : ""
                  }
                  value={obj.userValue || ""}
                  onChange={(e, { value }) =>
                    this.onChangeUserValue(value, obj.id)
                  }
                />
                {clonedSentence[1]}
              </List.Item>
            );
          })}
        </List>

        {isChecked ? (
          <div className="lesson-view-exercise__check-container">
            <Button
              primary
              className="lesson-view-exercise__check-button"
              onClick={this.retryTask}
            >
              Retry
              <Icon
                fitted
                className="lesson-view-exercise__button-icon"
                name="repeat"
              />
            </Button>
            <Button
              color="teal"
              disabled={isShowingSolution}
              className="lesson-view-exercise__check-button"
              onClick={this.showSolution}
            >
              Solution
              <Icon
                fitted
                className="lesson-view-exercise__button-icon"
                name={isShowingSolution ? "lock open" : "lock"}
              />
            </Button>
          </div>
        ) : (
          <div className="lesson-view-exercise__check-container">
            <Popup
              inverted
              on="hover"
              disabled={!isCheckedDisabled}
              content="Please fill all the empty fields to check your result."
              trigger={
                <Button
                  primary
                  className={`lesson-view-exercise__check-button ${
                    isCheckedDisabled ? "button-disabled" : ""
                  }`}
                  onClick={() => this.checkTask(isCheckedDisabled)}
                >
                  Check
                  <Icon
                    fitted
                    className="lesson-view-exercise__button-icon"
                    name="check"
                  />
                </Button>
              }
            />
          </div>
        )}
      </div>
    ) : (
      <ErrorMessage
        size="big"
        content="Unfortunately this exercise can't be loaded."
      />
    );
  }
}

const mapStateToProps = (state) => {
  const { sessionState } = state;
  return { sessionState };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSetSessionValues: (values) => dispatch(setSessionValues(values)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AnotherWay);
