import React, { Component } from "react";
import { connect } from "react-redux";
import { List, Input, Button, Icon, Popup, Header } from "semantic-ui-react";
import {
  DEFINITIONS,
  EXERCISES_DESCRIPTIONS,
  EXERCISES_DETAILED_DESCRIPTIONS,
  FILL_IN_THE_BLANKS,
  PREVIEW_MODE,
  WORDS_IN_CONTEXT,
} from "../../../../constants";
import { setSessionValues } from "../../../../redux/actions";
import { shuffleArray } from "../../../../utils";
import { ErrorMessage } from "../../../Shared/Message/Message";

// styles
import "./style.scss";

class FillInTheBlanksView extends Component {
  setInitValues = () => {
    const {
      lessonValues,
      chapterName,
      sessionState,
      currentTopic,
      isSectionStarted,
    } = this.props;
    const { lessonProgress } = sessionState;

    this.props.onSetSessionValues({
      lessonProgress: {
        ...lessonProgress,
        [currentTopic]: {
          ...lessonProgress[currentTopic],
          [chapterName]: {
            lessonId: lessonValues.id,
            content: lessonValues.content.map((obj) => ({
              ...obj,
              ...(!isSectionStarted && { isCorrect: false }),
              ...(!isSectionStarted && { userValue: "" }),
            })),
            exerciseDescription: EXERCISES_DESCRIPTIONS[lessonValues.name]
              ? EXERCISES_DESCRIPTIONS[lessonValues.name][0].text
              : null,
            detailedDescription: EXERCISES_DETAILED_DESCRIPTIONS[
              lessonValues.name
            ]
              ? EXERCISES_DETAILED_DESCRIPTIONS[lessonValues.name][0].text
              : null,
            answerList:
              lessonValues.content.length &&
              lessonValues.content.map(({ answer }) => answer),
            initWordsList:
              lessonValues.content.length &&
              shuffleArray(
                lessonValues.content.map(({ initWord, answer }) =>
                  initWord ? initWord : answer
                )
              ),
          },
        },
      },
    });
  };

  componentDidMount() {
    const { currentTopic, chapterName, mode } = this.props;
    const { lessonProgress } = this.props.sessionState;
    const topic = lessonProgress[currentTopic];

    // set init values if a user hasn't started an exercise yet
    if (mode === PREVIEW_MODE) {
      this.setInitValues();
    } else if (
      !topic ||
      !Object.entries(topic[chapterName] || {}).length ||
      !topic[chapterName].content ||
      !topic[chapterName].content.length
    ) {
      this.setInitValues();
    }
  }

  componentDidUpdate() {
    const { lessonProgress } = this.props.sessionState;
    const { lessonValues, currentTopic, chapterName } = this.props;

    /* it might me an exercise with the same name  
       so, check by id if it's a new exercise with the same name
     */
    if (
      lessonProgress[currentTopic][chapterName].lessonId === undefined ||
      lessonValues.id !== lessonProgress[currentTopic][chapterName].lessonId
    ) {
      this.setInitValues();
    }
  }

  onChangeUserValue = (value, id) => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      const findEditedObjById = content.findIndex((obj) => obj.id === id);
      const clonedContent = Array.from(content);

      if (clonedContent[findEditedObjById]) {
        clonedContent[findEditedObjById].userValue = value;
      }

      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              content: clonedContent,
            },
          },
        },
      });
    }
  };

  checkTask = (isDisabled) => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (!isDisabled && content.length) {
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              isChecked: true,
              content: content.map((obj) =>
                obj.userValue.trim().toLowerCase() === obj.answer
                  ? { ...obj, isCorrect: true }
                  : { ...obj, isCorrect: false }
              ),
            },
          },
        },
      });
    }
  };

  retryTask = () => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              isShowingSolution: false,
              isChecked: false,
              content: content.map((obj) => ({ ...obj, userValue: "" })),
            },
          },
        },
      });
    }
  };

  showSolution = () => {
    const { sessionState, currentTopic, chapterName } = this.props;
    const { lessonProgress } = sessionState;
    const { content } = lessonProgress[currentTopic][chapterName];

    if (content.length) {
      this.props.onSetSessionValues({
        lessonProgress: {
          ...lessonProgress,
          [currentTopic]: {
            ...lessonProgress[currentTopic],
            [chapterName]: {
              ...lessonProgress[currentTopic][chapterName],
              isShowingSolution: true,
              isChecked: true,
              content: content.map((obj) => ({
                ...obj,
                userValue: obj.answer,
                isCorrect: true,
              })),
            },
          },
        },
      });
    }
  };

  render() {
    const { currentTopic, chapterName, sessionState } = this.props;
    const {
      content = [],
      isChecked = false,
      isShowingSolution = false,
      exerciseDescription = "",
      detailedDescription = "",
      initWordsList = [],
      answerList = [],
    } =
      (sessionState.lessonProgress &&
        sessionState.lessonProgress[currentTopic] &&
        sessionState.lessonProgress[currentTopic][chapterName]) ||
      {};

    const wordsInCurlyBraces = /\{.*?\}/g;

    // a user can check a result after he/she fills all the fields
    const isCheckedDisabled =
      content.length &&
      content.some(({ userValue = "" }) => userValue.trim() === "");

    const contentAnswersBox =
      chapterName === WORDS_IN_CONTEXT ? answerList : initWordsList;

    return content.length ? (
      <div>
        <Header as="h2">{exerciseDescription}</Header>

        {detailedDescription && (
          <Header
            as="h3"
            className="lesson-view__exercise-detailed-description"
          >
            {detailedDescription}
          </Header>
        )}
        {/* <Popup
            inverted
            basic
            className="lesson-view-exercise-popup"
            content="Please fill all the fields."
            trigger={
              <Icon
                name="circle question"
                className="lesson-view-match-icon"
                size="small"
              />
            }
          /> */}

        <div className="lesson-view__exercise-answers-container">
          {contentAnswersBox && contentAnswersBox.length > 0 && (
            <div className="lesson-view-exercise__answers-list">
              {contentAnswersBox.map((word) => {
                const isWordWritten =
                  chapterName !== FILL_IN_THE_BLANKS &&
                  content.some(({ userValue }) => userValue === word);

                return (
                  <div
                    key={word}
                    className="lesson-view__exercise-answer-field noselect"
                    style={{
                      textDecoration: isWordWritten ? "line-through" : "",
                    }}
                  >
                    {word}
                  </div>
                );
              })}
            </div>
          )}
        </div>
        <List>
          {content.map(
            ({ id, sentence, answer, initWord, isCorrect, userValue }, key) => {
              /* identify answer value 
                  it's surrounded by curly braces { }
              */
              const answerValues =
                (sentence && sentence.match(wordsInCurlyBraces)) || [];

              let clonedSentence = sentence ? sentence : "";

              answerValues.length &&
                answerValues.forEach(
                  (rx) => (clonedSentence = clonedSentence.replace(rx, "**"))
                );

              clonedSentence = clonedSentence.split("**");

              return (
                <List.Item
                  key={answer}
                  className="lesson-view-exercise__sentence"
                  as="li"
                >
                  <span className="float-left fill-in-the-blank__bullet-point">
                    {key + 1}.
                  </span>
                  <span> {clonedSentence[0]}</span>
                  <Input
                    className={`lesson-view-exercise__input  
                    lesson-view-exercise__input--${
                      isChecked ? (isCorrect ? "correct" : "incorrect") : ""
                    } 
                    ${chapterName === WORDS_IN_CONTEXT ? "float-right" : ""}

                    ${chapterName === DEFINITIONS ? "float-left" : ""}
                    `}
                    disabled={isChecked}
                    value={userValue || ""}
                    onChange={(e, { value }) =>
                      this.onChangeUserValue(value, id)
                    }
                  />
                  {chapterName === WORDS_IN_CONTEXT && <b> {initWord} </b>}
                  <span> {clonedSentence[1]}</span>
                </List.Item>
              );
            }
          )}
        </List>

        {isChecked ? (
          <div className="lesson-view-exercise__check-container">
            <Button
              primary
              className="lesson-view-exercise__check-button"
              onClick={this.retryTask}
            >
              Retry
              <Icon
                fitted
                className="lesson-view-exercise__button-icon"
                name="repeat"
              />
            </Button>
            <Button
              color="teal"
              disabled={isShowingSolution}
              className="lesson-view-exercise__check-button"
              onClick={this.showSolution}
            >
              Solution
              <Icon
                fitted
                className="lesson-view-exercise__button-icon"
                name={isShowingSolution ? "lock open" : "lock"}
              />
            </Button>
          </div>
        ) : (
          <div className="lesson-view-exercise__check-container">
            <Popup
              inverted
              on="hover"
              disabled={!isCheckedDisabled}
              content="Please fill all the empty fields to check your result."
              trigger={
                <Button
                  primary
                  className={`lesson-view-exercise__check-button ${
                    isCheckedDisabled ? "button-disabled" : ""
                  }`}
                  onClick={() => this.checkTask(isCheckedDisabled)}
                >
                  Check
                  <Icon
                    fitted
                    className="lesson-view-exercise__button-icon"
                    name="check"
                  />
                </Button>
              }
            />
          </div>
        )}
      </div>
    ) : (
      <ErrorMessage
        size="big"
        content="Unfortunately this exercise can't be loaded."
      />
    );
  }
}

const mapStateToProps = ({ sessionState }) => ({ sessionState });

const mapDispatchToProps = (dispatch) => ({
  onSetSessionValues: (values) => dispatch(setSessionValues(values)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FillInTheBlanksView);
