import React, { useEffect } from "react";
import { Embed, Header } from "semantic-ui-react";
import { renderDraftHtml } from "../../../../utils";

// style
import "./style.scss";

const ContentView = ({ contentVideoValues, lessonValues }) => {
  const { id, source } = contentVideoValues;

  useEffect(() => {
    // scroll to the top by default
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <div className="lesson-view__content-video-embed">
        <Embed id={id} source={source} iframe={{ allowFullScreen: true }} />
        <Header as="h2" className="lesson-view__task-header d-block">
          Transcript
        </Header>
      </div>
      <div className="pt-1">{renderDraftHtml(lessonValues)}</div>
    </div>
  );
};

export default ContentView;
