import React, { useEffect, useState } from "react";
import { Grid, Image, Placeholder } from "semantic-ui-react";
import { renderDraftHtml } from "../../../../utils";

// style
import "./style.scss";

const AboutView = ({ lessonValues, chapterName, assets }) => {
  const assetsExist = assets && assets[chapterName];

  const [assetLoading, setAssetLoading] = useState(assetsExist);

  useEffect(() => {
    if (assetsExist) {
      setTimeout(() => {
        setAssetLoading(false);
      }, 700);
    }
  }, []); // eslint-disable-line

  return (
    <Grid className="m-0">
      <Grid.Row>
        <Grid.Column
          className="lesson-view__about-column"
          width={!assetsExist ? 16 : 10}
        >
          {renderDraftHtml(lessonValues)}
        </Grid.Column>
        {assetsExist && (
          <Grid.Column className="d-flex justify-content-end" width={6}>
            {assetLoading ? (
              <Placeholder className="lesson-view__about-placeholder">
                <Placeholder.Image square width="100%" />
              </Placeholder>
            ) : (
              <Image
                src={assets[chapterName]}
                alt="about"
                width="100%"
                height="100%"
                className="lesson-view__about-image"
              />
            )}
          </Grid.Column>
        )}
      </Grid.Row>
    </Grid>
  );
};

export default AboutView;
