import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { compose } from "recompose";
import { Button, Form, Header } from "semantic-ui-react";
import { withFirebase } from "../../firebase";
import { SIGN_IN, SIGN_IN_INITIAL_STATE } from "../../constants";
import * as ROUTES from "../../constants/routes";
import { AnotherAccount, FormInput, SignLanding } from "./Components";

// style
import "./style.scss";

const SignInPage = () => (
  <SignLanding>
    <SignInForm />
  </SignLanding>
);

class SignInFormBase extends Component {
  state = { ...SIGN_IN_INITIAL_STATE };

  onSubmit = () => {
    const { email, password } = this.state;

    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...SIGN_IN_INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
        localStorage.removeItem("persist:root");
      })
      .catch((error) => this.setState({ error }));
  };

  onFormFieldChange = (event) =>
    this.setState({ [event.target.name]: event.target.value });

  render() {
    const { error, email, password } = this.state;

    return (
      <Form className="sign-form" onSubmit={this.onSubmit}>
        <Header className="sign-form__header" as="h2">
          Sign in
        </Header>
        {error &&
          !error.message.includes("password") &&
          !error.message.includes("email") && (
            <p className="sign-form__error-no-users">{error.message}</p>
          )}

        <FormInput
          error={error}
          type="email"
          name="email"
          value={email}
          onChange={this.onFormFieldChange}
        />
        <FormInput
          error={error}
          type="password"
          name="password"
          value={password}
          onChange={this.onFormFieldChange}
        />
        <p className="sign-form__forgot-password">
          <Link to={ROUTES.PASSWORD_FORGET}>Forgot Password?</Link>
        </p>
        <Button className="sign-form__submit-button" type="submit">
          <span className="sign-form__submit-button-text">Sign in</span>
        </Button>
        <AnotherAccount
          type={SIGN_IN}
          history={this.props.history}
          firebase={this.props.firebase}
          noUser="sign-form__error-no-users"
          actionType="SIGN IN"
        />
        <div>
          <p className="sign-form__account-ask">
            Don't have an account?
            <span className="sign-word">
              <Link to={ROUTES.SIGN_UP}>Sign up</Link>
            </span>
          </p>
        </div>
      </Form>
    );
  }
}

const SignInForm = compose(withRouter, withFirebase)(SignInFormBase);

export default SignInPage;
