import React, { Component } from "react";
import { Header, Form, Button } from "semantic-ui-react";
import { withFirebase } from "../../firebase";
import { InfoMessage } from "../Shared/Message/Message";
import { FormInput, SignLanding } from "./Components";
import { PASSWORD_FORGET_INIT } from "../../constants";

// style
import "./style.scss";

const PasswordForgetPage = () => (
  <SignLanding>
    <PasswordForgetForm />
  </SignLanding>
);

class PasswordForgetFormBase extends Component {
  state = { ...PASSWORD_FORGET_INIT };

  onSubmit = () => {
    const { email } = this.state;

    this.props.firebase
      .doPasswordReset(email)
      .then(() => {
        this.setState({ ...PASSWORD_FORGET_INIT, isSent: true });
      })
      .catch((error) => {
        this.setState({ error });
      });
  };

  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { error, email, isSent } = this.state;

    return (
      <Form className="sign-form" onSubmit={this.onSubmit}>
        <Header className="sign-form__header" as="h3">
          Forgot your password?
        </Header>
        {error && error.message && !error.message.includes("email") && (
          <p className="sign-form__error-no-users">{error.message}</p>
        )}

        <FormInput
          name="email"
          type="email"
          value={email}
          error={error}
          onChange={this.onChange}
        />
        <Button className="sign-form__submit-button" type="submit">
          <span className="sign-form__submit-button-text">Reset Password</span>
        </Button>
        {isSent && (
          <InfoMessage
            className="mt-1"
            header="The Instructions sent"
            content="Check your E-mails (Spam folder included) and follow the instructions in the email."
          />
        )}
      </Form>
    );
  }
}

const PasswordForgetForm = withFirebase(PasswordForgetFormBase);

export default PasswordForgetPage;
