import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { Link, withRouter } from "react-router-dom";
import { Header, Form, Popup, List, Icon, Button } from "semantic-ui-react";
import { FormInput, AnotherAccount, SignLanding } from "./Components";
import { withFirebase } from "../../firebase";
import * as ROLES from "../../constants/roles";
import * as ROUTES from "../../constants/routes";
import {
  ERROR_MESSAGES,
  SIGN_UP_INITIAL_STATE,
  ERROR_CODE_ACCOUNT_EXISTS,
  PASSWORD_SPECIAL_CHARACTERS,
} from "../../constants";

const PasswordRequirementsContent = () => (
  <div className="tutorial-popup-container">
    <Header as="h4">
      We encourage you to have a secure password. Make sure you:
    </Header>
    <List bulleted>
      <List.Item>Use at least 6 characters</List.Item>
      <List.Item>Use a mix of upper and lower case characters</List.Item>
      <List.Item>Use at least 1 number</List.Item>
      <List.Item>
        Use at least 1 special character like:
        {PASSWORD_SPECIAL_CHARACTERS.map((sign, key) => (
          <b key={sign}>
            {` ${sign} ${
              key === PASSWORD_SPECIAL_CHARACTERS.length - 1 ? " ." : ", "
            }  `}
          </b>
        ))}
      </List.Item>
      <List.Item>
        Please, try not to use common words or simple passwords like “password”,
        “qwerty”, or “123456”
      </List.Item>
    </List>
  </div>
);

const checkIfIncludes = (error, ...rest) =>
  error &&
  error.message &&
  [...rest].every(
    (el) => !error.message.toUpperCase().includes(el.toUpperCase())
  );

const SignUpPage = () => (
  <SignLanding>
    <SignUpForm />
  </SignLanding>
);

class SignUpFormBase extends Component {
  constructor(props) {
    super(props);
    this.state = { ...SIGN_UP_INITIAL_STATE };
  }

  onSubmit = () => {
    const { username, email, password, isAdmin } = this.state;
    // check username and password match
    if (username.length < 4) {
      this.setState({
        error: {
          message: ERROR_MESSAGES.username,
        },
      });
    } else if (password !== this.state["repeat password"]) {
      this.setState({
        error: {
          message: ERROR_MESSAGES.confirmPassword,
        },
      });
    } else {
      const roles = {};
      if (isAdmin) {
        roles[ROLES.ADMIN] = ROLES.ADMIN;
      }
      this.props.firebase
        .doCreateUserWithEmailAndPassword(email, password)
        .then((authUser) => {
          // Create a user in your Firebase realtime database
          return this.props.firebase.user(authUser.user.uid).set({
            username,
            email,
            roles,
            signDate: new Date().getTime(),
            // add completed lesson in case if user finished one before signing up
            lessonsCompleted: {
              ...JSON.parse(localStorage.getItem("firstCompletedLesson")),
            },
            lessonsInProgress: {
              ...JSON.parse(localStorage.getItem("firstLessonInProgress")),
            },
            isAgreedOnCookies: false,
          });
        })
        .then(() => {
          return this.props.firebase.doSendEmailVerification();
        })
        .then(() => {
          this.setState({ ...SIGN_UP_INITIAL_STATE });
          this.props.history.push(ROUTES.HOME);
          localStorage.removeItem("persist:root");
        })
        .catch((error) => {
          if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
            error.message = ERROR_MESSAGES.accountExist;
          }
          this.setState({ error });
        });
    }
  };

  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onChangeCheckbox = (event) => {
    this.setState({ [event.target.name]: event.target.checked });
  };

  render() {
    const { error, username, email, password } = this.state;

    return (
      <Form className="sign-form" onSubmit={this.onSubmit}>
        <Header className="sign-form__header" as="h3">
          Sign up
        </Header>
        {checkIfIncludes(error, "password", "username", "email") && (
          <p className="sign-form__error-no-users">{error.message}</p>
        )}
        <FormInput
          name="username"
          type="text"
          error={error}
          value={username}
          onChange={this.onChange}
        />
        <FormInput
          name="email"
          type="email"
          error={error}
          value={email}
          onChange={this.onChange}
        />
        <FormInput
          name="password"
          type="password"
          helpIcon={
            <Popup
              inverted
              basic
              content={<PasswordRequirementsContent />}
              wide="very"
              trigger={
                <Icon
                  name="question circle"
                  size="small"
                  className="sign-form__popup-password-requirements"
                />
              }
            />
          }
          error={error}
          value={password}
          onChange={this.onChange}
        />

        <FormInput
          name="repeat password"
          type="password"
          error={error}
          value={this.state["repeat password"]}
          onChange={this.onChange}
        />

        <Button className="sign-form__submit-button" type="submit">
          <span className="sign-form__submit-button-text">Sign up</span>
        </Button>
        <AnotherAccount
          history={this.props.history}
          firebase={this.props.firebase}
          noUser="sign-form__error-no-users"
          actionType="Sign up"
        />
        <div className="sign-form__account-ask">
          <p className="">
            Already have an account?
            <span className="sign-word">
              <Link to={ROUTES.SIGN_IN}>Sign in</Link>
            </span>
          </p>
        </div>
        {/* <label>
          Admin:
          <input
            name="isAdmin"
            type="checkbox"
            checked={this.state.isAdmin}
            onChange={this.onChangeCheckbox}
          />
        </label> */}
      </Form>
    );
  }
}

const SignUpForm = compose(
  connect(null, null),
  withRouter,
  withFirebase
)(SignUpFormBase);

export default SignUpPage;
