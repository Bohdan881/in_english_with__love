import React, { useState } from "react";
import { Image } from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import {
  ERROR_CODE_ACCOUNT_EXISTS,
  ERROR_MSG_ACCOUNT_EXISTS,
} from "../../../../constants";
import * as ROUTES from "../../../../constants/routes";
import { withFirebase } from "../../../../firebase";

// style
import "./style.scss";

// assets
import facebook from "../../../../assets/images/facebook.png";
import google from "../../../../assets/images/google.png";

const AnotherAccountBase = ({ firebase, actionType, history, noUser }) => {
  const [error, setError] = useState(null);

  const onSubmitGoogle = () => {
    firebase
      .doSignInWithGoogle()
      .then((socialAuthUser) =>
        // Create a user in your Firebase Realtime Database too
        firebase.user(socialAuthUser.user.uid).set({
          username: socialAuthUser.user.displayName,
          email: socialAuthUser.user.email,
          roles: {},
        })
      )
      .then(() => {
        setError(null);
        history.push(ROUTES.HOME);
      })
      .catch(({ code }) => {
        if (code === ERROR_CODE_ACCOUNT_EXISTS) {
          setError(ERROR_MSG_ACCOUNT_EXISTS);
        }
      });
  };

  const onSubmitFacebook = () => {
    firebase
      .doSignInWithFacebook()
      .then((socialAuthUser) =>
        // Create a user in your Firebase Realtime Database too
        firebase.user(socialAuthUser.user.uid).set({
          username: socialAuthUser.additionalUserInfo.profile.name,
          email: socialAuthUser.additionalUserInfo.profile.email,
          roles: {},
        })
      )
      .then(() => {
        setError(null);
        history.push(ROUTES.HOME);
      })
      .catch(({ code }) => {
        if (code === ERROR_CODE_ACCOUNT_EXISTS) {
          setError(ERROR_MSG_ACCOUNT_EXISTS);
        }
      });
  };

  return (
    <div className="mt-1 mb-1">
      <p className="another-account__paragraph">
        <span className="capitalize">{`${actionType.toLowerCase()} `}</span>
        with another account:
      </p>
      <div className="d-flex flex-row">
        <Image
          onClick={onSubmitGoogle}
          className="social-image-google"
          src={google}
          alt="google"
        />
        <Image
          onClick={onSubmitFacebook}
          className="social-image-facebook"
          src={facebook}
          alt="facebook"
        />
        {error && <p className={noUser}> {error}</p>}
      </div>
    </div>
  );
};

const AnotherAccount = compose(withRouter, withFirebase)(AnotherAccountBase);

export default AnotherAccount;
