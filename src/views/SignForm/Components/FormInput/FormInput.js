import React from "react";
import { Form } from "semantic-ui-react";
import "./style.scss";

const FormInput = ({ error, type, value, name, onChange, helpIcon }) => {
  // check if error is related to the curret field
  const isFieldError =
    error &&
    error.message &&
    (error.message.toUpperCase().includes(value.toUpperCase()) ||
      error.message.toUpperCase().includes(type.toUpperCase()));

  return (
    <Form.Field className="sign-form__field">
      <label className="sign-form__label capitalize">
        {name}
        {helpIcon}
      </label>
      <Form.Input
        error={isFieldError ? { content: error.message } : false}
        name={name}
        placeholder={name}
        type={type}
        value={value}
        onChange={onChange}
      />
    </Form.Field>
  );
};

export default FormInput;
