import React from "react";
import { Grid } from "semantic-ui-react";
import LeftGridAuth from "../../../Shared/LeftGridAuth/LeftGridAuth";

// style
import "./style.scss";

const SignLanding = ({ children }) => (
  <Grid className="sign-grid">
    <Grid.Row columns={2}>
      <LeftGridAuth />
      <Grid.Column
        mobile={16}
        tablet={9}
        computer={8}
        floated="left"
        className="sign-grid__right-column"
      >
        {children}
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default SignLanding;
