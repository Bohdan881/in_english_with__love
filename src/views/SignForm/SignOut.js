import React from "react";
import { Link } from "react-router-dom";
import { withFirebase } from "../../firebase";
import { SIGN_IN } from "../../constants/routes";

const signUserOut = (firebase) => {
  localStorage.removeItem("firstCompletedLesson");
  return firebase.doSignOut;
};

const SignOut = ({ firebase }) => (
  <Link to={SIGN_IN} onClick={signUserOut(firebase)}>
    Sign out
  </Link>
);

export default withFirebase(SignOut);
