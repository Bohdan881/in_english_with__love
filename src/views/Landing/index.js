import React from "react";

//style
import "./style.scss";

const Landing = ({ children }) => (
  <div className="landing-wrapper">{children}</div>
);

export default Landing;
