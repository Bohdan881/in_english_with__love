import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Grid, Menu } from "semantic-ui-react";
import { compose } from "recompose";
import { EditAccount, HomePage } from "./Components";
import { withAuthorization, withEmailVerification } from "../../auth";
import { withFirebase } from "../../firebase";

// style
import "./style.scss";

const AccountPage = () => {
  const { authUser } = useSelector((db) => db.sessionState);

  const [activeItem, setActiveItem] = useState("Home");
  const [menuItems] = useState([
    { name: "Home", component: HomePage },
    { name: "Edit", component: EditAccount },
    // { name: "Help", component: Help },
  ]);

  const activeComponent = menuItems.findIndex((obj) => obj.name === activeItem);

  const ComponentName = menuItems[activeComponent].component;

  return (
    <Grid className="account__container">
      <Grid.Row>
        <Grid.Column
          tablet={4}
          computer={4}
          largeScreen={4}
          widescreen={4}
          mobile={16}
        >
          <Menu fluid vertical={window.innerWidth >= 767}>
            {menuItems.map(
              ({ name }) =>
                name && (
                  <Menu.Item
                    key={name}
                    name={name}
                    icon={name.toLowerCase()}
                    active={activeItem === name}
                    onClick={() => setActiveItem(name)}
                  />
                )
            )}
          </Menu>
        </Grid.Column>
        <Grid.Column
          tablet={12}
          computer={12}
          largeScreen={12}
          widescreen={12}
          mobile={16}
          className="account__content-column"
        >
          <ComponentName {...authUser} />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

// check if auth user exists
const condition = (authUser) => !!authUser;

export default compose(
  withEmailVerification,
  withAuthorization(condition),
  withFirebase
)(AccountPage);
