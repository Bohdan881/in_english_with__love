export const usersData = [
  {
    email: "admin@google.com",
    lessonsCompleted: {},
    roles: { ADMIN: "ADMIN" },
    uid: "4sQtrcTsnbRu0w07VG3EWcsX9Y32",
    username: "admin",
  },
  {
    email: "test@google.com",
    roles: { ADMIN: "ADMIN" },
    uid: "5pF1CNFqomdORhMausV8jfWrL7p2",
    username: "test",
  },

  {
    email: "okay@gmail.com",
    lessonsCompleted: { "-M9-_vU_v_MHcB_oa6HC": 1592672037439 },
    uid: "rBWwo6jTjbMfvJNvVBMPttXB0nx2",
    username: "UserName",
  },
];
