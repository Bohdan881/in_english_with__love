import React, { useState } from "react";
import { Segment, Accordion, Icon, Header } from "semantic-ui-react";
import { ADMIN } from "../../../../constants/roles";

const HELP_ACCORDION = [
  {
    title: "How to create a lesson?",
    description: "Description",

    justAdmin: true,
  },
];

const Help = ({ authUser }) => {
  const [activeIndex, setActiveIndex] = useState(-1);

  return (
    <Segment>
      <Header as="h3" textAlign="center">
        Help
      </Header>
      <Accordion>
        {HELP_ACCORDION.map((obj, key) => {
          return (authUser.roles && authUser.roles[ADMIN] === ADMIN) ||
            !obj.justAdmin ? (
            <React.Fragment key={key}>
              <Accordion.Title
                active={activeIndex === key}
                index={key}
                onClick={() => setActiveIndex(key === activeIndex ? -1 : key)}
              >
                <Icon name="dropdown" />
                {obj.title}
              </Accordion.Title>
              <Accordion.Content active={activeIndex === key}>
                {obj.description}
              </Accordion.Content>
            </React.Fragment>
          ) : null;
        })}
      </Accordion>
    </Segment>
  );
};

export default Help;
