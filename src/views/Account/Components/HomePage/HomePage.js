import React, { PureComponent } from "react";
import {
  ResponsiveContainer,
  AreaChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Area,
  Brush,
  PieChart,
  Pie,
  Cell,
  Legend,
} from "recharts";
import { connect } from "react-redux";
import { Segment, Header, List, Dimmer, Loader } from "semantic-ui-react";
import { CATEGORY_ID } from "../../../../constants";
import { setUsers, setSessionValues } from "../../../../redux/actions";
import { ErrorMessage, InfoMessage } from "../../../Shared/Message/Message";

// style
import "./style.scss";

const PIE_CHART_COLORS = ["#0088FE", "#00C49F", "#e0b4b4"];

const CustomizedTooltip = ({ label, payload, active }) => {
  if (active) {
    return (
      <div className="account-home-page__tooltip">
        <div>Date : {label} </div>
        <div>Completed lessons : {payload[0].payload.lessonsCompleted} </div>
      </div>
    );
  }
  return null;
};

const CustomizedUsersTooltip = ({ label, payload, active }) => {
  if (active) {
    return (
      <div className="account-home-page__tooltip">
        <div>Date : {label} </div>
        <div>Signed Users : {payload[0].payload.usersAmount} </div>
      </div>
    );
  }
  return null;
};

class HomePage extends PureComponent {
  state = {
    allLessons: 0,
    error: "",
    errorText: "",
    loadingUsers: false,
  };

  transfromUsersToAreaChart = (users) => {
    let usersDataChart = [];

    users.length &&
      users.forEach((user) => {
        if (user.signDate) {
          // transform from miliseconds to date
          const isoDate = new Date(user.signDate).toISOString().slice(0, 10);

          // check if current date exists in the array already
          const dateIndex = usersDataChart.findIndex(
            (obj) => obj.signDate === isoDate
          );

          /*  if the given date doesn't exist, push it in array,
              otherwise inceremnt its value
          */
          if (dateIndex === -1) {
            usersDataChart.push({
              signDate: isoDate,
              usersAmount: 1,
            });
          } else {
            usersDataChart[dateIndex].usersAmount += 1;
          }
        }
        return null;
      });
    this.setState({ loadingUsers: false });
    return usersDataChart;
  };

  setUsersDataForAdmin = () => {
    const { users = [] } = this.props.userState;

    this.setState({ loadingUsers: true });
    // load user values from db and set them to redux
    if (!users.length) {
      this.props.firebase.users().on(
        "value",
        (snapshot) => {
          const usersList = snapshot && snapshot.val();

          if (usersList) {
            const users = Object.keys(usersList || {}).map((key) => ({
              ...usersList[key],
              uid: key,
            }));

            this.props.onSetUsers({
              users,
              usersDataChart: this.transfromUsersToAreaChart(users),
            });
          }
        },
        (errorObject) => {
          this.setState({
            error: true,
            errorText: errorObject.code,
            loadingUsers: false,
          });
        }
      );
    } else {
      this.props.onSetUsers({
        usersDataChart: this.transfromUsersToAreaChart(users),
      });
    }
  };

  componentDidMount() {
    const { authUser } = this.props.sessionState;

    let progressAreaChart = [];
    let categoriesDataChart = [];

    if (authUser && authUser.lessonsCompleted) {
      const completedLessonsList = Object.entries(authUser.lessonsCompleted);

      completedLessonsList.forEach(([subCategory, date]) => {
        // transform from miliseconds to date
        const isoDate = new Date(date).toISOString().slice(0, 10);

        /*  Populate data for Progress Chart */

        // check if current date exists in array already
        const dateIndex = progressAreaChart.findIndex(
          ({ date }) => date === isoDate
        );

        /*  if the given date doesn't exist, push it in array,
            otherwise inceremnt its value
        */

        if (dateIndex === -1) {
          progressAreaChart.push({
            date: isoDate,
            lessonsCompleted: 1,
          });
        } else {
          progressAreaChart[dateIndex].lessonsCompleted += 1;
        }
        //--------------------------------

        /*  Populate data for Pie Chart 
            Pie Chart visualizes data by completed lesson 
            for each category
        */

        // identify lessons subCategory
        const subCategoryNameIndex = subCategory.lastIndexOf(CATEGORY_ID);

        /*   Slice value by index and add length of the connected word which is "-subCategory-".
             So we have to slice  it from the end subCategory, 
             that is why we add its length to find out its ending index
          */

        const transformedName = subCategory.slice(
          subCategoryNameIndex + CATEGORY_ID.length
        );

        if (subCategoryNameIndex !== -1) {
          // check if current subcategory exists in array
          const subCategoryIndex = categoriesDataChart.findIndex(
            (obj) => obj.subCategory === transformedName
          );

          /*  If the given date doesn't exist, push it in array,
              otherwise inceremnt its value
           */

          if (subCategoryIndex === -1) {
            categoriesDataChart.push({
              subCategory: transformedName,
              lessonsCompleted: 1,
            });
          } else {
            categoriesDataChart[subCategoryIndex].lessonsCompleted += 1;
          }
        }
      });

      this.props.onSetSessionValues({
        progressAreaChart: progressAreaChart.sort((a, b) =>
          new Date(a.date) > new Date(b.date) ? 1 : -1
        ),
        categoriesDataChart,
        allLessons: completedLessonsList.length,
      });
    }

    // set users values, if it's admin
    if (!!authUser.roles && !!authUser.roles["ADMIN"]) {
      this.setUsersDataForAdmin();
    }
  }
  render() {
    const { error, errorText, loadingUsers } = this.state;
    const { userState, sessionState } = this.props;
    const { users, usersDataChart } = userState;
    const {
      authUser,
      categoriesDataChart,
      progressAreaChart,
      allLessons,
    } = sessionState;

    return (
      <>
        <Segment>
          <Header as="h3" textAlign="center">
            Overview
          </Header>
          {categoriesDataChart.length ? (
            <div className="account-home-page__overview">
              <List size="huge">
                <List.Item>
                  <List.Icon name="user" />
                  <List.Content className="capitalize">
                    Username: <b>{authUser.username}</b>
                  </List.Content>
                </List.Item>

                <List.Item>
                  <List.Icon name="mail outline" />
                  <List.Content>
                    Email: <b> {authUser.email}</b>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Icon name="sort numeric up" />
                  <List.Content>
                    Completed lessons: <b>{allLessons}</b>
                  </List.Content>
                </List.Item>
                {authUser.roles && authUser.roles["ADMIN"] && (
                  <List.Item>
                    <List.Icon name="tag" />
                    <List.Content>
                      Amount of users: <b>{users ? users.length : 0}</b>
                    </List.Content>
                  </List.Item>
                )}
              </List>
              <div className="account-home-page-pie-chart">
                <PieChart width={300} height={250}>
                  <Pie
                    isAnimationActive={false}
                    data={categoriesDataChart}
                    innerRadius={65}
                    outerRadius={80}
                    fill="#8884d8"
                    paddingAngle={5}
                    dataKey="lessonsCompleted"
                  >
                    {categoriesDataChart.map((entry, index) => (
                      <Cell
                        key={index}
                        fill={PIE_CHART_COLORS[index % PIE_CHART_COLORS.length]}
                      />
                    ))}
                  </Pie>

                  <Legend
                    payload={categoriesDataChart.map(
                      ({ subCategory, lessonsCompleted }, index) => ({
                        id: subCategory,
                        type: "circle",
                        color:
                          PIE_CHART_COLORS[index % PIE_CHART_COLORS.length],
                        value: `${subCategory} (${lessonsCompleted})`,
                      })
                    )}
                  />
                </PieChart>
              </div>
            </div>
          ) : (
            <InfoMessage
              className="account-home-page__message"
              header="You haven't done any lessons yet."
              size="huge"
            />
          )}
        </Segment>
        {authUser.roles && authUser.roles["ADMIN"] && (
          <Segment>
            <Header as="h3" textAlign="center">
              Users Statistic
            </Header>

            {loadingUsers ? (
              <Dimmer inverted active className="account-home-page__dimmer">
                <Loader size="big">Loading</Loader>
              </Dimmer>
            ) : usersDataChart.length ? (
              <ResponsiveContainer width="100%" height={300}>
                <AreaChart
                  data={usersDataChart}
                  margin={{ top: 50, right: 30, left: 0, bottom: 0 }}
                >
                  <XAxis dataKey="signDate" angle={-50} dy={25} height={80} />
                  <YAxis
                    dataKey="usersAmount"
                    allowDecimals={false}
                    label={{
                      value: "Amount of users",
                      angle: -90,
                    }}
                  />
                  <CartesianGrid strokeDasharray="3 3" />
                  <Tooltip content={<CustomizedUsersTooltip />} />
                  <Area
                    type="monotone"
                    dataKey="usersAmount"
                    stroke="#0088FE"
                    fill="#0088FE"
                  />
                  <Brush height={30} stroke="#8884d8" />
                </AreaChart>
              </ResponsiveContainer>
            ) : !usersDataChart.length && !error ? (
              <InfoMessage
                className="account-home-page__message"
                header="You don't have users yet."
                size="huge"
              />
            ) : (
              error && (
                <ErrorMessage
                  className="account-home-page__message"
                  header={errorText}
                  size="huge"
                />
              )
            )}
          </Segment>
        )}
        <Segment>
          <Header as="h3" textAlign="center">
            Your Progress
          </Header>

          {progressAreaChart.length ? (
            <ResponsiveContainer width="100%" height={300}>
              <AreaChart
                data={progressAreaChart}
                margin={{ top: 50, right: 30, left: 0, bottom: 0 }}
              >
                <XAxis dataKey="date" angle={-50} dy={25} height={80} />
                <YAxis
                  dataKey="lessonsCompleted"
                  allowDecimals={false}
                  label={{
                    value: "Completed  Lessons",
                    angle: -90,
                  }}
                />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip content={<CustomizedTooltip />} />
                <Area
                  type="monotone"
                  dataKey="lessonsCompleted"
                  stroke="#82ca9d"
                  fill="#82ca9d"
                />
                <Brush height={30} stroke="#8884d8" />
              </AreaChart>
            </ResponsiveContainer>
          ) : (
            <InfoMessage
              className="account-home-page__message"
              header="You haven't done any lessons yet."
              size="huge"
            />
          )}
        </Segment>
      </>
    );
  }
}

const mapStateToProps = ({ sessionState, userState }) => ({
  sessionState,
  userState,
});

const mapDispatchToProps = (dispatch) => ({
  onSetUsers: (values) => dispatch(setUsers(values)),
  onSetSessionValues: (values) => dispatch(setSessionValues(values)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
