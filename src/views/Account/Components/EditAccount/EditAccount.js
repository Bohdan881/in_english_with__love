import React from "react";
import {
  PasswordChangeForm,
  UpdateUserProfile,
  DeleteAccount,
  SignInTypeManagement,
} from "./Components";

const EditAccount = () => (
  <>
    <UpdateUserProfile />
    <PasswordChangeForm />
    <SignInTypeManagement />
    <DeleteAccount />
  </>
);

export default EditAccount;
