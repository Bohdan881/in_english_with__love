import DeleteAccount from "./DeleteAccount";
import PasswordChangeForm from "./PasswordChangeForm";
import SignInTypeManagement from "./SignInTypeManagement";
import UpdateUserProfile from "./UpdateUserProfile";

export {
  DeleteAccount,
  PasswordChangeForm,
  SignInTypeManagement,
  UpdateUserProfile,
};
