import React, { PureComponent } from "react";
import { compose } from "recompose";
import { connect } from "react-redux";
import { Button, Header, Segment } from "semantic-ui-react";
import { withFirebase } from "../../../../../firebase";
import { confirmationAlert } from "../../../../../utils/fireAlert";
import { ErrorMessage } from "../../../../Shared/Message/Message";
import { REMOVE_ACCOUNT_CONFIRMATION } from "../../../../../constants/alertContent";

// style
import "./style.scss";

class DeleteAccount extends PureComponent {
  state = {
    error: null,
  };

  onDeleteAccount = () => {
    const { firebase } = this.props;

    confirmationAlert(REMOVE_ACCOUNT_CONFIRMATION).then((response) => {
      if (response.value) {
        firebase.auth.currentUser
          .delete()
          .catch((error) => this.setState({ error: error.message }));
      }
    });
  };
  render() {
    const { error } = this.state;
    const { authUser } = this.props;

    return (
      <Segment>
        <Header as="h3" textAlign="center">
          Delete Account
        </Header>
        <div className="delete-account__container">
          <p className="delete-account--warning">
            You can delete your account at any time. However, this action is
            irreversible.
          </p>
          <div
            className="delete-account__button-container"
            style={{ justifyContent: error ? "space-between" : "flex-end" }}
          >
            <Button
              color="red"
              type="submit"
              className="delete-account__button"
              onClick={this.onDeleteAccount}
              disabled={
                authUser.roles && authUser.roles["ADMIN"] ? true : false
              }
            >
              I understand, delete my account
            </Button>
          </div>
        </div>
        {error && (
          <ErrorMessage
            className="delete-account__error-message"
            content={error}
          />
        )}
      </Segment>
    );
  }
}

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});

export default compose(
  withFirebase,
  connect(mapStateToProps, null)
)(DeleteAccount);
