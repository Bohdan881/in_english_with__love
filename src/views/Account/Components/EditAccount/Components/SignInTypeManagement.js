import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { Segment, Header, List, Button, Form, Icon } from "semantic-ui-react";
import { SIGN_IN_METHODS } from "../../../../../constants";
import { withFirebase } from "../../../../../firebase";
import { setSessionValues } from "../../../../../redux/actions";
import { passwordValidator } from "../../../../../utils";
import { ErrorMessage } from "../../../../Shared/Message/Message";

class SignInTypeManagement extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      activeSignInMethods: [],
      error: null,
    };
  }

  onDefaultLoginLink = (password) => {
    const { firebase } = this.props;
    const { authUser } = this.props.sessionState;

    const credential = firebase.emailAuthProvider.credential(
      authUser.email,
      password
    );

    firebase.auth.currentUser
      .linkWithCredential(credential)
      .then(this.fetchSignInMethods)
      .catch((error) => this.setState({ error }));
  };

  // all social networks you already linked to your account
  fetchSignInMethods = () => {
    const { firebase } = this.props;
    const { authUser } = this.props.sessionState;

    firebase.auth
      .fetchSignInMethodsForEmail(authUser.email)
      .then((activeSignInMethods) =>
        this.setState({ activeSignInMethods, error: null })
      )
      .catch((error) => this.setState({ error }));
  };

  onSocialLoginLink = (provider) => {
    const { firebase } = this.props;

    firebase.auth.currentUser
      .linkWithPopup(this.props.firebase[provider])
      .then(this.fetchSignInMethods)
      .catch((error) => this.setState({ error }));
  };

  onUnlink = (providerId) => {
    this.props.firebase.auth.currentUser
      .unlink(providerId)
      .then(this.fetchSignInMethods)
      .catch((error) => this.setState({ error }));
  };

  componentDidMount() {
    this.fetchSignInMethods();
  }

  render() {
    const { activeSignInMethods, error } = this.state;
    return (
      <Segment>
        <Header as="h3" textAlign="center">
          Manage Sign In Methods
        </Header>
        <p className="sign-in-management__description">Sign In Methods:</p>
        <List>
          {SIGN_IN_METHODS.map((signInMethod) => {
            const onlyOneLeft = activeSignInMethods.length === 1;
            const isEnabled = activeSignInMethods.includes(signInMethod.id);
            return (
              <List.Item key={signInMethod.id} as="ul">
                {signInMethod.id === "password" ? (
                  <DefaultLoginToggle
                    onlyOneLeft={onlyOneLeft}
                    isEnabled={isEnabled}
                    signInMethod={signInMethod}
                    onLink={this.onDefaultLoginLink}
                    onUnlink={this.onUnlink}
                  />
                ) : (
                  <SocialLoginToggle
                    onlyOneLeft={onlyOneLeft}
                    isEnabled={isEnabled}
                    signInMethod={signInMethod}
                    onLink={this.onSocialLoginLink}
                    onUnlink={this.onUnlink}
                  />
                )}
              </List.Item>
            );
          })}
        </List>
        {error && <ErrorMessage content={error.message} />}
      </Segment>
    );
  }
}

class DefaultLoginToggle extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { passwordOne: "", passwordTwo: "" };
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.props.onLink(this.state.passwordOne);
    this.setState({ passwordOne: "", passwordTwo: "" });
  };

  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { onlyOneLeft, isEnabled, signInMethod, onUnlink } = this.props;
    const { passwordOne, passwordTwo } = this.state;

    const isInvalid =
      (passwordOne === "" && passwordTwo === "") ||
      passwordOne !== passwordTwo ||
      !passwordValidator(passwordOne);

    return isEnabled ? (
      <Button onClick={() => onUnlink(signInMethod.id)} disabled={onlyOneLeft}>
        Deactivate {signInMethod.id}
      </Button>
    ) : (
      <Form onSubmit={this.onSubmit}>
        <Form.Group>
          <Form.Input
            name="passwordOne"
            value={passwordOne}
            onChange={this.onChange}
            type="password"
            placeholder="********"
          />
          <Form.Input
            name="passwordTwo"
            value={passwordTwo}
            onChange={this.onChange}
            type="password"
            placeholder="********"
          />
          <Form.Button disabled={isInvalid}>Link {signInMethod.id}</Form.Button>
        </Form.Group>
      </Form>
    );
  }
}

const SocialLoginToggle = ({
  onlyOneLeft,
  isEnabled,
  signInMethod,
  onLink,
  onUnlink,
}) => (
  <div className="sign-in-management__social-container">
    <div className="sign-in-management__social-name">
      <span>
        <Icon name={signInMethod.icon} /> {signInMethod.name}
      </span>
    </div>
    <div>
      {isEnabled ? (
        <Button
          color="red"
          onClick={() => onUnlink(signInMethod.id)}
          disabled={onlyOneLeft}
        >
          Deactivate Account
        </Button>
      ) : (
        <Button primary onClick={() => onLink(signInMethod.provider)}>
          Link Account
        </Button>
      )}
    </div>
  </div>
);

const mapStateToProps = ({ sessionState }) => ({
  sessionState,
});

const mapDispatchToProps = (dispatch) => ({
  onUpdateUserProfile: (values) => dispatch(setSessionValues(values)),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withFirebase
)(SignInTypeManagement);
