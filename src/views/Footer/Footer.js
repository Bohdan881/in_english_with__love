import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Menu, Grid, Image, Icon } from "semantic-ui-react";
import * as ROUTES from "../../constants/routes";

// style
import "./style.scss";

// assets
import logo from "../../assets/images/default.png";

const Footer = () => {
  const [activeItem, setActiveItem] = useState(null);
  return (
    <Grid className="footer">
      <Grid.Row columns={3} verticalAlign="middle">
        <Grid.Column width={5} floated="left" className="footer__grid-logo">
          <Link onClick={() => setActiveItem(ROUTES.HOME)} to={ROUTES.HOME}>
            <Image className="logo" src={logo} alt="logo" />
          </Link>
        </Grid.Column>
        <Grid.Column width={6}>
          <Menu text className="footer__menu">
            <Menu.Item className="footer__item">
              © In English With Love
            </Menu.Item>
            {ROUTES.FOOTER_ROUTES.map((route) => (
              <Menu.Item
                key={route}
                className="capitalize"
                link
                active={activeItem === route}
              >
                <Link onClick={() => setActiveItem(route)} to={route}>
                  {route.slice(1)}
                </Link>
              </Menu.Item>
            ))}
          </Menu>
        </Grid.Column>

        <Grid.Column
          widescreen={5}
          largeScreen={5}
          computer={5}
          tablet={5}
          mobile={16}
          className="footer__icon-list"
        >
          <Menu text floated="right">
            <Menu.Item>
              <a
                href="https://www.instagram.com/inenglishwithlove"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon color="black" link size="large" name="instagram" />
              </a>
            </Menu.Item>
            <Menu.Item>
              <a
                href="https://www.facebook.com/inenglishwithlove/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon link size="large" name="facebook f" color="black" />
              </a>
            </Menu.Item>
            <Menu.Item>
              <a
                href="https://www.pinterest.ca/inenglishwithlove/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <Icon link size="large" name="pinterest p" color="black" />
              </a>
            </Menu.Item>
          </Menu>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default Footer;
