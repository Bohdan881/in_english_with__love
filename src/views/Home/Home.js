import React from "react";
import { Grid, Header, Image, Segment, Container } from "semantic-ui-react";

// style
import "./style.scss";

// assets
import laptopWork from "../../assets/images/laptopWork.jpg";
import build from "../../assets/images/build.jpg";
import practise from "../../assets/images/practice.jpg";

const HomePage = () => (
  <Grid className="home-page__grid">
    <Grid.Row className="home-page__slogan-row">
      <Grid.Column className="d-flex flex-column justify-content-center home-page__slogan-column">
        <Segment className="home-page__slogan-wrapper">
          <Header
            className="home-page__slogan-header"
            as="h1"
            textAlign="center"
          >
            Become A More Fluent & Natural English Speaker <br />
          </Header>
          <p className="home-page__subheader">
            Welcome to
            <i>
              <b> In Ensligh With Love</b>{" "}
            </i>
            academy. <br />
            Our mission is mission is to help you transcend your limitations in
            English and experience the progress that will help you achieve your
            dreams.
          </p>
        </Segment>
        <div className="home-page__svg-middle">
          <svg
            viewBox="0 0 1920 187"
            preserveAspectRatio="none"
            className="home-page__slogan-background-svg"
          >
            <polygon
              points="1920,187 0,187 0,181 82,172 458,31 778,110 1354,0 1920,182"
              className="c1"
            ></polygon>
          </svg>
        </div>
      </Grid.Column>
    </Grid.Row>
    <Grid.Row className="home-page__benefits-row">
      <Grid.Column>
        <Segment className="home-page__benefits-segment">
          <Container>
            <Header className="home-page__header" as="h1" textAlign="center">
              How Will You Benefit From Learning Here?
            </Header>
            <Grid className="m-0 pt-1">
              <Grid.Row columns={2}>
                <Grid.Column
                  widescreen={8}
                  largeScreen={8}
                  computer={8}
                  tablet={8}
                  mobile={16}
                >
                  <Container className="pl-1">
                    <Header className="home-page__header" as="h1">
                      Boost your vocabulary
                    </Header>
                    <Image
                      className="home-page__image-mobile"
                      src={laptopWork}
                      alt="work"
                    />
                    <p>
                      In our lessons you'll find a lot of intermediate and
                      advanced words, which will help you to flourish your
                      vocabulary.
                    </p>
                  </Container>
                </Grid.Column>
                <Grid.Column
                  className="home-page__benefits-column-asset"
                  textAlign="center"
                >
                  <Image
                    className="home-page__image"
                    src={laptopWork}
                    alt="work"
                  />
                </Grid.Column>
              </Grid.Row>

              <Grid.Row columns={2}>
                <Grid.Column
                  className="home-page__benefits-column-asset"
                  textAlign="center"
                >
                  <Image
                    className="home-page__image"
                    src={build}
                    alt="build"
                  />
                </Grid.Column>
                <Grid.Column
                  className="m-0 pt-1"
                  widescreen={8}
                  largeScreen={8}
                  computer={8}
                  tablet={8}
                  mobile={16}
                >
                  <Container className="pl-1">
                    <Header className="home-page__header" as="h1">
                      Practice new words
                    </Header>
                    <Image
                      className="home-page__image-mobile"
                      src={build}
                      alt="build"
                    />
                    <p>
                      Each lesson provides practise section. So you'll
                      consolidate the knowledge which you've gained during the
                      lesson.
                    </p>
                  </Container>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row columns={2}>
                <Grid.Column
                  widescreen={8}
                  largeScreen={8}
                  computer={8}
                  tablet={8}
                  mobile={16}
                >
                  <Container>
                    <Header className="home-page__header" as="h1">
                      Explore the world
                    </Header>
                    <Image
                      className="home-page__image-mobile"
                      src={practise}
                      alt="practice"
                    />
                    <p>
                      Lessons based on interesting stories & facts. You'll never
                      get bored.
                    </p>
                  </Container>
                </Grid.Column>
                <Grid.Column
                  className="home-page__benefits-column-asset"
                  textAlign="center"
                >
                  <Image
                    className="home-page__image"
                    src={practise}
                    alt="practice"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
        </Segment>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

// authUser != null;
// const condition = (authUser) => !authUser || authUser;

// export default withAuthorization(condition)(HomePage);
export default HomePage;
