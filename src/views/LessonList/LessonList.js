import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "recompose";
import _ from "lodash";
import { withFirebase } from "../../firebase";
import { getAllLessons, setSessionValues } from "../../redux/actions";
import {
  Grid,
  Loader,
  Segment,
  Dimmer,
  Header,
  Transition,
  Form,
} from "semantic-ui-react";
import { LESSON_TOPIC } from "../../constants/routes";
import { convertMillisecondsToDate } from "../../utils";
import { LessonCard } from "../Shared";
import { ErrorMessage, InfoMessage } from "../Shared/Message/Message";
import { CATEGORY_ID, LESSON_TYPE } from "../../constants";

// style
import "./style.scss";

// assets
import defaultImage from "../../assets/images/default.png";

class LessonList extends Component {
  state = {
    currentTopic: window.location.href.slice(
      window.location.href.lastIndexOf("=") + 1
    ),
    currentCategory:
      LESSON_TYPE[
        window.location.href
          .slice(
            window.location.href.indexOf("=") + 1,
            window.location.href.indexOf("&")
          )
          .split(/[^A-Za-z]/)
          .join("")
      ],

    topicLessons: [],
    isSearchingTopic: false,
    isLoadingLessons: false,
    error: false,
    isSearched: false,
  };

  filterPostsList = (postsList) => {
    const { currentTopic, currentCategory } = this.state;
    return (
      postsList &&
      postsList.filter((obj) => {
        return (
          obj.category
            .toLowerCase()
            .split(/[^A-Za-z]/)
            .join("") === currentCategory &&
          obj.subCategory.split(" ").join("_") === currentTopic
        );
      })
    );
  };

  handleSearchChange = (e, { value }) => {
    const { currentCategory, currentTopic } = this.state;
    const { data } = this.props.lessons;

    this.setState({ isSearchingTopic: true, value, isSearched: true });

    setTimeout(() => {
      if (this.state.value.length < 1) {
        this.setState({
          topicLessons: data[currentCategory][currentTopic],
          isSearchingTopic: false,
        });
      }

      const re = new RegExp(_.escapeRegExp(this.state.value), "i");
      const isMatch = (result) => re.test(result.title);

      this.setState({
        isSearchingTopic: false,
        topicLessons: _.filter(data[currentCategory][currentTopic], isMatch),
      });
    }, 300);
  };

  onClickCheckBox = (e, data) => {
    const { topicLessons, currentCategory, currentTopic } = this.state;
    const { authUser } = this.props.sessionState;

    this.setState({ isSearched: true });

    const allStartedLessons = [
      ...Object.keys(authUser.lessonsCompleted || {}),
      ...Object.keys(authUser.lessonsInProgress || {}),
    ];

    if (data.checked && !!topicLessons.length && allStartedLessons.length > 0) {
      /* use Category id to remove -category- from  completed
        lesson id which we added when a user completed his/her lesson
      */
      this.setState({
        checkedCompleted: true,
        topicLessons: topicLessons.filter((topic) => {
          return !allStartedLessons
            .map((lesson) =>
              lesson.indexOf(CATEGORY_ID) === -1
                ? lesson
                : lesson.slice(0, lesson.indexOf(CATEGORY_ID))
            )
            .includes(topic.uid);
        }),
      });
    } else {
      this.setState({
        checkedCompleted: false,
        topicLessons: this.props.lessons.data[currentCategory][currentTopic],
      });
    }
  };

  onCheckProgressLessons = (event, data) => {
    const {
      topicLessons,
      checkedCompleted,
      currentCategory,
      currentTopic,
    } = this.state;
    const { authUser } = this.props.sessionState;

    this.setState({ isSearched: true });

    if (data.checked && !!topicLessons.length && !!authUser.lessonsInProgress) {
      /* use Category id to remove -category- from  progress 
        lesson id which we added when a user progress his/her lesson
      */

      this.setState({
        checkedLessonInProgress: true,
        topicLessons: topicLessons.filter((topic) => {
          return !!Object.keys(authUser.lessonsInProgress)
            .map((lesson) => {
              return lesson.indexOf(CATEGORY_ID) === -1
                ? lesson
                : lesson.slice(0, lesson.indexOf(CATEGORY_ID));
            })
            .includes(topic.uid);
        }),
      });
    } else {
      this.setState({
        checkedCompleted: false,
      });
      if (checkedCompleted) {
        this.onClickCheckBox(null, true);
      } else {
        this.setState({
          topicLessons: this.props.lessons.data[currentCategory][currentTopic],
        });
      }
    }
  };

  componentDidMount() {
    const { topicLessons, currentCategory, currentTopic } = this.state;
    const { data, error } = this.props.lessons;

    if (!data) {
      this.props.onGetAllLessons(this.props.firebase);
      this.setState({ isLoadingLessons: true, error });
    } else if (
      topicLessons.length === 0 &&
      data &&
      data[currentCategory] &&
      data[currentCategory][currentTopic]
    ) {
      this.setState({
        error,
        isLoadingLessons: false,
        topicLessons: data[currentCategory][currentTopic],
      });
    }
  }

  componentDidUpdate() {
    const {
      topicLessons,
      currentCategory,
      currentTopic,
      isSearched,
    } = this.state;
    const { data, error } = this.props.lessons;

    if (
      topicLessons.length === 0 &&
      !error &&
      data &&
      data[currentCategory] &&
      data[currentCategory][currentTopic] &&
      !isSearched
    ) {
      this.setState({
        isLoadingLessons: false,
        topicLessons: data[currentCategory][currentTopic],
      });
    }
  }

  componentWillUnmount() {
    this.props.firebase.posts().off();
  }

  render() {
    const {
      currentCategory,
      currentTopic,
      isLoadingLessons,
      isSearchingTopic,
      error,
      value,
      transitionDuration,
      checkedCompleted,
      checkedLessonInProgress,
      topicLessons,
    } = this.state;
    const { sessionState, lessons } = this.props;
    const { authUser } = sessionState;
    const { data } = lessons;

    const currentTopicFormatted = !!currentTopic
      ? currentTopic.split("_").join(" ")
      : "";

    return (
      <Grid className="lesson-list" stackable>
        <Grid.Row className="lesson-list__header-row">
          <Grid.Column
            widescreen={6}
            largeScreen={6}
            computer={3}
            tablet={3}
            mobile={16}
            verticalAlign="bottom"
            textAlign="left"
          >
            <Header className="lesson-list__header capitalize" as="h2">
              {currentTopicFormatted}
            </Header>
          </Grid.Column>
          <Grid.Column
            widescreen={10}
            largeScreen={10}
            computer={13}
            tablet={13}
            mobile={16}
            className="lesson-list__search-column"
          >
            <Form>
              <Form.Group>
                {authUser && (
                  <Form.Field className="lesson-list__checkbox-field">
                    <Form.Checkbox
                      label="Lessons in progress"
                      onClick={this.onCheckProgressLessons}
                    />
                  </Form.Field>
                )}
                {authUser && (
                  <Form.Field className="lesson-list__checkbox-field">
                    <Form.Checkbox
                      label="Unstarted lessons"
                      onClick={this.onClickCheckBox}
                    />
                  </Form.Field>
                )}
                <Form.Field>
                  <Form.Input
                    className="input-search"
                    size="large"
                    icon="search"
                    placeholder="Search topics..."
                    disabled={
                      !data ||
                      !data[currentCategory] ||
                      !data[currentCategory][currentTopic] ||
                      data[currentCategory][currentTopic].length < 1
                    }
                    onChange={this.handleSearchChange}
                  />
                </Form.Field>
              </Form.Group>
            </Form>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row textAlign="left" className="topics-row">
          {error ? (
            <ErrorMessage content={error.message} />
          ) : isSearchingTopic || isLoadingLessons ? (
            <Segment inverted className="w-100 bg-transparent">
              <Dimmer className="bg-transparent" inverted active>
                <Loader className="w-100 mt-5" size="big">
                  Loading
                </Loader>
              </Dimmer>
            </Segment>
          ) : !topicLessons.length &&
            !checkedCompleted &&
            !checkedLessonInProgress ? (
            <InfoMessage
              className="lesson-list__info-message"
              header="No lessons found..."
              size="massive"
              content={
                <>
                  <span className="capitalize">{`${currentTopic}  `} </span>
                  doesn't have <b>{` ${value ? `"${value}"` : "lessons"}. `}</b>
                </>
              }
            />
          ) : !topicLessons.length &&
            (checkedCompleted || checkedLessonInProgress) ? (
            <InfoMessage
              className="lesson-list__info-message"
              size="massive"
              header={`${
                checkedCompleted ? "Unstarted lessons" : "Lessons in progress"
              }  not found!`}
              content="You didn't start any lessons in the current category."
            />
          ) : (
            topicLessons.map((lessonData) => {
              const { uid, title, iconPath, date } = lessonData;
              return (
                <Grid.Column
                  computer={4}
                  tablet={8}
                  className="mb-1"
                  key={title}
                >
                  <Transition
                    visible={true}
                    animation="fade"
                    duration={transitionDuration}
                    transitionOnMount={true}
                  >
                    <Link
                      className="h-100"
                      to={`${LESSON_TOPIC}?category=${currentCategory}&topic=${currentTopicFormatted}/${title
                        .toLowerCase()
                        .split(/[^A-Za-z]/)
                        .join("-")}`}
                      onClick={() =>
                        this.props.onSetSessionValues({
                          lessonInfo: {
                            currentCategory,
                            currentTopic,
                            title,
                          },
                        })
                      }
                    >
                      <LessonCard
                        currentId={uid}
                        img={iconPath || defaultImage}
                        title={title}
                        date={
                          date
                            ? convertMillisecondsToDate(date)
                            : convertMillisecondsToDate(new Date().getTime())
                        }
                        authUser={authUser}
                        isPreview={false}
                      />
                    </Link>
                  </Transition>
                </Grid.Column>
              );
            })
          )}
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = ({ lessons, sessionState }) => ({
  lessons,
  sessionState,
});

const mapDispatchToProps = (dispatch) => ({
  onSetSessionValues: (values) => dispatch(setSessionValues(values)),
  onGetAllLessons: (database) => dispatch(getAllLessons(database)),
});

export default compose(
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps)
)(LessonList);
