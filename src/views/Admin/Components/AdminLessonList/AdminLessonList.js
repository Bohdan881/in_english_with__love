import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "recompose";
import _ from "lodash";
import {
  Loader,
  Dimmer,
  Segment,
  Grid,
  Table,
  Button,
  Icon,
  Header,
  Input,
  Pagination,
  Statistic,
  Placeholder,
} from "semantic-ui-react";
import { LESSON_TOPIC } from "../../../../constants/routes";
import { ONE_PAGE_LESSONS, LESSONS_BUCKET_NAME } from "../../../../constants";
import {
  LESSON_REMOVED_ALERT,
  REMOVE_LESSON_ALERT_CONFIRMATION,
  ICON_LESSON_REMOVE_ALERT,
} from "../../../../constants/alertContent";
import {
  setLessonValues,
  getAllLessons,
  setSessionValues,
} from "../../../../redux/actions";
import { withFirebase } from "../../../../firebase";
import { LESSON_MODE } from "../../../../constants";
import { convertMillisecondsToDate } from "../../../../utils";
import {
  confirmationAlert,
  infoAutoCloseAlert,
} from "../../../../utils/fireAlert";
import { ErrorMessage, InfoMessage } from "../../../Shared/Message/Message";

// styles
import "./style.scss";

class AdminLessonList extends PureComponent {
  state = {
    isLoading: false,
    error: false,
    searchLoading: false,
    searchedValue: "",
    lessonsList: [],
    column: null,
    direction: null,
    activePage: 1,
    startIndex: 0,
    endIndex: ONE_PAGE_LESSONS,
  };

  handleSort = (clickedColumn) => () => {
    const { column, lessonsList, direction } = this.state;

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        lessonsList: _.sortBy(lessonsList, [clickedColumn]),
        direction: "ascending",
      });
      return;
    }

    this.setState({
      lessonsList: lessonsList.reverse(),
      direction: direction === "ascending" ? "descending" : "ascending",
    });
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ searchLoading: true, searchedValue: value.toLowerCase() });

    const transfromedData = this.transformDataToList();

    setTimeout(() => {
      if (this.state.searchedValue.length < 1) {
        this.setState({
          lessonsList: transfromedData,
          searchLoading: false,
        });
      }

      this.setState({
        searchLoading: false,
        lessonsList: _.filter(
          transfromedData,
          (item) =>
            item &&
            item.title &&
            value &&
            item.title.toLowerCase().includes(value.toLowerCase())
        ),
      });
    }, 300);
  };

  transformDataToList = () =>
    Object.values(this.props.lessons.data || {})
      .map((obj) => Object.values(obj).flat())
      .flat();

  componentDidMount() {
    const { data, error } = this.props.lessons;
    this.setState({ isLoading: true });

    if (!data) {
      this.props.onGetAllLessons(this.props.firebase);
      this.setState({
        error,
      });
    } else {
      this.setState({
        lessonsList: this.transformDataToList(data),
        error: false,
        isLoading: false,
      });
    }
  }

  componentDidUpdate() {
    const { lessonsList, isLoading } = this.state;
    const { data, error } = this.props.lessons;

    if (data && lessonsList.length === 0) {
      this.setState({
        lessonsList: this.transformDataToList(),
      });
    }

    if (isLoading && lessonsList.length > 0 && !error) {
      this.setState({
        isLoading: false,
      });
    }
  }

  componentWillUnmount() {
    this.props.firebase.posts().off();
  }

  removeLessonFromDb = (lesson) => {
    const { iconPath } = lesson;
    const { firebase } = this.props;

    confirmationAlert(REMOVE_LESSON_ALERT_CONFIRMATION).then((response) => {
      this.setState({ lessonsList: [] });
      if (response.value) {
        // check if the lesson has an icon
        if (lesson.iconPath !== "") {
          firebase.storage
            .refFromURL(iconPath)
            .delete()
            .catch(({ text }) =>
              infoAutoCloseAlert({ ...ICON_LESSON_REMOVE_ALERT, text })
            );
        }

        // check if the lesson has any assets
        if (Object.entries(lesson.assets || {}).length) {
          Object.values(lesson.assets || {}).map((assetUrl) =>
            firebase.storage
              .refFromURL(assetUrl)
              .delete()
              .catch(({ text }) =>
                infoAutoCloseAlert(ICON_LESSON_REMOVE_ALERT, text)
              )
          );
        }
        // remove the lesson
        firebase.db
          .ref(`${LESSONS_BUCKET_NAME}/${lesson.uid}`)
          .remove()
          .then(() =>
            infoAutoCloseAlert({
              ...LESSON_REMOVED_ALERT,
              text: `${lesson.title} has been deleted successfully.`,
            })
          )
          .catch(({ text }) =>
            infoAutoCloseAlert({ ...LESSON_REMOVED_ALERT, text })
          );
      }
    });
  };

  onPagination = (data) => {
    // an example => page number (2 * 10) - 10 == start index is 10
    const startIndex = data.activePage * ONE_PAGE_LESSONS - 10;

    this.setState({
      activePage: data.activePage,
      startIndex,
      endIndex: startIndex + ONE_PAGE_LESSONS,
    });
  };

  render() {
    const {
      isLoading,
      searchLoading,
      error,
      lessonsList,
      direction,
      column,
      activePage,
      startIndex,
      endIndex,
    } = this.state;

    return isLoading && !error ? (
      <Segment className="loader-whole-screen">
        <Dimmer active>
          <Loader size="massive">Loading </Loader>
        </Dimmer>
      </Segment>
    ) : error && !isLoading ? (
      <ErrorMessage size="massive" content={error.message} />
    ) : (
      <Grid>
        <Grid.Row columns={2} className="admin-lesson-list__header-row">
          <Grid.Column>
            <Header className="admin-lesson-list-header" as="h1">
              Lessons
            </Header>
          </Grid.Column>
          <Grid.Column className="d-flex justify-content-end pr-0">
            <Input
              className="mr-1"
              size="large"
              icon="search"
              placeholder="Search lessons..."
              onChange={this.handleSearchChange}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {lessonsList.length ? (
              <>
                <Table
                  sortable
                  celled
                  className="w-100 admin-lesson-list__table"
                >
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell
                        sorted={column === "title" ? direction : null}
                        onClick={this.handleSort("title")}
                      >
                        Title
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        sorted={column === "category" ? direction : null}
                        onClick={this.handleSort("category")}
                      >
                        Category
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        sorted={column === "subCategory" ? direction : null}
                        onClick={this.handleSort("subCategory")}
                      >
                        SubCategory
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        sorted={column === "date" ? direction : null}
                        onClick={this.handleSort("date")}
                      >
                        Date
                      </Table.HeaderCell>
                      <Table.HeaderCell>Edit</Table.HeaderCell>
                      <Table.HeaderCell>Remove</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {searchLoading ? (
                      <Table.Row>
                        <td colSpan={6}>
                          <Placeholder fluid>
                            <Placeholder.Paragraph>
                              <Placeholder.Line length="full" />
                              <Placeholder.Line length="full" />
                              <Placeholder.Line length="full" />
                              <Placeholder.Line length="full" />
                              <Placeholder.Line length="full" />
                              <Placeholder.Line length="full" />
                              <Placeholder.Line length="full" />
                              <Placeholder.Line length="full" />
                            </Placeholder.Paragraph>
                          </Placeholder>
                        </td>
                      </Table.Row>
                    ) : (
                      lessonsList.slice(startIndex, endIndex).map((lesson) => {
                        const { category, subCategory, title, date } = lesson;
                        return (
                          <Table.Row key={title}>
                            <Table.Cell width="6">
                              <Link
                                className="admin-lesson-list__link"
                                to={`${LESSON_TOPIC}?category=${category.toLowerCase()}&topic=${
                                  subCategory &&
                                  subCategory.split(" ").join("_")
                                }/${title.toLowerCase().split(" ").join("-")}`}
                                onClick={() => {
                                  this.props.onSetSessionValues({
                                    lessonInfo: {
                                      currentCategory: category,
                                      currentTopic: subCategory,
                                      title,
                                    },
                                  });
                                }}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                {title}
                              </Link>
                            </Table.Cell>
                            <Table.Cell width="3">{category}</Table.Cell>
                            <Table.Cell width="3">{subCategory}</Table.Cell>
                            <Table.Cell width="3" error={!date}>
                              {date
                                ? convertMillisecondsToDate(date)
                                : "No date provided."}
                            </Table.Cell>
                            <Table.Cell width="1" textAlign="center">
                              <Button
                                className="admin-lesson-list__action-icon"
                                onClick={() => {
                                  this.props.setEditLessonTabIndex();

                                  this.props.onSetLessonValues({
                                    ...lesson,
                                    lessonMode: LESSON_MODE.EDIT,
                                  });
                                }}
                              >
                                <Icon name="edit" color="yellow" />
                              </Button>
                            </Table.Cell>
                            <Table.Cell width="1" textAlign="center">
                              <Button
                                className="admin-lesson-list__action-icon"
                                onClick={() => this.removeLessonFromDb(lesson)}
                              >
                                <Icon name="remove" color="red" />
                              </Button>
                            </Table.Cell>
                          </Table.Row>
                        );
                      })
                    )}
                  </Table.Body>
                </Table>
                <div className="d-flex justify-content-between">
                  <Statistic size="mini" horizontal></Statistic>
                  <div className="d-flex">
                    <div className="admin-lesson-list__lessons-quantity-container">
                      <span>{`${startIndex}...${
                        endIndex > lessonsList.length
                          ? lessonsList.length
                          : endIndex
                      }`}</span>
                    </div>
                    <Pagination
                      className="float-right"
                      totalPages={Math.ceil(
                        lessonsList.length / ONE_PAGE_LESSONS
                      )}
                      activePage={activePage}
                      onPageChange={(e, data) => this.onPagination(data)}
                    />
                  </div>
                </div>
              </>
            ) : (
              <InfoMessage header="No Lessons Found" />
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = ({ lessons }) => ({ lessons });

const mapDispatchToProps = (dispatch) => ({
  onGetAllLessons: (database) => dispatch(getAllLessons(database)),
  onSetLessonValues: (values) => dispatch(setLessonValues(values)),
  onSetSessionValues: (values) => dispatch(setSessionValues(values)),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withFirebase
)(AdminLessonList);
