import React, { Component } from "react";
import { connect } from "react-redux";
import { withFirebase } from "../../../../firebase";
import { compose } from "recompose";
import {
  EditorState,
  convertToRaw,
  convertFromRaw,
  ContentState,
} from "draft-js";
import _ from "lodash";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import { setLessonValues } from "../../../../redux/actions";
import { EDITOR_OPTIONS } from "../../../../constants";
import { CustomColorPicker } from "./CustomComponents";

// styles
import "../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./style.scss";

class CustomEditor extends Component {
  constructor(props) {
    super(props);
    this.editorRef = React.createRef();
    this.state = {
      editorState: EditorState.createEmpty(),
    };
  }

  onEditorStateChange = (editorState) => {
    this.setState({ editorState });
    this.onChangeReduxState(editorState);
  };

  onChangeReduxState = _.debounce((editorState) => {
    const { sectionKey, newLessonState } = this.props;

    this.props.onSetLessonValues({
      lessonTheoreticalContent: {
        ...newLessonState.lessonTheoreticalContent,
        [sectionKey]: editorState,
      },
      localStorageData: {
        ...newLessonState.localStorageData,
        [sectionKey]: JSON.stringify(
          convertToRaw(editorState.getCurrentContent())
        ),
      },
    });
  }, 300);

  parseFromLocalStorage = () => {
    const { newLessonState, sectionKey } = this.props;
    const { localStorageData } = newLessonState;

    const transformJSONToEditorState = EditorState.createWithContent(
      convertFromRaw(JSON.parse(localStorageData[sectionKey]))
    );

    this.props.onSetLessonValues({
      lessonTheoreticalContent: {
        ...newLessonState.lessonTheoreticalContent,
        [sectionKey]: transformJSONToEditorState,
      },
    });

    this.setState({
      editorState: transformJSONToEditorState,
      // editorState: EditorState.createEmpty(),
    });
  };

  parseCodeFromDb = (lessonTheoreticalContent, sectionKey) => {
    const blocksFromHtml = htmlToDraft(
      JSON.parse(lessonTheoreticalContent[sectionKey])
    );

    const { contentBlocks, entityMap } = blocksFromHtml;

    const contentState = ContentState.createFromBlockArray(
      contentBlocks,
      entityMap
    );

    const editorState = EditorState.createWithContent(contentState);

    this.props.onSetLessonValues({
      lessonTheoreticalContent: {
        ...lessonTheoreticalContent,
        [sectionKey]: editorState,
      },
    });

    this.setState({ editorState: editorState });
  };

  praseCodeFromRedux = (sectionData) => {
    let isError = false;

    try {
      sectionData.getCurrentContent();
    } catch (ex) {
      isError = true;
    }

    if (isError) {
      this.parseFromLocalStorage();
    } else {
      this.setState({
        editorState: EditorState.createWithContent(
          sectionData.getCurrentContent()
        ),
      });
    }
  };

  componentDidMount() {
    const { newLessonState, sectionKey } = this.props;
    const {
      lessonTheoreticalContent = { [sectionKey]: "" },
      localStorageData = { [sectionKey]: "" },
    } = newLessonState;

    const sectionData = lessonTheoreticalContent[sectionKey] || "";
    const sectionLocalStorageData = localStorageData[sectionKey] || "";

    // set editor empty if there's no any data neither in redux nor in local storage
    if (sectionData === "" && sectionLocalStorageData === "") {
      this.setState({ editorState: EditorState.createEmpty() });
    }
    // if typeof is string after parsing, it means that we received html code from db
    else if (
      sectionData !== "" &&
      typeof sectionData === "string" &&
      JSON.parse(sectionData) === "string"
    ) {
      this.parseCodeFromDb(lessonTheoreticalContent, sectionKey);
    }
    // if it's immutable, it's the latest code from redux
    else if (sectionData && sectionData._immutable) {
      this.praseCodeFromRedux(sectionData);
      // if none of above if valid, fetch data from localStorage
    } else if (sectionLocalStorageData !== "") {
      this.parseFromLocalStorage();
    }
  }

  render() {
    const { editorState } = this.state;

    return (
      <Editor
        ref={this.editorRef}
        editorState={editorState} // EditorState.createEmpty()
        onEditorStateChange={this.onEditorStateChange}
        editorClassName="editor-area"
        toolbarClassName="editor-toolbar toolbar-class"
        toolbar={{
          fontFamily: {
            options: EDITOR_OPTIONS.fontFamily,
          },
          colorPicker: { component: CustomColorPicker },
          embedded: { className: "default-embedded" },
          image: {
            className: "d-none",
          },
        }}
        spellCheck={true}
      />
    );
  }
}

const mapStateToProps = ({ newLessonState }) => ({ newLessonState });

const mapDispatchToProps = (dispatch) => ({
  onSetLessonValues: (values) => dispatch(setLessonValues(values)),
});

export default compose(
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps)
)(CustomEditor);
