import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Card, Grid, Image } from "semantic-ui-react";
import CreateLessonTemplate from "./CreateLessonTemplate";
import { CATEGORIES } from "../../../../constants";
import { setLessonValues } from "../../../../redux/actions";

// style
import "./style.scss";

// assets
import { assetsLessonCreateCategoryRoutes } from "./assetsCategoriesRoutes";
import defaultImage from "../../../../assets/images/default.png";

const CreateLessonContainer = () => {
  // redux ---------------------------------
  const { category } = useSelector((db) => db.newLessonState);
  const dispatch = useDispatch();
  // ---------------------------------------

  return category === "" ? (
    <Grid stackable>
      <Grid.Row columns={1}>
        <Grid.Column>
          <Card.Group itemsPerRow={5}>
            {CATEGORIES.map(({ key, text }) => (
              <Card
                key={key}
                className="create-lesson__card"
                onClick={() => {
                  dispatch(
                    setLessonValues({
                      category: text,
                    })
                  );
                }}
              >
                <Image
                  src={assetsLessonCreateCategoryRoutes[key] || defaultImage}
                  wrapped
                  ui={false}
                  alt="create-lesson"
                />

                <Card.Content>
                  <Card.Header>{text}</Card.Header>
                </Card.Content>
              </Card>
            ))}
          </Card.Group>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  ) : (
    <CreateLessonTemplate />
  );
};

export default CreateLessonContainer;
