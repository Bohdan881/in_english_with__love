/* match subCategory with its own asset */
import video from "../../../../assets/images/subCategories/video.png";
import vocabulary from "../../../../assets/images/subCategories/vocab.png";
import phrasalverbs from "../../../../assets/images/subCategories/phrasal.png";

export const assetsLessonCreateCategoryRoutes = {
  video,
  vocabulary,
  phrasalverbs,
};
