import React, { Component } from "react";
import { withFirebase } from "../../../../firebase";
import { compose } from "recompose";
import { connect } from "react-redux";
import { withAuthorization } from "../../../../auth";
import * as ROLES from "../../../../constants/roles";
import { convertToRaw } from "draft-js";
import {
  ADMIN_DROPDOWN_TITLES,
  LESSONS_BUCKET_NAME,
  CREATE_LESSON_STAGES,
  LESSON_MODE,
  PREVIEW_MODE,
  INIT_LESSON_SHARED_KEY_VALUES,
  INIT_LESSON_KEY_VALUES_BY_CATEGORY,
  CATEGORIES,
  SUB_CATEGORIES,
  OPTIMIZED_ICON_IMAGE_ID,
  CARD_OPTIONS,
} from "../../../../constants";
import {
  Form,
  Button,
  Popup,
  Icon,
  Tab,
  Segment,
  Dimmer,
  Loader,
  Modal,
  Image,
} from "semantic-ui-react";
import draftToHtml from "draftjs-to-html";
import _ from "lodash";
import {
  getAllLessons,
  setLessonValues,
  setLessonToInit,
  setLessonToInitByCategory,
} from "../../../../redux/actions";
import { transformToOptions, optimizeImage } from "../../../../utils";
import buildSectionsByCategory from "./utilities/buildSectionsByCategory";
import { LessonPreview } from "./TheoryComponents";
import {
  ICON_LESSON_REMOVE_ALERT,
  ICON_LESSON_ADD_ALERT,
  CREATE_EDIT_LESSON_CONFIRAMTION,
  REMOVE_IMAGE_CONFIRMATION,
  LESSON_STATUS_ALERT,
  ERROR_ALERT,
} from "../../../../constants/alertContent";
import {
  confirmationAlert,
  infoAutoCloseAlert,
} from "../../../../utils/fireAlert";
import { ErrorMessage } from "../../../Shared/Message/Message";

class CreateLessonTemplate extends Component {
  constructor(props) {
    super(props);
    this.fileInputRef = React.createRef();
    this.state = {
      isLoading: false,
      lessons: [],
      iconFile: null,
      iconName: null,
      iconSrc: "",
      exercises: [],
      errorFields: [],
      error: false,
      modalState: false,
    };
  }

  componentDidMount() {
    const { data } = this.props.lessons;

    if (!data) {
      this.props.onGetAllLessons(this.props.firebase);
    }
  }

  componentWillUnmount() {
    this.props.firebase.posts().off();
  }

  onDropDownChange = (value = "", dropDownType) => {
    if (dropDownType === ADMIN_DROPDOWN_TITLES.category.defaultVal) {
      this.props.onSetLessonToInitByCategory({
        ...INIT_LESSON_SHARED_KEY_VALUES,
        ...INIT_LESSON_KEY_VALUES_BY_CATEGORY[value.toLowerCase()],
      });
    } else {
      this.props.onSetLessonValues({
        [dropDownType]: value,
      });
    }

    this.setState({
      errorFields: { ...this.state.errorFields, [dropDownType]: false },
    });
  };

  /*  Icon Handlers */

  handleChangeInput = async (files) => {
    if (files) {
      const optimizedImage = await optimizeImage(files, {
        ...CARD_OPTIONS,
        outputAddress: `#${OPTIMIZED_ICON_IMAGE_ID}`,
      });

      this.setState({
        iconFile: optimizedImage,
        iconName: files[0].name,
      });
    }
  };

  uploadIconToDb = () => {
    const { iconFile, iconName } = this.state;
    const { firebase } = this.props;

    const createdTime = new Date().getTime();
    const referencedValue = `${LESSONS_BUCKET_NAME}/${createdTime}-icon-${iconName}`;

    const storageRef = firebase.storage.ref(referencedValue);

    storageRef
      .put(iconFile)
      .then(() => {
        this.props.firebase.storage
          .ref()
          .child(referencedValue)
          .getDownloadURL()
          .then((url) => {
            this.setState({ iconSrc: referencedValue });
            this.props.onSetLessonValues({
              iconPath: url,
            });
          })
          .then(() => infoAutoCloseAlert(ICON_LESSON_ADD_ALERT))
          .catch((error) =>
            infoAutoCloseAlert({
              ...ERROR_ALERT,
              text: error.message,
            })
          );
      })
      .catch((error) => {
        infoAutoCloseAlert({
          ...ERROR_ALERT,
          text: error.message,
        });
      });
  };

  removeIcon = () => {
    const { iconPath } = this.props.newLessonState;
    const { firebase } = this.props;

    firebase.storage
      .refFromURL(iconPath)
      .delete()
      .then(() => infoAutoCloseAlert(ICON_LESSON_REMOVE_ALERT))
      .catch((error) =>
        infoAutoCloseAlert({ ...ERROR_ALERT, text: error.message })
      );

    this.setState({
      modalState: false,
      iconFile: null,
      iconName: null,
      iconSrc: "",
    });

    this.props.onSetLessonValues({ iconPath: "" });
    if (this.fileInputRef.current) {
      this.fileInputRef.current.value = "";
    }
  };

  /* Submit Handlers */

  onSuccessSubmit = (text) => {
    this.setState({ isLoading: false });

    infoAutoCloseAlert({ ...LESSON_STATUS_ALERT, text });

    this.props.onGetAllLessons(this.props.firebase);
    this.props.onSetLessonToInit();

    this.setState({ iconFile: null });
  };

  pushFormattedLessonToDb = (formattedLesson) => {
    const { newLessonState } = this.props;
    const { lessonMode, uid } = newLessonState;
    const { firebase } = this.props;

    if (lessonMode === LESSON_MODE.CREATE) {
      firebase
        .posts()
        .push()
        .set(formattedLesson)
        .then(() => this.onSuccessSubmit("The lesson has been created!"))
        .catch(({ text }) => infoAutoCloseAlert({ ...ERROR_ALERT, text }));
    } else if (lessonMode === LESSON_MODE.EDIT) {
      firebase.db
        .ref(`${LESSONS_BUCKET_NAME}/${uid}`)
        .update(formattedLesson)
        .then(() => this.onSuccessSubmit("The lesson has been edited!"))
        .catch(({ text }) => infoAutoCloseAlert({ ...ERROR_ALERT, text }));
    }
  };

  formatLessonStructure = () => {
    const { newLessonState } = this.props;
    const {
      uid,
      title,
      date,
      assets,
      category,
      subCategory,
      iconPath,
      lessonPracticalContent,
      lessonTheoreticalContent,
      lessonMode,
      localStorageData,
    } = newLessonState;

    return {
      uid,
      title,
      iconPath,
      category,
      subCategory,
      lessonPracticalContent,
      lessonTheoreticalContent,
      date: lessonMode === LESSON_MODE.CREATE ? new Date().getTime() : date,
      localStorageData,
      /* optional keys
         insert them only if they exist
      */
      ...(assets && { assets }),
      ...(newLessonState.contentVideoValues && {
        contentVideoValues: newLessonState.contentVideoValues,
      }),
    };
  };

  convertEditorToHtml = () => {
    const { lessonTheoreticalContent = {} } = this.props.newLessonState;
    const clonedContent = Object.assign({}, lessonTheoreticalContent);

    Object.entries(clonedContent || {}).forEach(([key, values]) => {
      // _immutable means that the value in EditorState
      if (values && values._immutable) {
        clonedContent[key] = JSON.stringify(
          draftToHtml(convertToRaw(values.getCurrentContent()))
        );
      }
    });

    this.props.onSetLessonValues({
      lessonTheoreticalContent: clonedContent,
    });
  };

  formatLesson = () => {
    /* Convert EditorState to html format because it has undefined values, 
       therefore we can't push it into DB
    */
    this.convertEditorToHtml();

    this.pushFormattedLessonToDb(this.formatLessonStructure());
  };

  checkEachRequiredField = () => {
    const { newLessonState } = this.props;
    const { title } = newLessonState;
    // clone state to avoid immutability
    const errorFields = Object.assign({}, this.state.errorFields);

    let isAllFieldFilled;

    // iterate through each obj
    [{ title }].forEach((obj) => {
      // equal to false if there's an empty field
      const valueExist = !!Object.values(obj)[0];
      isAllFieldFilled = valueExist;
      if (!valueExist) {
        errorFields[Object.keys(obj)[0]] = !valueExist;
      }
    });

    this.setState({ errorFields });

    return isAllFieldFilled;
  };

  onSubmitLesson = () => {
    confirmationAlert(CREATE_EDIT_LESSON_CONFIRAMTION).then(
      ({ isConfirmed }) => {
        if (isConfirmed && this.checkEachRequiredField()) {
          // formatt lesson before sending it to DB
          this.formatLesson();
        }
      }
    );
  };

  render() {
    const { isLoading, exercises, errorFields, error, modalState } = this.state;
    const { newLessonState } = this.props;
    const {
      category = "",
      subCategory,
      title,
      iconPath,
      lessonMode,
      reset,
    } = newLessonState;

    const initialState = {
      ...INIT_LESSON_SHARED_KEY_VALUES,
      ...INIT_LESSON_KEY_VALUES_BY_CATEGORY[category.toLowerCase()],
      ...(reset && { reset }),
      category,
    };

    // assign persist values from the local storage
    initialState["_persist"] = newLessonState._persist;

    /* check if a user has made some progress, if so, he or she might be able to hit the cancel button */
    const isObjectsEqual = _.isEqual(newLessonState, initialState);

    return isLoading ? (
      <Segment className="loader-whole-screen">
        <Dimmer active>
          <Loader size="massive" inverted>
            Loading
          </Loader>
        </Dimmer>
      </Segment>
    ) : error && !isLoading ? (
      <ErrorMessage content={error.message} />
    ) : (
      <div>
        <Form>
          <Form.Group widths="equal">
            <Form.Field>
              <Form.Dropdown
                className="capitalize"
                disabled={!isObjectsEqual}
                label={ADMIN_DROPDOWN_TITLES.category.label}
                selection
                search
                error={errorFields[ADMIN_DROPDOWN_TITLES.category.defaultVal]}
                value={category}
                options={CATEGORIES}
                onChange={(e, { value }) =>
                  this.onDropDownChange(
                    value,
                    ADMIN_DROPDOWN_TITLES.category.defaultVal
                  )
                }
                placeholder="Select Category"
              />
            </Form.Field>
            <Form.Field>
              <Form.Dropdown
                className="capitalize"
                label={ADMIN_DROPDOWN_TITLES.subCategory.label}
                selection
                search
                error={
                  errorFields[ADMIN_DROPDOWN_TITLES.subCategory.defaultVal]
                }
                value={subCategory}
                options={transformToOptions(SUB_CATEGORIES)}
                placeholder={ADMIN_DROPDOWN_TITLES.subCategory.placeholder}
                onChange={(e, { value }) =>
                  this.onDropDownChange(
                    value,
                    ADMIN_DROPDOWN_TITLES.subCategory.defaultVal
                  )
                }
              />
            </Form.Field>
          </Form.Group>
        </Form>
        <Form widths="equal">
          <Form.Group widths="equal">
            <Form.Field required>
              <label>{ADMIN_DROPDOWN_TITLES.title.label}</label>
              <Form.Input
                error={errorFields.title}
                value={title}
                onChange={(e, { value }) =>
                  this.onDropDownChange(
                    value,
                    ADMIN_DROPDOWN_TITLES.title.defaultVal
                  )
                }
                placeholder={ADMIN_DROPDOWN_TITLES.title.placeholder}
              />
            </Form.Field>
            <Form.Field>
              <label>
                Icon Settings
                <Popup
                  inverted
                  className="icon-settings__popup"
                  content="Recommended image size measurements: 260px wide by 180px tall."
                  trigger={
                    <Icon className="icon-trigger" name="question circle" />
                  }
                />
              </label>

              <input
                accept="image/*"
                ref={this.fileInputRef}
                type="file"
                hidden
                id="imageFile"
                onChange={(e) => {
                  this.handleChangeInput(e.target.files);

                  this.setState({
                    modalState: true,
                  });
                }}
              />
              <Button.Group className="create-lesson__image-button-group" fluid>
                <Button
                  disabled={iconPath !== ""}
                  content="Upload an icon"
                  labelPosition="left"
                  icon="file"
                  onClick={() => this.fileInputRef.current.click()}
                />
                <Button
                  color="red"
                  disabled={iconPath === ""}
                  style={{ cursor: !iconPath ? "not-allowed" : "pointer" }}
                  type="submit"
                  onClick={() =>
                    confirmationAlert(REMOVE_IMAGE_CONFIRMATION).then(
                      ({ isConfirmed }) => isConfirmed && this.removeIcon()
                    )
                  }
                >
                  Remove an icon
                </Button>
              </Button.Group>
            </Form.Field>
          </Form.Group>
        </Form>
        <Tab
          className="create-lesson__tab-lesson-stages"
          menu={{ secondary: true, pointing: true }}
          panes={[
            ...buildSectionsByCategory(category.toLowerCase(), exercises),
            {
              menuItem: CREATE_LESSON_STAGES.preview,
              render: () => (
                <Tab.Pane>
                  <LessonPreview
                    category={newLessonState.category}
                    title={title}
                    iconSrc={newLessonState.iconPath}
                    sectionKey={PREVIEW_MODE}
                  />
                </Tab.Pane>
              ),
            },
          ]}
        />
        <div className="create-lesson__submit-button-container">
          <Button primary onClick={this.onSubmitLesson}>
            {lessonMode === LESSON_MODE.EDIT
              ? "Update changes"
              : "Create a lesson"}
          </Button>
        </div>

        {modalState && (
          <Modal open={modalState}>
            <Modal.Header>Upload image</Modal.Header>
            <Modal.Content image>
              <div className="create-lesson__upload-image-container mr-1">
                <Image id={OPTIMIZED_ICON_IMAGE_ID} alt="icon" />
              </div>
              <Modal.Description>
                <p>Would you like to upload this image?</p>
                <p>
                  Recommended image size measurements: 260px wide by 180px tall.
                </p>
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
              <Button
                onClick={() => {
                  this.setState({
                    modalState: false,
                    iconFile: null,
                    iconSrc: "",
                  });
                  this.props.onSetLessonValues({ iconPath: "" });

                  if (this.fileInputRef.current) {
                    this.fileInputRef.current.value = "";
                  }
                }}
              >
                Cancel
              </Button>
              <Button
                onClick={() => {
                  this.uploadIconToDb();
                  this.setState({ modalState: false });
                }}
                positive
              >
                Ok
              </Button>
            </Modal.Actions>
          </Modal>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ lessons, newLessonState }) => ({
  lessons,
  newLessonState,
});

const mapDispatchToProps = (dispatch) => ({
  onGetAllLessons: (database) => dispatch(getAllLessons(database)),
  onSetLessonValues: (values) => dispatch(setLessonValues(values)),
  onSetLessonToInit: () => dispatch(setLessonToInit()),
  onSetLessonToInitByCategory: (values) =>
    dispatch(setLessonToInitByCategory(values)),
});

const condition = (authUser) => authUser && !!authUser.roles[ROLES.ADMIN];

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withAuthorization(condition),
  withFirebase
)(CreateLessonTemplate);
