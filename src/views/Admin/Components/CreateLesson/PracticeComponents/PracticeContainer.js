import React, { PureComponent } from "react";
import { connect } from "react-redux";
import {
  Form,
  Header,
  Button,
  Segment,
  Transition,
  Popup,
} from "semantic-ui-react";
import {
  INIT_FIELDS_CONTENT,
  EXERCISES_NAMES,
  WORDS_IN_CONTEXT,
  CLOZE,
  FILL_IN_THE_BLANKS,
  DEFINITIONS,
  ANOTHER_WAY_TO_SAY,
} from "../../../../../constants";
import { REMOVE_EXERCISE_CONFIRMATION } from "../../../../../constants/alertContent";
import { setLessonValues } from "../../../../../redux/actions";
import { confirmationAlert } from "../../../../../utils/fireAlert";
import CompleteSentencesExercise from "./CompleteSentencesExercise";
import ClozeSentencesExercise from "./ClozeSentencesExercise";
import DefinitionExercise from "./DefinitionExercise";

// style
import "./style.scss";

class PracticeContainer extends PureComponent {
  state = {
    exercisesViewState: {
      allExercisesView: true,
    },
    removeProcess: false,
    exercisesComponents: {
      [CLOZE]: ClozeSentencesExercise,
      [DEFINITIONS]: DefinitionExercise,
      [WORDS_IN_CONTEXT]: CompleteSentencesExercise,
      [FILL_IN_THE_BLANKS]: CompleteSentencesExercise,
      [ANOTHER_WAY_TO_SAY]: CompleteSentencesExercise,
    },
  };

  onDropDownChange = (value, dropDownType, exerciseId) => {
    const { lessonPracticalContent = [] } = this.props.newLessonState;

    this.props.onSetLessonValues({
      lessonPracticalContent: lessonPracticalContent.map((obj) =>
        obj.id === exerciseId
          ? {
              ...obj,
              [dropDownType]: value,
              content: [INIT_FIELDS_CONTENT[value] || {}],
            }
          : obj
      ),
    });
  };

  removeExerciseById = (exerciseId, stateExerciseView) => {
    const { lessonPracticalContent } = this.props.newLessonState;
    confirmationAlert(REMOVE_EXERCISE_CONFIRMATION).then((res) => {
      if (res.value) {
        let clonedArray = Array.from(lessonPracticalContent);

        clonedArray = clonedArray
          .filter(({ id }) => id !== exerciseId)
          .map((obj) =>
            obj.id > exerciseId ? { ...obj, id: obj.id - 1 } : { ...obj }
          );

        this.props.onSetLessonValues({ lessonPracticalContent: clonedArray });

        // set view to undefined because we removed an object from an array
        this.setState({
          exercisesViewState: {
            ...this.state.exercisesViewState,
            [stateExerciseView]: undefined,
          },
          removeProcess: true,
        });
      }
    });
  };

  componentDidUpdate() {
    if (this.state.removeProcess) {
      this.setState({
        removeProcess: false,
      });
    }
  }

  addExercise = () => {
    const { lessonPracticalContent = [] } = this.props.newLessonState;
    const currentId = lessonPracticalContent.length || 0;

    const name = EXERCISES_NAMES[currentId]
      ? EXERCISES_NAMES[currentId].text
      : EXERCISES_NAMES[0].text;

    this.props.onSetLessonValues({
      lessonPracticalContent: lessonPracticalContent.concat({
        id: currentId,
        name,

        content: [INIT_FIELDS_CONTENT[name] || {}],
      }),
    });
  };

  // handle views by name of an exercise
  toggleView = (stateName) => {
    const { exercisesViewState } = this.state;
    // if init exercise === undefined => exercise is currently open
    this.setState({
      exercisesViewState: {
        ...exercisesViewState,
        [stateName]:
          exercisesViewState[stateName] === undefined
            ? false
            : !exercisesViewState[stateName],
      },
    });
  };

  closeView = (stateName) =>
    this.setState({
      [stateName]: false,
    });

  openView = (stateName) =>
    this.setState({
      [stateName]: true,
    });

  render() {
    const {
      exercisesViewState,
      removeProcess,
      exercisesComponents,
    } = this.state;
    const { lessonPracticalContent = [] } = this.props.newLessonState;

    return (
      <div>
        <Segment className="new-field__header-container" secondary>
          <Header className="new-field__header" as="h2">
            {`You created ${lessonPracticalContent.length} ${
              lessonPracticalContent.length === 1 ? "exercises" : "exercises"
            }`}
          </Header>
          <div>
            {exercisesViewState["allExercisesView"] !== null && (
              <Popup
                inverted
                position="top center"
                content={
                  exercisesViewState["allExercisesView"]
                    ? "Hide all exercises."
                    : "Show all exercises."
                }
                trigger={
                  <Button
                    basic
                    color="brown"
                    icon={
                      exercisesViewState["allExercisesView"]
                        ? "eye slash"
                        : "eye"
                    }
                    onClick={() => this.toggleView("allExercisesView")}
                  />
                }
              />
            )}
            <Popup
              inverted
              position="top center"
              content="Add an exercise"
              trigger={
                <Button
                  basic
                  color="green"
                  icon="plus"
                  onClick={() => {
                    this.openView("allExercisesView");
                    this.addExercise();
                  }}
                />
              }
            />
          </div>
        </Segment>
        <Transition
          visible={exercisesViewState["allExercisesView"]}
          animation="fade"
          duration={0}
          unmountOnHide={true}
          key="allExercisesView"
        >
          <>
            {!!lessonPracticalContent.length &&
              lessonPracticalContent.map((currentExerciseValues) => {
                const { id, name, content } = currentExerciseValues;

                const stateExercise = `${id}-${name}`;

                // we're getting undefined when component just mounted which means an exercise is visible by default
                const isExerciseOpen =
                  exercisesViewState[stateExercise] === undefined ||
                  exercisesViewState[stateExercise] === true;

                const MatchedExerciseComponent = exercisesComponents[name];

                return (
                  <Transition
                    visible={this.state[stateExercise] === false ? false : true}
                    /* visible={true} */
                    animation="fade"
                    duration={1000}
                    key={id}
                  >
                    <Segment secondary>
                      <div className="new-field-builder__container">
                        <div className="new-field__header-container">
                          <Header className="new-field__header" as="h3">
                            {`${id + 1}. ${name}`}
                          </Header>
                          <div className="new-field__header-toolbar">
                            <Popup
                              inverted
                              position="top center"
                              content={
                                isExerciseOpen
                                  ? "Hide content."
                                  : "Show content."
                              }
                              trigger={
                                <Button
                                  color="brown"
                                  className="new-field-builder__button--toggle-view"
                                  icon={isExerciseOpen ? "eye slash" : "eye"}
                                  onClick={() => this.toggleView(stateExercise)}
                                />
                              }
                            />

                            <Popup
                              basic
                              inverted
                              content="Remove this exercise."
                              trigger={
                                <Button
                                  color="red"
                                  icon="remove"
                                  className="button-remove"
                                  onClick={() =>
                                    this.removeExerciseById(id, stateExercise)
                                  }
                                />
                              }
                            />
                          </div>
                        </div>
                        <Transition
                          visible={isExerciseOpen}
                          animation="fade"
                          duration={500}
                          transitionOnMount={true}
                          unmountOnHide={true}
                        >
                          <>
                            <Form
                              widths="equal"
                              className="practice-container__form"
                            >
                              <Form.Group>
                                <Form.Field>
                                  <Form.Dropdown
                                    disabled={content && content.length > 1}
                                    label="Title"
                                    placeholder="title"
                                    selection
                                    search
                                    value={name}
                                    options={EXERCISES_NAMES}
                                    onChange={(e, { value }) =>
                                      this.onDropDownChange(value, "name", id)
                                    }
                                  />
                                </Form.Field>
                              </Form.Group>
                            </Form>
                            {/* render an exercise component */}
                            {!removeProcess && (
                              <MatchedExerciseComponent
                                currentExerciseValues={currentExerciseValues}
                              />
                            )}
                          </>
                        </Transition>
                      </div>
                    </Segment>
                  </Transition>
                );
              })}
          </>
        </Transition>
      </div>
    );
  }
}

const mapStateToProps = ({ newLessonState }) => ({ newLessonState });

const mapDispatchToProps = (dispatch) => ({
  onSetLessonValues: (values) => dispatch(setLessonValues(values)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PracticeContainer);
