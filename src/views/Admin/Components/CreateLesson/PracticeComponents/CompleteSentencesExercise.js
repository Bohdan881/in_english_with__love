import React, { Component } from "react";
import { connect } from "react-redux";
import { setLessonValues } from "../../../../../redux/actions";
import {
  Segment,
  Icon,
  Button,
  Label,
  Statistic,
  Grid,
  Container,
  TextArea,
  Popup,
  Header,
  List,
} from "semantic-ui-react";
import _ from "lodash";
import {
  EXERCISES_LABELS_COLORS,
  INIT_FIELDS_CONTENT,
  COMPLETE_FIELDS,
  ANOTHER_WAY_TO_SAY,
  FILL_IN_THE_BLANKS,
  WORDS_IN_CONTEXT,
} from "../../../../../constants";

const TutorialContent = ({ name }) => (
  <div className="tutorial-popup-container">
    <Header as="h4">
      Please follow the below steps to complete the exercise.
    </Header>
    <List bulleted>
      <List.Item>Fill all the fields.</List.Item>
      {name === ANOTHER_WAY_TO_SAY ? (
        <List.Item>
          Wrap an answer in curly braces in <b> Sentence</b> field.
          <br /> For example:
          <span className="tutorial-popup-container__sentence-span">
            <q>
              I think we need to
              <b>
                {" ("} practice{" )  "}
                {" {"} rehearse{" }  "}
              </b>
              first scene again.
            </q>
          </span>
        </List.Item>
      ) : (
        <>
          {name === FILL_IN_THE_BLANKS && (
            <List.Item>
              Write down an initial word or phrase in
              <b> Initial Word Field </b>.
              <br /> For example:
              <span className="tutorial-popup-container__sentence-span">
                <b> Turns out </b>
              </span>
              <br />
            </List.Item>
          )}
          <List.Item>
            Wrap an answer in curly braces and a tooltip in brackets in
            <b> Sentence</b> field.
            <br /> For example:
            <span className="tutorial-popup-container__sentence-span">
              <q>
                It
                <b>
                  {" {"} turns out {"}  "}
                  {/* {" ("} became known {")  "} */}
                </b>
                that eating some fat is actually good for you.
              </q>
            </span>
            <br />
          </List.Item>
        </>
      )}
      <List.Item>
        Once you wrapped it, you should see an answer in an appropriate field.
      </List.Item>
    </List>
  </div>
);

class CompleteSentencesExercise extends Component {
  state = {
    content: [],
    exerciseName: null,
    exerciseId: null,
  };

  addField = () => {
    const { exerciseId, exerciseName } = this.state;
    const { lessonPracticalContent } = this.props.newLessonState;
    const lessonIdValues = lessonPracticalContent[exerciseId];

    const incrementedNumber =
      lessonIdValues && lessonIdValues.content.length
        ? lessonIdValues.content.length
        : 0;

    this.props.onSetLessonValues({
      lessonPracticalContent: lessonPracticalContent.map((obj) =>
        obj.id === exerciseId
          ? {
              ...obj,
              content: obj.content.concat({
                ...INIT_FIELDS_CONTENT[exerciseName],
                id: incrementedNumber,
              }),
            }
          : obj
      ),
    });

    this.setState({
      content: this.state.content.concat({ id: incrementedNumber }),
    });
  };

  idetifyAnswerValue = (value) => {
    const wordsInCurlyBraces = /\{.*?\}/g;
    const removeSpecialCharacters = /[^a-z0-9&@.$%\-,-:;()', A-Z]/g;

    const matchedValue = value.match(wordsInCurlyBraces);

    const answerValue = matchedValue
      ? matchedValue.map((element) =>
          element.replace(removeSpecialCharacters, " ")
        )
      : [""];

    return !!answerValue[0] ? answerValue.join("").trim() : "";
  };

  onChangeValue = (value, fieldId) => {
    const { content, exerciseName } = this.state;

    const contentIndex = content.findIndex((obj) => obj.id === fieldId);

    const answer = this.idetifyAnswerValue(value);

    const isInitWordRequired = exerciseName === FILL_IN_THE_BLANKS;

    this.setState({
      content:
        contentIndex === -1
          ? [...content, { id: fieldId, sentence: value }]
          : content.map((obj) =>
              obj.id === fieldId
                ? {
                    ...obj,
                    sentence: value,
                    ...(answer && { answer }),
                    ...(answer &&
                      isInitWordRequired && {
                        initWord: answer,
                      }),
                  }
                : obj
            ),
    });

    this.persistChangedValueToRedux(value, answer, isInitWordRequired, fieldId);
  };

  persistChangedValueToRedux = _.debounce(
    (value, answer, isInitWord, fieldId) => {
      const { exerciseId } = this.state;
      const { lessonPracticalContent } = this.props.newLessonState;

      this.props.onSetLessonValues({
        lessonPracticalContent: lessonPracticalContent.map((obj) =>
          obj.id === exerciseId
            ? {
                ...obj,
                content: obj.content.map((nestedObj) =>
                  nestedObj.id === fieldId
                    ? {
                        ...nestedObj,
                        sentence: value,
                        ...(answer && { answer }),
                        ...(answer && isInitWord && { initWord: answer }),
                      }
                    : nestedObj
                ),
              }
            : obj
        ),
      });
    },
    300
  );

  onChangeInitialWord = _.debounce((value, fieldId) => {
    const { exerciseId, content } = this.state;
    const { lessonPracticalContent } = this.props.newLessonState;

    this.setState({
      content: content.map((obj) =>
        obj.id === fieldId
          ? {
              ...obj,
              initWord: value,
            }
          : obj
      ),
    });

    // persist it in redux
    this.props.onSetLessonValues({
      lessonPracticalContent: lessonPracticalContent.map((obj) =>
        obj.id === exerciseId
          ? {
              ...obj,
              content: obj.content.map((nestedObj) =>
                nestedObj.id === fieldId
                  ? {
                      ...nestedObj,
                      initWord: value,
                    }
                  : nestedObj
              ),
            }
          : obj
      ),
    });
  }, 0);

  removeLastField = () => {
    const { lessonPracticalContent } = this.props.newLessonState;
    const { exerciseId } = this.state;
    // clone object & array to keep props immutable
    const exercise = _.cloneDeep(lessonPracticalContent[exerciseId]);
    const content = _.cloneDeep(this.state.content);

    // remove the last field
    exercise.content.pop();
    content.pop();

    this.setState({ content });

    this.props.onSetLessonValues({
      lessonPracticalContent: lessonPracticalContent.map((obj) =>
        obj.id !== exerciseId ? obj : exercise
      ),
    });
  };

  componentDidMount() {
    const { content, name, id } = this.props.currentExerciseValues;

    this.setState({ exerciseName: name, exerciseId: id, content });
  }

  render() {
    const { content, exerciseName, exerciseId } = this.state;

    const requireInitWord =
      exerciseName === FILL_IN_THE_BLANKS || exerciseName === WORDS_IN_CONTEXT;

    const labelColorIndex =
      exerciseId < EXERCISES_LABELS_COLORS.length ? exerciseId : 0;

    return (
      <Segment className="exercise-container">
        <div className="exercise-container__toolbox">
          <Label
            color={EXERCISES_LABELS_COLORS[labelColorIndex]}
            size="large"
            ribbon
            className="exercise-container__label-name"
          >
            {exerciseName && exerciseName.toUpperCase()}
          </Label>
          <div>
            <Statistic
              horizontal
              size="mini"
              className="exercise-container__fields-quantity"
              color="brown"
            >
              <Statistic.Label> Total: </Statistic.Label>
              <Statistic.Value>
                <span>{content.length}</span>
              </Statistic.Value>
            </Statistic>
            <Popup
              content={<TutorialContent name={exerciseName} />}
              wide="very"
              on="click"
              trigger={
                <Button
                  basic
                  color="purple"
                  icon
                  labelPosition="right"
                  disabled={!content.length}
                >
                  Quick tutorial
                  <Icon name="info" />
                </Button>
              }
            />
            <Button
              basic
              color="red"
              icon
              labelPosition="right"
              disabled={!content.length}
              onClick={() => this.removeLastField()}
            >
              Remove last field <Icon name="minus" />
            </Button>
            <Button
              icon
              labelPosition="right"
              basic
              color="green"
              onClick={() => this.addField()}
            >
              Add field <Icon name="plus" />
            </Button>
          </div>
        </div>
        <div>
          <Segment tertiary>
            <Grid className="exercise-container__builder-container">
              {content.length ? (
                content.map(({ id }) => (
                  <Grid.Row key={id}>
                    <Grid.Column
                      widescreen={requireInitWord ? 12 : 14}
                      largeScreen={requireInitWord ? 12 : 14}
                      computer={requireInitWord ? 10 : 12}
                      tablet={requireInitWord ? 10 : 12}
                    >
                      <Container fluid className="d-flex flex-column w-100">
                        <label className="exercise-container__textarea-label">
                          {COMPLETE_FIELDS.sentence.label}
                        </label>
                        <TextArea
                          value={content[id] ? content[id].sentence : ""}
                          placeholder={COMPLETE_FIELDS.sentence.placeholder}
                          className="exercise-container__text-area"
                          onChange={(e, { value }) =>
                            this.onChangeValue(value, id)
                          }
                        />
                      </Container>
                    </Grid.Column>
                    <Grid.Column
                      className="p-0"
                      widescreen={2}
                      largeScreen={2}
                      tablet={requireInitWord ? 3 : 4}
                    >
                      <Container fluid>
                        <label className="exercise-container__textarea-label">
                          {COMPLETE_FIELDS.asnwer.label}
                        </label>
                        <TextArea
                          value={content[id] ? content[id].answer : ""}
                          placeholder={COMPLETE_FIELDS.asnwer.placeholder}
                          className="exercise-container__text-area answer-textarea"
                          disabled
                        />
                      </Container>
                    </Grid.Column>
                    {requireInitWord && (
                      <Grid.Column
                        widescreen={2}
                        largeScreen={2}
                        tablet={3}
                        className="p-0"
                      >
                        <Container fluid>
                          <label className="exercise-container__textarea-label">
                            {COMPLETE_FIELDS.initWord.label}
                          </label>
                          <TextArea
                            value={content[id] ? content[id].initWord : ""}
                            placeholder={COMPLETE_FIELDS.initWord.placeholder}
                            className="exercise-container__text-area init-word-textarea border-0"
                            onChange={(e, { value }) =>
                              this.onChangeInitialWord(value, id)
                            }
                          />
                        </Container>
                      </Grid.Column>
                    )}
                  </Grid.Row>
                ))
              ) : (
                <p className="pt-1 pb-1"> No Fields Found</p>
              )}
            </Grid>
          </Segment>
        </div>
      </Segment>
    );
  }
}

const mapStateToProps = ({ newLessonState }) => ({ newLessonState });

const mapDispatchToProps = (dispatch) => ({
  onSetLessonValues: (values) => dispatch(setLessonValues(values)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompleteSentencesExercise);
