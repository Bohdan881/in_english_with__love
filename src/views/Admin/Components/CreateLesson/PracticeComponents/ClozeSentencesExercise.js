import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Header,
  Icon,
  Label,
  List,
  Segment,
  Popup,
  Grid,
  TextArea,
  Container,
} from "semantic-ui-react";
import { EXERCISES_LABELS_COLORS } from "../../../../../constants";
import { setLessonValues } from "../../../../../redux/actions";
import _ from "lodash";

const TutorialContent = () => (
  <div className="tutorial-popup-container">
    <Header as="h4">
      Please follow the below steps to complete the exercise.
    </Header>
    <List bulleted>
      <List.Item>
        Wrap an answer in curly braces in the Text field.
        <br /> For example:
        <span className="tutorial-popup-container__sentence-span">
          <q>
            I think we need to
            <b>
              {" {"} rehearse{" }  "}
            </b>
            first scene again.
          </q>
        </span>
      </List.Item>
      <List.Item>
        Please use {`<br/>`} to produce a line break in text.
      </List.Item>
    </List>
  </div>
);

const ClozeSentencesExercise = ({ currentExerciseValues }) => {
  // redux ---------------------------------
  const { newLessonState } = useSelector((db) => db);

  const { id, name, type } = currentExerciseValues;

  const practicalContent = currentExerciseValues.content[0] || {};

  const dispatch = useDispatch();
  // ----------------------------------------

  // states ---------------------------------
  const [content, setContent] = useState("");
  // ----------------------------------------

  // effects ---------------------------------
  useEffect(() => {
    setContent(practicalContent && practicalContent.text);
    // eslint-disable-next-line
  }, []);
  // ----------------------------------------

  // handlers ---------------------------------
  const handleContent = (value) => {
    setContent(value);
    onPersistContentInRedux(value);
  };

  const onPersistContentInRedux = _.debounce((value) => {
    let clonedArray = _.cloneDeep(newLessonState.lessonPracticalContent);

    const wordsInCurlyBraces = /\{.*?\}/g;
    const removeSpecialCharacters = /[^a-z0-9&@.$%\-,-:;()', A-Z]/g;

    const answerValues = value.match(wordsInCurlyBraces);

    let answers =
      answerValues && answerValues.length > 0
        ? answerValues.map((element) =>
            element.replace(removeSpecialCharacters, " ")
          )
        : [""];

    clonedArray[id].content = [
      {
        text: value,
        answers,
      },
    ];

    dispatch(
      setLessonValues({
        lessonPracticalContent: clonedArray,
      })
    );
  }, 0);

  return (
    <Segment className="exercise-container">
      <div className="exercise-container__toolbox">
        <Label
          color={
            id < EXERCISES_LABELS_COLORS.length
              ? EXERCISES_LABELS_COLORS[id]
              : "blue"
          }
          size="large"
          ribbon
          className="exercise-container__label-name"
        >
          {name && name.toUpperCase()}
        </Label>

        <Popup
          basic
          content={<TutorialContent type={type} />}
          wide="very"
          on="click"
          trigger={
            <Button
              basic
              color="purple"
              icon
              disabled={!content || !content.length}
            >
              Quick tutorial
              <Icon name="info" />
            </Button>
          }
        />
      </div>
      {currentExerciseValues.content && currentExerciseValues.content.length && (
        <div>
          <Segment tertiary>
            <Grid className="exercise-container__builder-container">
              <Grid.Row className="cloze-exercise__fields-container">
                <Grid.Column largeScreen={12}>
                  <Container className="cloze-exercise__field" fluid>
                    <label className="exercise-container__textarea-label">
                      Text
                    </label>
                    <TextArea
                      className="exercise-container__text-area w-100 h-100"
                      value={content}
                      onChange={(e, { value }) => handleContent(value)}
                    />
                  </Container>
                </Grid.Column>
                <Grid.Column widescreen={4} largeScreen={4}>
                  <Container className="cloze-exercise__field" fluid>
                    <label className="exercise-container__textarea-label">
                      Answer list
                    </label>
                    <List className="exercise-container__text-area d-flex flex-column text-black h-100 bg-white">
                      {practicalContent &&
                        practicalContent.answers &&
                        practicalContent.answers.map(
                          (answer, key) =>
                            answer !== "" && (
                              <List.Item key={answer}>{`${
                                key + 1
                              }. ${answer}`}</List.Item>
                            )
                        )}
                    </List>
                  </Container>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Segment>
        </div>
      )}
    </Segment>
  );
};

export default ClozeSentencesExercise;
