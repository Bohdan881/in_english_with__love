import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Button,
  Container,
  Grid,
  Icon,
  Label,
  Popup,
  Segment,
  Statistic,
  TextArea,
} from "semantic-ui-react";
import _ from "lodash";

import {
  DEFINITIONS,
  EXERCISES_LABELS_COLORS,
  INIT_FIELDS_CONTENT,
} from "../../../../../constants";
import { setLessonValues } from "../../../../../redux/actions";

const TutorialContent = () => (
  <div className="tutorial-popup-container">
    <p>
      Please type a word in the left field and its description in the right
      field.
    </p>
  </div>
);

const DefenitionExercise = ({ currentExerciseValues = {} }) => {
  // redux ---------------------------------

  const { newLessonState } = useSelector((db) => db);

  const dispatch = useDispatch();
  // ----------------------------------------

  // constants ------------------------------
  const { id, name } = currentExerciseValues;
  const practicalContent = newLessonState.lessonPracticalContent;

  // ----------------------------------------

  // states ---------------------------------
  const [content, setContent] = useState([INIT_FIELDS_CONTENT[DEFINITIONS]]);
  const [exerciseId] = useState(id);
  // ----------------------------------------

  // effects ---------------------------------
  useEffect(() => {
    const currentObj = practicalContent[id];

    if (currentObj && currentObj.content) {
      setContent(currentObj.content);
    }

    // eslint-disable-next-line
  }, []);
  // ----------------------------------------

  // handlers -------------------------------

  const handleContent = (value, keyName, fieldId) => {
    const contentIndex = content.findIndex((obj) => obj.id === fieldId);

    setContent(
      contentIndex === -1
        ? [
            ...content,
            {
              ...[INIT_FIELDS_CONTENT[DEFINITIONS]],
              id: fieldId,
              [keyName]: value,
            },
          ]
        : content.map((obj) =>
            obj.id === fieldId
              ? {
                  ...obj,
                  [keyName]: value,
                }
              : obj
          )
    );

    onPersistContentInRedux(value, keyName, fieldId);
  };

  const onPersistContentInRedux = _.debounce((value, keyName, fieldId) => {
    dispatch(
      setLessonValues({
        lessonPracticalContent: practicalContent.map((obj) =>
          obj.id === exerciseId
            ? {
                ...obj,
                content: obj.content.map((nestedObj) =>
                  nestedObj.id === fieldId
                    ? {
                        ...nestedObj,
                        [keyName]: value,
                      }
                    : nestedObj
                ),
              }
            : obj
        ),
      })
    );
  }, 300);

  const removeField = () => {
    const { lessonPracticalContent } = newLessonState;

    // clone object && array to keep props immutable
    const exercise = Object.assign({}, lessonPracticalContent[exerciseId]);

    const clonedContent = _.cloneDeep(content);

    // remove the last field
    exercise.content.pop();
    clonedContent.pop();

    setContent(clonedContent);

    dispatch(
      setLessonValues({
        lessonPracticalContent: lessonPracticalContent.map((obj) =>
          obj.id !== exerciseId ? obj : exercise
        ),
      })
    );
  };

  const addField = () => {
    const { lessonPracticalContent } = newLessonState;

    const exercise = lessonPracticalContent[exerciseId];

    const incrementedNumber =
      exercise && exercise.content.length ? exercise.content.length : 0;

    dispatch(
      setLessonValues({
        lessonPracticalContent: lessonPracticalContent.map((obj) =>
          obj.id === exerciseId
            ? {
                ...obj,
                content: obj.content.concat({
                  ...INIT_FIELDS_CONTENT[DEFINITIONS],
                  id: incrementedNumber,
                }),
              }
            : obj
        ),
      })
    );

    setContent(
      content.concat({
        ...INIT_FIELDS_CONTENT[DEFINITIONS],
        id: incrementedNumber,
      })
    );
  };

  // ----------------------------------------

  return (
    <Segment className="exercise-container">
      <div className="exercise-container__toolbox">
        <Label
          color={
            id < EXERCISES_LABELS_COLORS.length
              ? EXERCISES_LABELS_COLORS[id]
              : "blue"
          }
          size="large"
          ribbon
          className="exercise-container__label-name"
        >
          {name && name.toUpperCase()}
        </Label>
        <div>
          <Statistic
            horizontal
            size="mini"
            className="exercise-container__fields-quantity"
            color="brown"
          >
            <Statistic.Label> Total: </Statistic.Label>
            <Statistic.Value>
              <span>{content.length}</span>
            </Statistic.Value>
          </Statistic>
          <Popup
            basic
            content={<TutorialContent />}
            wide="very"
            on="click"
            trigger={
              <Button basic color="purple" icon disabled={!content.length}>
                Quick tutorial
                <Icon name="info" />
              </Button>
            }
          />
          <Button
            basic
            color="red"
            icon
            labelPosition="right"
            disabled={!content.length}
            onClick={() => removeField()}
          >
            Remove last field <Icon name="minus" />
          </Button>
          <Button
            icon
            labelPosition="right"
            basic
            color="green"
            className="button-add-field"
            onClick={() => addField()}
          >
            Add field <Icon name="plus" />
          </Button>
        </div>
      </div>
      <div>
        <Segment tertiary>
          <Grid className="exercise-container__builder-container">
            {practicalContent &&
              practicalContent[id] &&
              practicalContent[id].content.map((obj) => {
                return (
                  obj && (
                    <Grid.Row key={obj.id}>
                      <Grid.Column widescreen={4} largeScreen={4}>
                        <Container className="d-flex flex-column" fluid>
                          <label className="exercise-container__textarea-label">
                            Word
                          </label>
                          <TextArea
                            className="exercise-container__text-area"
                            value={
                              !!content[obj.id]
                                ? content[obj.id].answer || ""
                                : ""
                            }
                            onChange={(e, { value }) =>
                              handleContent(value, "answer", obj.id)
                            }
                          />
                        </Container>
                      </Grid.Column>
                      <Grid.Column widescreen={12} largeScreen={12}>
                        <Container className="d-flex flex-column" fluid>
                          <label className="exercise-container__textarea-label">
                            Description
                          </label>
                          <TextArea
                            className="exercise-container__text-area"
                            value={
                              !!content[obj.id]
                                ? content[obj.id].sentence || ""
                                : ""
                            }
                            onChange={(e, { value }) =>
                              handleContent(value, "sentence", obj.id)
                            }
                          />
                        </Container>
                      </Grid.Column>
                    </Grid.Row>
                  )
                );
              })}
          </Grid>
        </Segment>
      </div>
    </Segment>
  );
};

export default DefenitionExercise;
