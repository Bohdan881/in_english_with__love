/* 
 The function that builds panges according to category name
*/
import React from "react";
import { Tab } from "semantic-ui-react";
import PracticeContainer from "../PracticeComponents/PracticeContainer";
import {
  AboutTheLesson,
  LessonNewVocabulary,
  LessonVideoContent,
  SpeakingPractice,
  WritingComponent,
} from "../TheoryComponents";
import { CREATE_LESSON_STAGES } from "../../../../../constants";

const buildSectionsByCategory = (category, exercises) => {
  switch (category) {
    case "vocabulary":
    case "video":
    case "phrasal verbs":
      return [
        {
          menuItem: CREATE_LESSON_STAGES.about,
          render: () => (
            <Tab.Pane>
              <AboutTheLesson sectionKey={CREATE_LESSON_STAGES.about.key} />
            </Tab.Pane>
          ),
        },

        category === "video"
          ? {
              menuItem: CREATE_LESSON_STAGES.content,
              render: () => (
                <Tab.Pane>
                  <LessonVideoContent
                    sectionKey={CREATE_LESSON_STAGES.content.key}
                  />
                </Tab.Pane>
              ),
            }
          : {
              menuItem:
                CREATE_LESSON_STAGES[
                  category === "vocabulary" ? "words" : "phrasalverbs"
                ],
              render: () => (
                <Tab.Pane>
                  <LessonNewVocabulary />
                </Tab.Pane>
              ),
            },
        {
          menuItem: CREATE_LESSON_STAGES.practice,
          render: () => (
            <Tab.Pane>
              <PracticeContainer
                exercises={exercises}
                sectionKey={CREATE_LESSON_STAGES.practice.key}
              />
            </Tab.Pane>
          ),
        },

        {
          menuItem: CREATE_LESSON_STAGES.writing,
          render: () => (
            <Tab.Pane>
              <WritingComponent />
            </Tab.Pane>
          ),
        },

        {
          menuItem: CREATE_LESSON_STAGES.speaking,
          render: () => (
            <Tab.Pane>
              <SpeakingPractice
                sectionKey={CREATE_LESSON_STAGES.speaking.key}
              />
            </Tab.Pane>
          ),
        },
      ];
    default:
      return [];
  }
};

export default buildSectionsByCategory;
