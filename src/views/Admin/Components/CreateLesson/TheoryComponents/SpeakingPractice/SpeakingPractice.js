import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Form,
  Header,
  Icon,
  Popup,
  Segment,
  TextArea,
  Transition,
} from "semantic-ui-react";
import { setLessonValues } from "../../../../../../redux/actions";
import {
  INIT_SPEAKING_CONTENT,
  MAX_SPEAKING_SENTENCES_PER_PAGE,
  SPEAKING_PRACTICE,
} from "../../../../../../constants";
import {
  REMOVE_BLOCK_CONFIRMATION,
  REMOVE_QUESTION_CONFIRMATION,
} from "../../../../../../constants/alertContent";
import { confirmationAlert } from "../../../../../../utils/fireAlert";

const HelpPopup = () => (
  <p>Please note that you're able to use HTML tags to edit your text.</p>
);

const SpeakingPractice = () => {
  // redux ---------------------------------
  const { newLessonState } = useSelector((db) => db);
  const { lessonTheoreticalContent } = newLessonState;

  const dispatch = useDispatch();
  // ----------------------------------------

  // states ---------------------------------
  const [questionBlockView, setQuestionBlockView] = useState({
    allExercisesView: true,
  });

  // const [sentencesNumber, setSentencesNumber] = useState(0);

  const theoreticalContent = lessonTheoreticalContent[SPEAKING_PRACTICE];
  // ----------------------------------------

  // handlers ---------------------------------
  const numberOfcontentBlocks = theoreticalContent
    ? theoreticalContent.length
    : 0;

  const updateContent = (content) => {
    let sentenceNumber = 0;
    let sentencePage = 0;

    const updatedContent =
      content && content.length
        ? content.map((fieldContent) => {
            const { contentBlocks } = fieldContent;
            if (contentBlocks && contentBlocks.length) {
              contentBlocks.map((contentBlock) => {
                // add a sentence
                sentenceNumber += 1;
                // calculate page number based on sentence number

                contentBlock.pageNumber = Math.ceil(
                  sentenceNumber / MAX_SPEAKING_SENTENCES_PER_PAGE
                );

                sentencePage = Math.max(sentencePage, contentBlock.pageNumber);
                return fieldContent;
              });
            }
            fieldContent.maxPage = sentencePage;
            return fieldContent;
          })
        : [];

    dispatch(
      setLessonValues({
        lessonTheoreticalContent: {
          ...lessonTheoreticalContent,
          [SPEAKING_PRACTICE]: updatedContent,
        },
      })
    );
  };

  const addField = (exerciseId) => {
    const contentWithNewField =
      theoreticalContent && theoreticalContent.length
        ? theoreticalContent.concat({
            ...INIT_SPEAKING_CONTENT,
            id: exerciseId,
          })
        : [
            {
              ...INIT_SPEAKING_CONTENT,
              id: exerciseId,
            },
          ];

    updateContent(contentWithNewField);
  };

  const addSentenceToField = (blockId, contentBlocks = []) => {
    const contentWithNewSentence = theoreticalContent.map((speakingObj) =>
      speakingObj.id !== blockId
        ? speakingObj
        : {
            ...speakingObj,
            contentBlocks: contentBlocks.length
              ? contentBlocks.concat({
                  blockContent: "",
                  blockId: contentBlocks.length + 1,
                })
              : [
                  {
                    blockContent: "",
                    blockId: 1,
                  },
                ],
          }
    );

    updateContent(contentWithNewSentence);
  };

  const removeField = (fieldId) => {
    const contentWithRemovedField = theoreticalContent.filter((obj) => {
      const filteredObjects = obj.id !== fieldId;

      // decrement values which are higher than removed obj.id
      obj.id = obj.id > fieldId ? obj.id - 1 : obj.id;

      return filteredObjects;
    });

    updateContent(contentWithRemovedField);
  };

  const removeSentenceFromField = (fieldId, blockId, contentBlocks) => {
    const contentWithRemovedSentence = theoreticalContent.map((speakingObj) =>
      speakingObj.id !== fieldId
        ? speakingObj
        : {
            ...speakingObj,
            contentBlocks: contentBlocks.filter((obj) => {
              const filteredObjects = obj.blockId !== blockId;

              // decrement values which are higher than removed obj.id
              obj.blockId =
                obj.blockId > blockId ? obj.blockId - 1 : obj.blockId;

              return filteredObjects;
            }),
          }
    );

    updateContent(contentWithRemovedSentence);
  };

  const handleTextArea = (id, blockId, value, contentBlocks) =>
    dispatch(
      setLessonValues({
        lessonTheoreticalContent: {
          ...lessonTheoreticalContent,
          [SPEAKING_PRACTICE]: theoreticalContent.map((newBlockObj) =>
            newBlockObj.id !== id
              ? newBlockObj
              : {
                  ...newBlockObj,
                  contentBlocks: contentBlocks.map((questionBlockObj) =>
                    questionBlockObj.blockId !== blockId
                      ? questionBlockObj
                      : {
                          ...questionBlockObj,
                          blockContent: value,
                        }
                  ),
                }
          ),
        },
      })
    );

  // ----------------------------------------

  return (
    <div>
      <Segment className="new-field__header-container" secondary>
        <Header className="new-field__header" as="h2">
          {`You created ${numberOfcontentBlocks} ${
            numberOfcontentBlocks === 1 ? "block" : "blocks"
          }`}
        </Header>
        <div>
          <Popup
            inverted
            position="top center"
            content="Add a field"
            trigger={
              <Button
                basic
                color="green"
                icon="plus"
                onClick={() => {
                  const exerciseId = theoreticalContent
                    ? theoreticalContent.length + 1
                    : 1;

                  setQuestionBlockView({
                    ...questionBlockView,
                    [exerciseId]: true,
                  });
                  addField(exerciseId);
                }}
              />
            }
          />
        </div>
      </Segment>
      {theoreticalContent &&
        Array.isArray(theoreticalContent) &&
        theoreticalContent.length > 0 &&
        !theoreticalContent[0].totalPages &&
        theoreticalContent
          .slice(0)
          .reverse()
          .map(({ id, contentBlocks }) => {
            /* We're getting undefined when component
               just mounted, which means an exercise is visible by default
             */
            const isVisible =
              questionBlockView[id] === undefined ||
              questionBlockView[id] === true;

            return (
              <Segment key={id} secondary>
                <div className="new-field-builder__container">
                  <div className="new-field__header-container">
                    <Header className="new-field__header" as="h3">
                      {`Block ${id}`}
                    </Header>
                    <div className="new-field__header-toolbar">
                      <Popup
                        inverted
                        position="top center"
                        content="Add a sentence"
                        trigger={
                          <Button
                            icon="plus"
                            className="button-add"
                            disabled={!isVisible}
                            onClick={() =>
                              addSentenceToField(id, contentBlocks)
                            }
                          />
                        }
                      />

                      <Popup
                        inverted
                        position="top center"
                        content={isVisible ? "Hide content" : "Show content"}
                        trigger={
                          <Button
                            color="brown"
                            className="new-field-builder__button--toggle-view"
                            icon={isVisible ? "eye slash" : "eye"}
                            onClick={() =>
                              setQuestionBlockView({
                                ...questionBlockView,
                                [id]:
                                  questionBlockView[id] === undefined
                                    ? false
                                    : !questionBlockView[id],
                              })
                            }
                          />
                        }
                      />
                      <Popup
                        basic
                        inverted
                        content={<HelpPopup />}
                        trigger={<Button icon="circle question" />}
                      />
                      <Popup
                        basic
                        inverted
                        content="Remove this field"
                        trigger={
                          <Button
                            color="red"
                            icon="remove"
                            className="button-remove"
                            onClick={() => {
                              confirmationAlert(REMOVE_BLOCK_CONFIRMATION).then(
                                (res) => {
                                  if (!!res && res.value) {
                                    removeField(id);

                                    // set view to undefined because we removed an object from an array
                                    const newQuestionBlockView = {
                                      ...questionBlockView,
                                    };
                                    delete newQuestionBlockView[id];
                                    setQuestionBlockView({
                                      ...newQuestionBlockView,
                                    });
                                  }
                                }
                              );
                            }}
                          />
                        }
                      />
                    </div>
                  </div>
                  <Transition
                    key={id}
                    visible={questionBlockView[id] === false ? false : true}
                    animation="fade"
                    duration={500}
                  >
                    <div>
                      {!contentBlocks || contentBlocks.length === 0
                        ? "No Content Found..."
                        : contentBlocks.map(({ blockId, blockContent }) => (
                            <div key={blockId} className="d-flex flex-row mb-1">
                              <TextArea
                                className="new-field-builder__text-area--one-line w-100"
                                value={blockContent}
                                onChange={(e, { value }) =>
                                  handleTextArea(
                                    id,
                                    blockId,
                                    value,
                                    contentBlocks
                                  )
                                }
                              />
                              <Form.Button
                                className="new-word-example--button-remove"
                                color="red"
                                onClick={() =>
                                  confirmationAlert(
                                    REMOVE_QUESTION_CONFIRMATION
                                  ).then((res) => {
                                    if (res && res.value) {
                                      removeSentenceFromField(
                                        id,
                                        blockId,
                                        contentBlocks
                                      );
                                    }
                                  })
                                }
                              >
                                <Icon
                                  size="large"
                                  className="field--icon-remove"
                                  name="remove"
                                />
                              </Form.Button>
                            </div>
                          ))}
                    </div>
                  </Transition>
                </div>
              </Segment>
            );
          })}
    </div>
  );
};

export default SpeakingPractice;
