import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Form,
  // Icon,
  Header,
  Segment,
  Transition,
  Embed,
  Popup,
} from "semantic-ui-react";
import { setLessonValues } from "../../../../../../redux/actions";
import CustomEditor from "../../../Editor/CustomEditor";
import { transformToOptions } from "../../../../../../utils";
import { CONTENT_VIDEO_SOURCES } from "../../../../../../constants";

// style
import "./style.scss";

const LessonVideoContent = ({ sectionKey }) => {
  // redux ---------------------------------
  const { contentVideoValues = {} } = useSelector((db) => db.newLessonState);

  const { id, source, link } = contentVideoValues;

  const dispatch = useDispatch();
  // ----------------------------------------

  // states ---------------------------------
  const [viewState, setViewState] = useState({
    videoContent: true,
    editorContent: true,
  });

  // const [editMode, setEditMode] = useState(false);
  // ----------------------------------------

  return (
    <div>
      <Segment className="new-field__header-container" secondary>
        <Header className="new-field__header" as="h2">
          Video Content
        </Header>
      </Segment>
      <Segment secondary>
        <div className="new-field-builder__container">
          <div className="new-field__header-container">
            <Header className="new-field__header" as="h3">
              Video Settings
            </Header>
            <div className="new-field__header-toolbar">
              <Popup
                inverted
                position="top center"
                content={
                  viewState.videoContent ? "Hide content" : "Show content"
                }
                trigger={
                  <Button
                    color="brown"
                    className="new-field-builder__button--toggle-view"
                    icon={viewState.videoContent ? "eye slash" : "eye"}
                    onClick={() =>
                      setViewState({
                        ...viewState,
                        videoContent: !viewState.videoContent,
                      })
                    }
                  />
                }
              />
            </div>
          </div>
        </div>
        <Transition
          visible={viewState.videoContent}
          animation="fade"
          duration={500}
        >
          <div>
            <Form className="video-content-form">
              <Form.Field>
                <Form.Dropdown
                  className="capitalize"
                  label="Source"
                  selection
                  search
                  value={source ? source : CONTENT_VIDEO_SOURCES[0]}
                  options={transformToOptions(CONTENT_VIDEO_SOURCES)}
                  onChange={(e, { value }) =>
                    dispatch(
                      setLessonValues({
                        contentVideoValues: {
                          ...contentVideoValues,
                          source: value,
                        },
                      })
                    )
                  }
                  placeholder="Select Source"
                />
              </Form.Field>
              <Form.Field>
                <Form.Input
                  label="Video Link"
                  className="focus-outline-none"
                  value={link || ""}
                  onChange={(e, { value = "" }) => {
                    let videoId = value.split("v=")[1];

                    if (videoId) {
                      const ampersandPosition = videoId.indexOf("&");

                      if (ampersandPosition !== -1) {
                        videoId = videoId
                          .substring(0, ampersandPosition)
                          .trim();
                      }
                    }

                    dispatch(
                      setLessonValues({
                        contentVideoValues: {
                          ...contentVideoValues,
                          id: videoId,
                          link: value,
                        },
                      })
                    );
                  }}
                />
              </Form.Field>
            </Form>

            <div className="mt-1">
              <Embed
                id={id || ""}
                autoplay
                brandedUI
                defaultActive
                source={source || CONTENT_VIDEO_SOURCES[0]}
              />
            </div>
          </div>
        </Transition>
      </Segment>
      <Segment className="container-exercises-builders video-content__transcript_container">
        <div className="new-field-builder__container">
          <div className="new-field__header-container">
            <Header className="new-field__header" as="h3">
              Transcript
            </Header>
            <div className="new-field__header-toolbar">
              <Popup
                inverted
                position="top center"
                content={
                  viewState.editorContent ? "Hide content" : "Show content"
                }
                trigger={
                  <Button
                    color="brown"
                    className="new-field-builder__button--toggle-view"
                    icon={viewState.editorContent ? "eye slash" : "eye"}
                    onClick={() =>
                      setViewState({
                        ...viewState,
                        editorContent: !viewState.editorContent,
                      })
                    }
                  />
                }
              />
            </div>
          </div>
        </div>
        <Transition
          visible={viewState.editorContent}
          animation="fade"
          duration={500}
        >
          <div className="editor-container">
            <CustomEditor sectionKey={sectionKey} />
          </div>
        </Transition>
      </Segment>
    </div>
  );
};

export default LessonVideoContent;
