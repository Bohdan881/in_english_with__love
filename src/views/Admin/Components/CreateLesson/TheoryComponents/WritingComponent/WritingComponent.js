import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Table,
  Button,
  TextArea,
  Icon,
  Header,
  Input,
  Grid,
  Popup,
} from "semantic-ui-react";
import _ from "lodash";
import {
  WRITING_PRACTICE,
  WRITING_TABLE_HEADER_CELLS,
  WRITING_INIT_VALUES,
} from "../../../../../../constants";
import { setLessonValues } from "../../../../../../redux/actions";

// style
import "./style.scss";

const WritingComponent = () => {
  // redux ---------------------------------
  const { newLessonState } = useSelector((db) => db);

  const { lessonTheoreticalContent } = newLessonState;

  const theoreticalContent = lessonTheoreticalContent[WRITING_PRACTICE];

  const productionContent = theoreticalContent.production;

  const dispatch = useDispatch();
  // ----------------------------------------

  // states ---------------------------------
  const [editExamplesMode, setEditExamplesMode] = useState({
    simple: false,
    compound: false,
    complex: false,
    production: false,
  });
  const [bulletPointsValues, setBulletPointsValues] = useState(
    productionContent ? productionContent.bulletPoints : []
  );

  // ----------------------------------------

  // handlers -------------------------------

  const handleTextArea = (keyName, value) => {
    dispatch(
      setLessonValues({
        content: {
          ...lessonTheoreticalContent,
          [WRITING_PRACTICE]: {
            ...theoreticalContent,
            examples: {
              ...theoreticalContent.examples,
              [keyName]: value,
            },
          },
        },
      })
    );
  };

  const handleRemoveBulletPoint = () => {
    const newBulletPointsContainer = bulletPointsValues.slice(
      0,
      bulletPointsValues.length - 1
    );

    // change state
    setBulletPointsValues(newBulletPointsContainer);

    // persist in redux
    dispatch(
      setLessonValues({
        content: {
          ...lessonTheoreticalContent,
          [WRITING_PRACTICE]: {
            ...theoreticalContent,
            production: {
              ...productionContent,
              bulletPoints: newBulletPointsContainer,
            },
          },
        },
      })
    );
  };

  const handleAddBulletPoint = () => {
    const newBulletPointsContainer = bulletPointsValues.concat("");

    // change state
    setBulletPointsValues(newBulletPointsContainer);

    // persist in redux
    dispatch(
      setLessonValues({
        content: {
          ...lessonTheoreticalContent,
          [WRITING_PRACTICE]: {
            ...theoreticalContent,
            production: {
              ...productionContent,
              bulletPoints: newBulletPointsContainer,
            },
          },
        },
      })
    );
  };

  const handleBulletPoints = (value, key) => {
    const newBulletPointsContainer = bulletPointsValues.map(
      (bulletPoint, index) => (index === key ? value : bulletPoint)
    );

    // set state
    setBulletPointsValues(newBulletPointsContainer);

    // persist values by using debounce util function
    onPersistBulletPoints(newBulletPointsContainer);
  };

  const onPersistBulletPoints = _.debounce((newBulletPointsContainer) => {
    dispatch(
      setLessonValues({
        content: {
          ...lessonTheoreticalContent,
          [WRITING_PRACTICE]: {
            ...theoreticalContent,
            production: {
              ...productionContent,
              bulletPoints: newBulletPointsContainer,
            },
          },
        },
      })
    );
  }, 300);

  const handleDescriptionValue = (value) => {
    dispatch(
      setLessonValues({
        content: {
          ...lessonTheoreticalContent,
          [WRITING_PRACTICE]: {
            ...theoreticalContent,
            production: {
              ...productionContent,
              description: value,
            },
          },
        },
      })
    );
  };

  // ----------------------------------------

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <Header as="h4" className="writing-task__sub-header uppercase">
            Production
          </Header>

          <div>
            <Header as="h4"> Description </Header>
            <Input
              className="w-100"
              value={
                (productionContent && productionContent.description) ||
                WRITING_INIT_VALUES.production.description
              }
              onChange={(_, { value }) => handleDescriptionValue(value)}
            />
          </div>
          <div className="d-flex flex-row mt-1">
            <Header as="h4" className="writing-task__bullet-point-header">
              Bullet Points
            </Header>
            <div className="new-field__header-toolbar">
              <Popup
                inverted
                position="top center"
                content="Add a bullet point"
                trigger={
                  <Button
                    icon="plus"
                    className="button-add"
                    onClick={() => handleAddBulletPoint()}
                  />
                }
              />

              <Popup
                inverted
                position="top center"
                content="Remove the last bullet point"
                trigger={
                  <Button
                    disabled={
                      !productionContent ||
                      !productionContent.bulletPoints ||
                      productionContent.bulletPoints.length === 0
                    }
                    icon="minus"
                    className="button-remove"
                    onClick={() => handleRemoveBulletPoint()}
                  />
                }
              />
            </div>
          </div>
          {bulletPointsValues.length &&
            bulletPointsValues.map((bulletPoint, key) => (
              <Input
                key={key}
                className="w-100 pb-1"
                value={bulletPoint}
                onChange={(e, { value }) => handleBulletPoints(value, key)}
              />
            ))}
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Header as="h4" className="writing-task__sub-header uppercase">
            USE A VARIETY OF SENTENCE STYLES
          </Header>
          <p>
            Avoid using simple sentences. Instead, try to use a variety of
            sentence styles and lengths:
          </p>
          <Table celled>
            <Table.Header>
              <Table.Row>
                {WRITING_TABLE_HEADER_CELLS.map((header) => (
                  <Table.HeaderCell key={header}>{header}</Table.HeaderCell>
                ))}
              </Table.Row>
            </Table.Header>

            <Table.Body>
              <Table.Row>
                <Table.Cell singleLine>
                  <b> Simple </b>
                </Table.Cell>
                <Table.Cell>
                  {!editExamplesMode.simple ? (
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          (theoreticalContent.examples &&
                            theoreticalContent.examples.simple) ||
                          WRITING_INIT_VALUES.examples.simple,
                      }}
                    />
                  ) : (
                    <TextArea
                      className="writing-task__text-area"
                      value={
                        (theoreticalContent.examples &&
                          theoreticalContent.examples.simple) ||
                        WRITING_INIT_VALUES.examples.simple
                      }
                      onChange={(e, { value }) =>
                        handleTextArea("simple", value)
                      }
                    />
                  )}
                  <Button
                    className="writing-task__edit-button"
                    onClick={() =>
                      setEditExamplesMode({
                        ...editExamplesMode,
                        simple: !editExamplesMode.simple,
                      })
                    }
                  >
                    <Icon
                      name={editExamplesMode.simple ? "check" : "edit"}
                      color={editExamplesMode.simple ? "green" : "yellow"}
                    />
                  </Button>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell singleLine>
                  <b> Compound </b>
                </Table.Cell>
                <Table.Cell>
                  {!editExamplesMode.compound ? (
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          (theoreticalContent.examples &&
                            theoreticalContent.examples.compound) ||
                          WRITING_INIT_VALUES.examples.compound,
                      }}
                    />
                  ) : (
                    <TextArea
                      className="writing-task__text-area"
                      value={
                        (theoreticalContent.examples &&
                          theoreticalContent.examples.compound) ||
                        WRITING_INIT_VALUES.examples.compound
                      }
                      onChange={(e, { value }) =>
                        handleTextArea("compound", value)
                      }
                    />
                  )}
                  <Button
                    className="writing-task__edit-button"
                    onClick={() =>
                      setEditExamplesMode({
                        ...editExamplesMode,
                        compound: !editExamplesMode.compound,
                      })
                    }
                  >
                    <Icon
                      name={editExamplesMode.compound ? "check" : "edit"}
                      color={editExamplesMode.compound ? "green" : "yellow"}
                    />
                  </Button>
                </Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell singleLine width="4">
                  <b>Complex</b>
                </Table.Cell>

                <Table.Cell>
                  {!editExamplesMode.complex ? (
                    <span
                      dangerouslySetInnerHTML={{
                        __html:
                          (theoreticalContent.examples &&
                            theoreticalContent.examples.complex) ||
                          WRITING_INIT_VALUES.examples.complex,
                      }}
                    />
                  ) : (
                    <TextArea
                      className="writing-task__text-area"
                      value={
                        (theoreticalContent.examples &&
                          theoreticalContent.examples.complex) ||
                        WRITING_INIT_VALUES.examples.complex
                      }
                      onChange={(e, { value }) =>
                        handleTextArea("complex", value)
                      }
                    />
                  )}
                  <Button
                    className="writing-task__edit-button"
                    onClick={() =>
                      setEditExamplesMode({
                        ...editExamplesMode,
                        complex: !editExamplesMode.complex,
                      })
                    }
                  >
                    <Icon
                      name={editExamplesMode.complex ? "check" : "edit"}
                      color={editExamplesMode.complex ? "green" : "yellow"}
                    />
                  </Button>
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default WritingComponent;
