import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { compose } from "recompose";
import {
  Segment,
  Header,
  Popup,
  Button,
  Transition,
  Image,
  Modal,
  Icon,
} from "semantic-ui-react";
import { withFirebase } from "../../../../../../firebase";
import { withAuthorization } from "../../../../../../auth";
import { setLessonValues } from "../../../../../../redux/actions";
import CustomEditor from "../../../Editor/CustomEditor";
import {
  ABOUT_IMAGE_OPTIONS,
  LESSONS_BUCKET_NAME,
} from "../../../../../../constants";
import * as ROLES from "../../../../../../constants/roles";
import { optimizeImage } from "../../../../../../utils";
import {
  confirmationAlert,
  infoAutoCloseAlert,
} from "../../../../../../utils/fireAlert";
import {
  ERROR_ALERT,
  IMAGE_LESSON_ADD_ALERT,
  IMAGE_LESSON_REMOVE_ALERT,
  REMOVE_IMAGE_CONFIRMATION,
} from "../../../../../../constants/alertContent";

// styles
import "./style.scss";

// assets
import imagePlaceHolder from "../../../../../../assets/images/imagePlaceholder.png";

const AboutTheLesson = ({ sectionKey, firebase }) => {
  // redux ---------------------------------
  const { assets = {} } = useSelector((db) => db.newLessonState);

  const dispatch = useDispatch();
  // ----------------------------------------

  // states & refs --------------------------

  const fileInputElementRef = useRef();

  const [viewState, setViewState] = useState({
    imageContent: true,
    editorContent: true,
  });

  const [iconOptions, setIconOptions] = useState({
    iconFile: null,
    iconName: null,
  });

  const [modalState, setModalState] = useState(false);

  // ----------------------------------------

  // handlers -------------------------------

  const IMAGE_ID = "about-optimized-image";

  const handleChangeInput = async (files) => {
    if (files && files[0]) {
      const optimizedImage = await optimizeImage(files, {
        ...ABOUT_IMAGE_OPTIONS,
        outputAddress: `#${IMAGE_ID}`,
      });

      setIconOptions({
        ...iconOptions,
        iconFile: optimizedImage,
        iconName: files[0].name,
      });
    }
  };

  const uploadImageToDb = () => {
    const { iconFile, iconName } = iconOptions;

    const createdTime = new Date().getTime();
    const formattedSectionKey = sectionKey.replaceAll(" ", "");

    const referencedValue = `${LESSONS_BUCKET_NAME}/${createdTime}-${formattedSectionKey}-${iconName}`;

    const storageRef = firebase.storage.ref(referencedValue);

    storageRef
      .put(iconFile)
      .then(() => {
        firebase.storage
          .ref()
          .child(referencedValue)
          .getDownloadURL()
          .then((url) => {
            dispatch(
              setLessonValues({
                assets: {
                  ...assets,
                  [sectionKey]: url,
                },
              })
            );
          })
          .then(() => infoAutoCloseAlert(IMAGE_LESSON_ADD_ALERT))
          .catch((error) =>
            infoAutoCloseAlert({
              ...ERROR_ALERT,
              text: error.message,
            })
          );
      })
      .catch((error) => {
        infoAutoCloseAlert({
          ...ERROR_ALERT,
          text: error.message,
        });
      });
  };

  const removeImage = () => {
    firebase.storage
      .refFromURL(assets[sectionKey])
      .delete()
      .then(() => infoAutoCloseAlert(IMAGE_LESSON_REMOVE_ALERT))
      .catch((error) =>
        infoAutoCloseAlert({ ...ERROR_ALERT, text: error.message })
      );

    setIconOptions({
      iconFile: null,
      iconName: null,
    });

    setModalState(false);

    const clonedAssets = Object.assign({}, assets);

    delete clonedAssets[sectionKey];

    dispatch(
      setLessonValues({
        assets: clonedAssets,
      })
    );

    if (fileInputElementRef.current) {
      fileInputElementRef.current.value = "";
    }
  };

  // disable upload if an image exists
  const isUploadDisabled =
    assets && assets[sectionKey] && assets[sectionKey] !== "";

  // ----------------------------------------

  return (
    <div>
      <Segment className="new-field__header-container" secondary>
        <Header className="new-field__header" as="h2">
          About Section
        </Header>
      </Segment>
      <Segment secondary>
        <div className="new-field-builder__container">
          <div className="new-field__header-container">
            <Header className="new-field__header" as="h3">
              Image Settings
              <span>
                <Popup
                  inverted
                  className="icon-settings__popup"
                  content="Recommended image size measurements: 300px wide by 250px tall."
                  trigger={
                    <Icon
                      className="icon-trigger"
                      size="small"
                      name="question circle"
                    />
                  }
                />
              </span>
            </Header>

            <div className="new-field__header-toolbar">
              <Popup
                inverted
                position="top center"
                content={
                  viewState.imageContent ? "Hide content" : "Show content"
                }
                trigger={
                  <Button
                    color="brown"
                    className="new-field-builder__button--toggle-view"
                    icon={viewState.imageContent ? "eye slash" : "eye"}
                    onClick={() =>
                      setViewState({
                        ...viewState,
                        imageContent: !viewState.imageContent,
                      })
                    }
                  />
                }
              />
            </div>
          </div>
        </div>
        <Transition
          visible={viewState.imageContent}
          animation="fade"
          duration={500}
        >
          <div>
            <div className="about-button-group__container">
              <input
                accept="image/*"
                ref={fileInputElementRef}
                type="file"
                hidden
                onChange={(e) => {
                  handleChangeInput(e.target.files);
                  setModalState(true);
                }}
              />
              <Button.Group className="create-lesson__image-button-group">
                {!isUploadDisabled ? (
                  <Button
                    content="Upload an image"
                    labelPosition="left"
                    icon="file"
                    primary
                    onClick={() => fileInputElementRef.current.click()}
                  />
                ) : (
                  <Button
                    color="red"
                    type="submit"
                    onClick={() =>
                      confirmationAlert(REMOVE_IMAGE_CONFIRMATION).then(
                        ({ isConfirmed }) => isConfirmed && removeImage()
                      )
                    }
                  >
                    Remove an image
                  </Button>
                )}
              </Button.Group>
            </div>
            <div>
              <Image
                src={assets[sectionKey] ? assets[sectionKey] : imagePlaceHolder}
                className="about__image"
                size="medium"
                width="100%"
                height="100%"
                verticalAlign="middle"
              />
            </div>
          </div>
        </Transition>
      </Segment>

      <Segment className="container-exercises-builders video-content__transcript_container">
        <div className="new-field-builder__container">
          <div className="new-field__header-container">
            <Header className="new-field__header" as="h3">
              Description
            </Header>
            <div className="new-field__header-toolbar">
              <Popup
                inverted
                position="top center"
                content={
                  viewState.editorContent ? "Hide content" : "Show content"
                }
                trigger={
                  <Button
                    color="brown"
                    className="new-field-builder__button--toggle-view"
                    icon={viewState.editorContent ? "eye slash" : "eye"}
                    onClick={() =>
                      setViewState({
                        ...viewState,
                        editorContent: !viewState.editorContent,
                      })
                    }
                  />
                }
              />
            </div>
          </div>
        </div>
        <Transition
          visible={viewState.editorContent}
          animation="fade"
          duration={500}
        >
          <div className="editor-container">
            <CustomEditor sectionKey={sectionKey} />
          </div>
        </Transition>
      </Segment>

      {modalState && (
        <Modal open={modalState}>
          <Modal.Header>Upload image</Modal.Header>
          <Modal.Content image>
            <div className="create-lesson__upload-image-container mr-1">
              <Image
                id={IMAGE_ID}
                src={imagePlaceHolder}
                width="100%"
                height="100%"
              />
            </div>
            <Modal.Description>
              <p>Would you like to upload this image?</p>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button
              onClick={() => {
                setIconOptions({
                  iconFile: null,
                  iconName: null,
                });
                setModalState(false);

                if (fileInputElementRef.current) {
                  fileInputElementRef.current.value = "";
                }
              }}
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                uploadImageToDb();
                setModalState(false);
              }}
              positive
            >
              Ok
            </Button>
          </Modal.Actions>
        </Modal>
      )}
    </div>
  );
};

const condition = (authUser) => authUser && !!authUser.roles[ROLES.ADMIN];

export default compose(
  withAuthorization(condition),
  withFirebase
)(AboutTheLesson);
