import React, { useState } from "react";
import { Popup, Button, Segment, Header, Transition } from "semantic-ui-react";
import LessonView from "../../../../../LessonView/LessonViewContainer";
import { PREVIEW_MODE } from "../../../../../../constants";
import { convertMillisecondsToDate } from "../../../../../../utils";
import { LessonCard } from "../../../../../Shared";

// styles
import "./style.scss";

// assets
import defaultPicture from "../../../../../../assets/images/default.png";

const PreviewComponentBuilder = ({
  title,
  viewState,
  toggleView,
  component,
}) => (
  <Segment secondary>
    <div className="new-field-builder__container">
      <div className="new-field__header-container">
        <Header className="new-field__header" as="h2">
          {title}
        </Header>
        <div className="new-field__header-toolbar">
          <Popup
            inverted
            position="top center"
            content={viewState ? "Hide content." : "Show content."}
            trigger={
              <Button
                color="brown"
                className="new-field-builder__button--toggle-view"
                icon={viewState ? "eye slash" : "eye"}
                onClick={() => toggleView(!viewState)}
              />
            }
          />
          <Popup
            basic
            inverted
            content="Please note you're able only preview your current progress."
            trigger={<Button icon="circle question" />}
          />
        </div>
      </div>
      <Transition
        visible={viewState}
        animation="fade"
        duration={500}
        transitionOnMount={true}
        unmountOnHide={true}
      >
        {component}
      </Transition>
    </div>
  </Segment>
);

const LessonPreview = ({
  sectionKey,
  iconSrc,
  title,
  focus = "",
  category,
}) => {
  const [cardView, toggleCardView] = useState(true);
  const [lessonView, toggleLessonView] = useState(true);
  return (
    <div>
      <PreviewComponentBuilder
        title="Card View"
        viewState={cardView}
        toggleView={toggleCardView}
        component={
          <div className="card-preview-container">
            <LessonCard
              img={iconSrc !== "" ? iconSrc : defaultPicture}
              title={title}
              focus={focus}
              date={convertMillisecondsToDate(new Date().getTime())}
              isPreview={true}
            />
          </div>
        }
      />
      <PreviewComponentBuilder
        title="Lesson View"
        viewState={lessonView}
        toggleView={toggleLessonView}
        component={
          <LessonView
            category={category}
            mode={PREVIEW_MODE}
            sectionKey={sectionKey}
          />
        }
      />
    </div>
  );
};

export default LessonPreview;
