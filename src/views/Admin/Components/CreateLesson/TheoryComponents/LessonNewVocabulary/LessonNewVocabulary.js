import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Segment,
  Header,
  Popup,
  Button,
  TextArea,
  Transition,
  Form,
  Icon,
} from "semantic-ui-react";
import { setLessonValues } from "../../../../../../redux/actions";
import {
  CREATE_LESSON_STAGES,
  INIT_NEW_WORDS_CONTENT,
} from "../../../../../../constants";
import { confirmationAlert } from "../../../../../../utils/fireAlert";
import {
  REMOVE_WORD_CONFIRMATION,
  REMOVE_EXAMPLE_CONFIRMATION,
  REMOVE_STRUCTURE_CONFIRMATION,
} from "../../../../../../constants/alertContent";

// style
import "./style.scss";

const HelpPopup = () => (
  <>
    <p>
      Please note that you're able to use HTML tags to edit your text in
      Description, Examples and Structures fields
    </p>
    <p>
      If you WANT a specific word in bold, please wrap the word in a structure
      written below:
      <br />
      <br />
      <span>{`<b> Some word </b>`} </span>
    </p>
    <p>
      If you DON'T WANT a specific word in bold, but it's in bold for some
      reason, please wrap the word in a structure written below:
      <br />
      <br />
      <span>{`<span className="beat-bold"> Some word </span>`} </span>
    </p>
  </>
);

const LessonNewVocabulary = () => {
  // redux ---------------------------------
  const { newLessonState } = useSelector((db) => db);
  const { lessonTheoreticalContent, category = "" } = newLessonState;

  const dispatch = useDispatch();
  // ----------------------------------------

  // states ---------------------------------

  const [wordView, setWordView] = useState({ allExercisesView: true });

  // ----------------------------------------

  // constants ------------------------------

  // const currentKey = category.toLowerCase().includes("vocabulary")
  //   ? CREATE_LESSON_STAGES.newWords.key
  //   : CREATE_LESSON_STAGES.phrasalverbs.key;
  const currentKey = CREATE_LESSON_STAGES.vocabulary.key;

  // ----------------------------------------

  // handlers -------------------------------

  const setHeaderOrDescription = (id, value, type) =>
    dispatch(
      setLessonValues({
        lessonTheoreticalContent: {
          ...lessonTheoreticalContent,
          [currentKey]: lessonTheoreticalContent[currentKey].map((newWordObj) =>
            newWordObj.id !== id ? newWordObj : { ...newWordObj, [type]: value }
          ),
        },
      })
    );

  const setStructureValues = (
    id,
    structures = [],
    structureId,
    value,
    editedKey
  ) =>
    dispatch(
      setLessonValues({
        lessonTheoreticalContent: {
          ...lessonTheoreticalContent,
          [currentKey]: lessonTheoreticalContent[currentKey].map((newWordObj) =>
            newWordObj.id !== id
              ? newWordObj
              : {
                  ...newWordObj,
                  structures: structures.map((structureObj) =>
                    structureObj.structureId !== structureId
                      ? structureObj
                      : {
                          ...structureObj,
                          [editedKey]: value,
                        }
                  ),
                }
          ),
        },
      })
    );

  const numberOfWords = !!lessonTheoreticalContent[currentKey]
    ? lessonTheoreticalContent[currentKey].length
    : 0;

  const titleContent = category.toLowerCase().includes("vocabulary")
    ? "Word"
    : "Phrasal verb";

  // ----------------------------------------

  return (
    <>
      <Segment className="new-field__header-container" secondary>
        <Header className="new-field__header" as="h2">
          {`You created ${numberOfWords}  
          ${titleContent}${numberOfWords === 1 ? "" : "s"}`}
        </Header>
        <div>
          <Popup
            inverted
            position="top center"
            content="Add a word"
            trigger={
              <Button
                basic
                color="green"
                icon="plus"
                onClick={() => {
                  const exerciseId = lessonTheoreticalContent[currentKey]
                    ? lessonTheoreticalContent[currentKey].length + 1
                    : 1;

                  dispatch(
                    setLessonValues({
                      lessonTheoreticalContent: {
                        ...lessonTheoreticalContent,
                        [currentKey]:
                          lessonTheoreticalContent[currentKey] &&
                          lessonTheoreticalContent[currentKey].length
                            ? lessonTheoreticalContent[currentKey].concat({
                                ...INIT_NEW_WORDS_CONTENT,
                                id: exerciseId,
                              })
                            : [{ ...INIT_NEW_WORDS_CONTENT, id: exerciseId }],
                      },
                    })
                  );
                }}
              />
            }
          />
        </div>
      </Segment>
      {lessonTheoreticalContent[currentKey] &&
        lessonTheoreticalContent[currentKey].length > 0 &&
        lessonTheoreticalContent[currentKey]
          .slice(0)
          .reverse()
          .map(({ id, header, description, examples, structures }) => {
            /* We're getting undefined when component
               just mounted, which means an exercise is visible by default
            */
            const isVisible =
              wordView[id] === undefined || wordView[id] === true;

            return (
              <Segment key={id} secondary>
                <div className="new-field-builder__container">
                  <div className="new-field__header-container">
                    <Header className="new-field__header" as="h3">
                      {`${id}. ${header}`}
                    </Header>
                    <div className="new-field__header-toolbar">
                      <Popup
                        inverted
                        position="top center"
                        content={isVisible ? "Hide content" : "Show content"}
                        trigger={
                          <Button
                            color="brown"
                            className="new-field-builder__button--toggle-view"
                            icon={isVisible ? "eye slash" : "eye"}
                            onClick={() =>
                              setWordView({
                                ...wordView,
                                [id]:
                                  wordView[id] === undefined
                                    ? false
                                    : !wordView[id],
                              })
                            }
                          />
                        }
                      />
                      <Popup
                        basic
                        inverted
                        content={<HelpPopup />}
                        trigger={<Button icon="circle question" />}
                      />
                      <Popup
                        basic
                        inverted
                        content="Remove it"
                        trigger={
                          <Button
                            color="red"
                            icon="remove"
                            className="button-remove"
                            onClick={() => {
                              confirmationAlert(REMOVE_WORD_CONFIRMATION).then(
                                (res) => {
                                  if (res.value) {
                                    dispatch(
                                      setLessonValues({
                                        lessonTheoreticalContent: {
                                          ...lessonTheoreticalContent,
                                          [currentKey]: lessonTheoreticalContent[
                                            currentKey
                                          ].filter((obj) => {
                                            const filteredObjects =
                                              obj.id !== id;
                                            //  decrement values which are higher than removed obj.id
                                            obj.id =
                                              obj.id > id ? obj.id - 1 : obj.id;
                                            return filteredObjects;
                                          }),
                                        },
                                      })
                                    );

                                    // set view to undefined because we removed an object from an array
                                    const newWordView = { ...wordView };
                                    delete wordView[id];
                                    setWordView({ ...newWordView });
                                  }
                                }
                              );
                            }}
                          />
                        }
                      />
                    </div>
                  </div>
                  <Transition
                    key={id}
                    visible={wordView[id] === false ? false : true}
                    animation="fade"
                    duration={500}
                  >
                    <div>
                      <div className="d-flex flex-column mb-1">
                        <Header as="h4">Name</Header>
                        <TextArea
                          className="new-field-builder__text-area--one-line w-100"
                          value={header}
                          onChange={(e, { value }) =>
                            setHeaderOrDescription(id, value, "header")
                          }
                        />
                      </div>
                      <div className="d-flex flex-column mb-1">
                        <Header as="h4">Description</Header>
                        <TextArea
                          className="new-field-builder__text-area--multiple-lines w-100"
                          value={description}
                          onChange={(e, { value }) =>
                            setHeaderOrDescription(id, value, "description")
                          }
                        />
                      </div>
                      <div className="d-flex flex-column mb-1">
                        <div className="new-vocabulary__header-container d-flex flex-row">
                          <Header as="h4">Examples</Header>
                          <div className="new-field__header-toolbar">
                            <Popup
                              inverted
                              position="top center"
                              content="Add an example"
                              trigger={
                                <Button
                                  icon="plus"
                                  className="button-add"
                                  onClick={() => {
                                    dispatch(
                                      setLessonValues({
                                        lessonTheoreticalContent: {
                                          ...lessonTheoreticalContent,
                                          [currentKey]:
                                            lessonTheoreticalContent[
                                              currentKey
                                            ] &&
                                            lessonTheoreticalContent[
                                              currentKey
                                            ].map((newWordObj) =>
                                              newWordObj.id !== id
                                                ? newWordObj
                                                : {
                                                    ...newWordObj,
                                                    examples: examples.concat({
                                                      example: "",
                                                      exampleId: examples
                                                        ? examples.length + 1
                                                        : 1,
                                                    }),
                                                  }
                                            ),
                                        },
                                      })
                                    );
                                  }}
                                />
                              }
                            />
                          </div>
                        </div>
                        {!examples || examples.length === 0
                          ? "No Examples Found"
                          : examples.map(({ exampleId, example }) => (
                              <div
                                key={exampleId}
                                className="d-flex flex-row mb-1"
                              >
                                <TextArea
                                  className="new-field-builder__text-area--one-line w-100"
                                  value={example}
                                  onChange={(e, { value }) => {
                                    dispatch(
                                      setLessonValues({
                                        lessonTheoreticalContent: {
                                          ...lessonTheoreticalContent,
                                          [currentKey]: lessonTheoreticalContent[
                                            currentKey
                                          ].map((newWordObj) =>
                                            newWordObj.id !== id
                                              ? newWordObj
                                              : {
                                                  ...newWordObj,
                                                  examples: examples.map(
                                                    (exampleObj) =>
                                                      exampleObj.exampleId !==
                                                      exampleId
                                                        ? exampleObj
                                                        : {
                                                            ...exampleObj,
                                                            example: value,
                                                          }
                                                  ),
                                                }
                                          ),
                                        },
                                      })
                                    );
                                  }}
                                />
                                <Form.Button
                                  className="new-vocabulary-example--button-remove"
                                  color="red"
                                  onClick={() =>
                                    confirmationAlert(
                                      REMOVE_EXAMPLE_CONFIRMATION
                                    ).then((res) => {
                                      if (res.value) {
                                        dispatch(
                                          setLessonValues({
                                            lessonTheoreticalContent: {
                                              ...lessonTheoreticalContent,
                                              [currentKey]:
                                                lessonTheoreticalContent[
                                                  currentKey
                                                ] &&
                                                lessonTheoreticalContent[
                                                  currentKey
                                                ].map((wordObj) => {
                                                  return wordObj.id !== id
                                                    ? wordObj
                                                    : {
                                                        ...wordObj,
                                                        examples: wordObj.examples.filter(
                                                          (obj) => {
                                                            const filteredObjects =
                                                              obj.exampleId !==
                                                              exampleId;
                                                            // decrement values which are higher than removed obj.id
                                                            obj.exampleId =
                                                              obj.exampleId >
                                                              exampleId
                                                                ? obj.exampleId -
                                                                  1
                                                                : obj.exampleId;

                                                            return filteredObjects;
                                                          }
                                                        ),
                                                      };
                                                }),
                                            },
                                          })
                                        );
                                      }
                                    })
                                  }
                                >
                                  <Icon
                                    size="large"
                                    className="new-vocabulary__remove-field-icon"
                                    name="remove"
                                  />
                                </Form.Button>
                              </div>
                            ))}
                      </div>

                      <div className="d-flex flex-column">
                        <div className="new-vocabulary__header-container d-flex flex-row">
                          <Header as="h4">Structures</Header>
                          <div className="new-field__header-toolbar">
                            <Popup
                              inverted
                              position="top center"
                              content="Add Structure"
                              trigger={
                                <Button
                                  icon="plus"
                                  className="button-add"
                                  onClick={() => {
                                    dispatch(
                                      setLessonValues({
                                        lessonTheoreticalContent: {
                                          ...lessonTheoreticalContent,
                                          [currentKey]: lessonTheoreticalContent[
                                            currentKey
                                          ].map((newWordObj) =>
                                            newWordObj.id !== id
                                              ? newWordObj
                                              : {
                                                  ...newWordObj,
                                                  structures: structures
                                                    ? structures.concat({
                                                        firstWord: header,
                                                        secondWord: "",
                                                        structureExample: "",
                                                        structureId:
                                                          structures.length + 1,
                                                      })
                                                    : [
                                                        {
                                                          firstWord: header,
                                                          secondWord: "",
                                                          structureExample: "",
                                                          structureId: 1,
                                                        },
                                                      ],
                                                }
                                          ),
                                        },
                                      })
                                    );
                                  }}
                                />
                              }
                            />
                          </div>
                        </div>
                      </div>
                      {!structures || structures.length === 0
                        ? "No Structures Found"
                        : structures.map(
                            ({
                              structureId,
                              firstWord,
                              secondWord,
                              structureExample,
                            }) => (
                              <div
                                key={structureId}
                                className="d-flex flex-row mb-1"
                              >
                                <TextArea
                                  className="new-field-builder__text-area--one-line w-50 mr-1"
                                  value={firstWord}
                                  onChange={(e, { value }) =>
                                    setStructureValues(
                                      id,
                                      structures,
                                      structureId,
                                      value,
                                      "firstWord"
                                    )
                                  }
                                />
                                <span className="new-vocabulary-structure-container__character">
                                  +
                                </span>
                                <TextArea
                                  className="new-field-builder__text-area--one-line w-50 mr-1"
                                  value={secondWord}
                                  onChange={(e, { value }) =>
                                    setStructureValues(
                                      id,
                                      structures,
                                      structureId,
                                      value,
                                      "secondWord"
                                    )
                                  }
                                />
                                <span className="new-vocabulary-structure-container__character">
                                  →
                                </span>
                                <TextArea
                                  className="new-field-builder__text-area--one-line w-100"
                                  value={structureExample}
                                  onChange={(e, { value }) =>
                                    setStructureValues(
                                      id,
                                      structures,
                                      structureId,
                                      value,
                                      "structureExample"
                                    )
                                  }
                                />
                                <Form.Button
                                  className="new-vocabulary-example--button-remove"
                                  color="red"
                                  onClick={() =>
                                    confirmationAlert(
                                      REMOVE_STRUCTURE_CONFIRMATION
                                    ).then((res) => {
                                      if (res.value) {
                                        dispatch(
                                          setLessonValues({
                                            lessonTheoreticalContent: {
                                              ...lessonTheoreticalContent,
                                              [currentKey]: lessonTheoreticalContent[
                                                currentKey
                                              ].map((wordObj) => {
                                                return wordObj.id !== id
                                                  ? wordObj
                                                  : {
                                                      ...wordObj,
                                                      structures: wordObj.structures.filter(
                                                        (obj) => {
                                                          const filteredObjects =
                                                            obj.structureId !==
                                                            structureId;
                                                          // decrement values which are higher than removed obj.id
                                                          obj.structureId =
                                                            obj.structureId >
                                                            structureId
                                                              ? obj.structureId -
                                                                1
                                                              : obj.structureId;

                                                          return filteredObjects;
                                                        }
                                                      ),
                                                    };
                                              }),
                                            },
                                          })
                                        );
                                      }
                                    })
                                  }
                                >
                                  <Icon
                                    size="large"
                                    className="new-vocabulary__remove-field-icon"
                                    name="remove"
                                  />
                                </Form.Button>
                              </div>
                            )
                          )}
                    </div>
                  </Transition>
                </div>
              </Segment>
            );
          })}
    </>
  );
};

export default LessonNewVocabulary;
