import React, { Component } from "react";
import { connect } from "react-redux";
import "firebase/storage";
import { compose } from "recompose";
import { Tab, Grid, Button, Icon } from "semantic-ui-react";
import { withAuthorization } from "../../auth";
import * as ROLES from "../../constants/roles";
import { withFirebase } from "../../firebase";
import { setLessonToInit } from "../../redux/actions";
import {
  ADMIN_TABS,
  EDIT_CREATE_LESSON_TAB_INDEX,
  LESSON_MODE,
} from "../../constants";
import {
  CONFIRMATION_REMOVE_CONTENT,
  ERROR_ALERT,
} from "../../constants/alertContent";
import CreateLessonContainer from "./Components/CreateLesson/CreateLessonContainer";
import AdminLessonList from "./Components/AdminLessonList/AdminLessonList";
import { confirmationAlert, infoAutoCloseAlert } from "../../utils/fireAlert";
import { InfoMessage } from "../Shared/Message/Message";

// style
import "./style.scss";

class AdminPage extends Component {
  state = {
    activeTabIndex: 0,
  };

  onTabChange = ({ activeIndex }) =>
    this.setState({
      activeTabIndex: activeIndex,
    });

  setEditLessonTabIndex = () =>
    this.setState({
      activeTabIndex: EDIT_CREATE_LESSON_TAB_INDEX,
      lessonMode: LESSON_MODE.EDIT,
    });

  setNewLessonToInit = () => {
    const { newLessonState } = this.props;
    const { iconPath } = newLessonState;

    confirmationAlert(CONFIRMATION_REMOVE_CONTENT).then(({ dismiss }) => {
      if (!dismiss) {
        // remove an icon from db if user uploaded one
        if (
          newLessonState.lessonMode === LESSON_MODE.CREATE &&
          iconPath !== ""
        ) {
          this.props.firebase.storage
            .refFromURL(this.props.newLessonState.iconPath)
            .delete()
            .catch((error) =>
              infoAutoCloseAlert({ ...ERROR_ALERT, text: error.code })
            );
        }

        this.props.onSetLessonToInit();
      }
    });
  };

  render() {
    const { activeTabIndex, lessonMode } = this.state;
    const { newLessonState } = this.props;

    const panes = [
      {
        menuItem: ADMIN_TABS.createLesson,
        render: () => (
          <Tab.Pane>
            <CreateLessonContainer lessonMode={lessonMode} />
          </Tab.Pane>
        ),
      },
      {
        menuItem: ADMIN_TABS.allLessons,
        render: () => (
          <Tab.Pane>
            <AdminLessonList
              setEditLessonTabIndex={this.setEditLessonTabIndex}
            />
          </Tab.Pane>
        ),
      },
    ];

    return (
      <>
        <Grid className="admin-page__container">
          <Grid.Row>
            <Grid.Column>
              <Button
                className="admin-page__reset-progress-button"
                style={{
                  display:
                    activeTabIndex === 1 || newLessonState.category === ""
                      ? "none"
                      : "",
                }}
                onClick={() => this.setNewLessonToInit()}
              >
                <Icon name="redo" />
                {newLessonState.lessonMode === LESSON_MODE.EDIT
                  ? "Cancel Editing"
                  : "Cancel Progress"}
              </Button>
              <Tab
                activeIndex={activeTabIndex}
                onTabChange={(e, { activeIndex }) =>
                  this.onTabChange(activeIndex)
                }
                panes={panes}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <div className="admin-page__message">
          <InfoMessage
            size="large"
            header="Too small screen"
            content="If you see this message, probably your device has small screen
              width. Please use a device with at least 768px width."
          />
        </div>
      </>
    );
  }
}

const mapStateToProps = ({ newLessonState }) => ({ newLessonState });

const mapDispatchToProps = (dispatch) => ({
  onSetLessonToInit: () => dispatch(setLessonToInit()),
});

const condition = (authUser) => authUser && !!authUser.roles[ROLES.ADMIN];

export default compose(
  withAuthorization(condition),
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps)
)(AdminPage);
