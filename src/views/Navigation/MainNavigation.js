import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { Link, useLocation } from "react-router-dom";
import { Menu, Image, Message, Button, Dropdown } from "semantic-ui-react";
import { withFirebase } from "../../firebase";
import * as ROUTES from "../../constants/routes";
import * as ROLES from "../../constants/roles";
import { SignOut } from "../SignForm";
import MobileMenu from "./MobileMenu";
import { setSessionValues } from "../../redux/actions";
import { USERS_BUCKET_NAME } from "../../constants";

// style
import "./style.scss";

// assets
import logo from "../../assets/images/default.png";

const identifyActiveLink = (basePath, searchedPath) => {
  // identify by subcategory
  if (basePath === ROUTES.LESSON_TOPIC_LIST) {
    // looking for the first '=' to identify the current category
    const linkIndex = searchedPath.indexOf("=");
    const linkLastIndex = searchedPath.indexOf("&");

    return `/${searchedPath.slice(linkIndex + 1, linkLastIndex)}`;
    // identify by lesson
  } else if (basePath === ROUTES.LESSON_TOPIC) {
    // looking for the 'category=' to identify the current category
    const linkIndex = searchedPath.indexOf("category=");
    const linkLastIndex = searchedPath.indexOf("/");

    return `/${searchedPath.slice(
      linkIndex + "category=".length,
      linkLastIndex
    )}`;
  } else {
    return basePath;
  }
};

const NavigationAuth = ({ authUser, firebase, setAgreedCookies }) => {
  const pathname = useLocation().pathname;
  const searchedPath = useLocation().search;
  const [activeItem, setActiveItem] = useState(pathname);

  useEffect(() => {
    setActiveItem(identifyActiveLink(pathname, searchedPath));
  }, [pathname, searchedPath]);

  const isAdmin =
    !!authUser && !!authUser.roles && !!authUser.roles[ROLES.ADMIN];

  return (
    <>
      <div className="nav-bar__menu-container">
        <MobileMenu
          // remove admin route if a user is not an admin
          routes={[
            ...ROUTES.AUTH_ROUTES,
            ROUTES.ACCOUNT,
            isAdmin && ROUTES.ADMIN,
          ]}
          signOut={true}
        />
        <Menu className="nav-bar__menu" borderless>
          <Menu.Menu className="nav-bar__sub-menu">
            <Menu.Item>
              <Link onClick={() => setActiveItem(ROUTES.HOME)} to={ROUTES.HOME}>
                <Image
                  className="logo"
                  src={logo}
                  alt="logo"
                  width="100%"
                  height="100%"
                />
              </Link>
            </Menu.Item>
            {ROUTES.AUTH_ROUTES.map((route) => (
              <Menu.Item
                key={route}
                className="capitalize"
                active={activeItem === route}
              >
                <Link onClick={() => setActiveItem(route)} to={route}>
                  {route && route === "/"
                    ? "Home"
                    : route.split(/[^A-Za-z]/).join(" ")}
                </Link>
              </Menu.Item>
            ))}
          </Menu.Menu>
          <Menu.Menu className="nav-bar__sub-menu" position="right">
            <Dropdown text={authUser.username}>
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link
                    onClick={() => setActiveItem(ROUTES.ACCOUNT)}
                    to={ROUTES.ACCOUNT}
                  >
                    Account
                  </Link>
                </Dropdown.Item>
                {isAdmin && (
                  <Dropdown.Item>
                    <Link
                      onClick={() => setActiveItem(ROUTES.ADMIN)}
                      to={ROUTES.ADMIN}
                    >
                      Admin
                    </Link>
                  </Dropdown.Item>
                )}
                <Dropdown.Item>
                  <SignOut />
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Menu>
      </div>
      {/* cookie modal generator in case if user hasn't agreed on it yet*/}
      {!authUser.isAgreedOnCookies && (
        <Message className="cookies-message">
          <Message.Header>
            {" "}
            By continuing to use this site, you consent to the use of cookies in
            accordance with our{" "}
            <Link to={ROUTES.PRIVACY_POLICY}> Cookie Policy.</Link>
          </Message.Header>
          <Button
            onClick={() => {
              return firebase.db
                .ref(`${USERS_BUCKET_NAME}/${authUser.uid}`)
                .update({
                  ...authUser,
                  isAgreedOnCookies: true,
                })
                .then(() => {
                  setAgreedCookies({
                    authUser: {
                      ...authUser,
                      isAgreedOnCookies: true,
                    },
                  });
                });
            }}
          >
            Accept
          </Button>
        </Message>
      )}
    </>
  );
};

const NavigationNonAuth = () => {
  const pathname = useLocation().pathname;
  const searchedPath = useLocation().search;
  const [activeItem, setActiveItem] = useState(pathname);

  useEffect(() => {
    setActiveItem(identifyActiveLink(pathname, searchedPath));
  }, [pathname, searchedPath]);

  return (
    <div className="nav-bar__menu-container">
      <MobileMenu
        signRoutes={[ROUTES.SIGN_IN, ROUTES.SIGN_UP]}
        routes={ROUTES.AUTH_ROUTES}
      />
      <Menu borderless className="nav-bar__menu">
        <Menu.Menu className="nav-bar__sub-menu">
          <Menu.Item className="nav-bar__logo-container">
            <Link onClick={() => setActiveItem(ROUTES.HOME)} to={ROUTES.HOME}>
              <Image
                className="logo"
                src={logo}
                alt="logo"
                width="100%"
                height="100%"
              />
            </Link>
          </Menu.Item>
          {ROUTES.AUTH_ROUTES.map((route) => (
            <Menu.Item
              key={route}
              active={activeItem === route}
              className="capitalize"
            >
              <Link onClick={() => setActiveItem(route)} to={route}>
                {route && route === "/"
                  ? "Home"
                  : route.split(/[^A-Za-z]/).join(" ")}
              </Link>
            </Menu.Item>
          ))}
        </Menu.Menu>
        <Menu.Menu className="nav-bar__sub-menu" position="right">
          <Menu.Item active={activeItem === ROUTES.SIGN_UP}>
            <Link
              onClick={() => setActiveItem(ROUTES.SIGN_UP)}
              to={ROUTES.SIGN_UP}
            >
              Sign up
            </Link>
          </Menu.Item>
          <Menu.Item active={activeItem === ROUTES.SIGN_IN}>
            <Link
              onClick={() => setActiveItem(ROUTES.SIGN_IN)}
              to={ROUTES.SIGN_IN}
            >
              Sign in
            </Link>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    </div>
  );
};

const Navigation = ({ authUser, firebase, setAgreedCookies }) =>
  authUser ? (
    <NavigationAuth
      authUser={authUser}
      firebase={firebase}
      setAgreedCookies={setAgreedCookies}
    />
  ) : (
    <NavigationNonAuth />
  );

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});

const mapDispatchToProps = (dispatch) => ({
  setAgreedCookies: (values) => dispatch(setSessionValues(values)),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withFirebase
)(Navigation);
