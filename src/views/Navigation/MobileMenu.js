import React, { useState } from "react";
import { List, Transition, Image } from "semantic-ui-react";
import { SIGN_IN } from "../../constants/routes";
import { Link } from "react-router-dom";
import * as ROUTES from "../../constants/routes";
import { SignOut } from "../SignForm";

// assets
import logo from "../../assets/images/default.png";

const BuildRoutes = ({ routes = [] }) =>
  routes.length &&
  routes.map(
    (item, key) =>
      typeof item === "string" && (
        <List.Item
          name={"error"}
          key={key}
          as="a"
          href={item}
          className="mobile-menu__item-container capitalize"
        >
          <List.Content>
            {item.slice(1) === "" ? "Home" : item.slice(1)}
          </List.Content>
        </List.Item>
      )
  );

const MobileMenu = ({ routes, signRoutes, signOut }) => {
  const [menu, toggleMenu] = useState(false);
  const hamburgerClass = menu ? "open" : "";
  const menuClass = menu ? "mobile-menu--open" : "";

  return (
    <div className="mobile-nav-bar">
      <div
        className="mobile-nav-bar__icon-container"
        onClick={() => toggleMenu(!menu)}
      >
        <div className="mobile-nav-bar__logo-container">
          <Link to={ROUTES.HOME}>
            <Image src={logo} alt="logo" width="100%" height="100%" />
          </Link>
        </div>
        <div id="hamburger" className={hamburgerClass}>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>

      <Transition visible={menu} animation="fade" duration={300} mountOnShow>
        <div className={menuClass}>
          <BuildRoutes routes={routes} />
          {signRoutes &&
            signRoutes.map((item) => (
              <List.Item
                as="a"
                key={item}
                href={item}
                className="mobile-menu__item-container"
              >
                <List.Content>
                  {item === SIGN_IN ? "Sign In" : "Sign Up"}
                </List.Content>
              </List.Item>
            ))}
          {signOut && (
            <List.Item className="mobile-menu__item-container">
              <List.Content>
                <SignOut />
              </List.Content>
            </List.Item>
          )}
        </div>
      </Transition>
    </div>
  );
};

export default MobileMenu;
